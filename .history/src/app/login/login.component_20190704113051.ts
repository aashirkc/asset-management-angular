import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../router.animations';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthenticateService } from '../shared';
import { JwtHelperService } from '@auth0/angular-jwt';

import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    @ViewChild('estateCombo') estateCombo: jqxComboBoxComponent;
    @ViewChild('loginLoader') loginLoader: jqxLoaderComponent;

    estateAdapter: any = [];

    loginForm: FormGroup;
    estateSelected: boolean = false;

    msgType: string;
    msgText: string;

    jwtHelper: JwtHelperService = new JwtHelperService();

    constructor(
        private route: ActivatedRoute,
        public router: Router,
        private as: AuthenticateService,
        // private es: EstateService,
        private fb: FormBuilder,
        // public dataService: AppDataService

    ) {
        this.loginForm = this.fb.group({
            'userCode': [null, Validators.required],
            'userPass': [null, Validators.required],
        });
    }

    ngOnInit() {
        let currentToken = this.as.getToken();
        let currentUser = this.as.getUser();
        if (currentToken && this.jwtHelper.isTokenExpired(currentToken)) {
            this.as.logout();
            this.router.navigate(['/login'], { queryParams: { type: 'error', msg: 'Session Expired Please login to continue' } });
        } else if (currentToken && currentUser && !this.jwtHelper.isTokenExpired(currentToken)) {
            this.router.navigate(['/']);
        } else {
            this.as.logout();
        }

        this.route.queryParams.subscribe(params => {
            // Defaults to 0 if no query param provided.
            this.msgType = params['type'] || null;
            this.msgText = params['msg'] || null;
        });

    }

    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }

    getLogin(post) {
        console.log(post)

        this.loginLoader.open();
       let formdata= post;
       console.log(formdata)
        this.as.login(formdata)
            .subscribe(result => {
                console.log(result);
                if (result['token']) {
                    localStorage.setItem('fAssetToken', result['token']);
                    localStorage.setItem('fAssetUser', JSON.stringify(result));
                    this.loginLoader.close();
                    this.router.navigate(['/dashboard']);
                } else {
                    this.loginLoader.close();
                    this.msgText = result['error'] && result['error']['message'];
                }

                this.loginLoader.close();
            },
                error => {
                    this.loginLoader.close();
                });
    }

}
