import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticateService {

  public token: string;
  apiUrl: string;
  apiUrl1: string;

  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string,
    @Inject('LoginUrl') apiUrl1:string

  ) {
    this.apiUrl = apiUrl;
    this.apiUrl1 = apiUrl1;
    //set token if saved in localstorage
    this.token = localStorage.getItem('fAssetToken') || null;
  }

  getToken(): string {
    let currentToken = localStorage.getItem('fAssetToken') || null;
    return currentToken;
  }

  getUser(): any {
    let currentUser = JSON.parse(localStorage.getItem('fAssetUser'));
    return currentUser;
  }

  login(post): Observable<any[]> {
      console.log(post)
    return this.http.post(this.apiUrl1 + 'Login/' , post)
      .map((response) => {
        let token = response && response['token'];
        return <any[]>response;
      },
        (error: Error) => error);
  }




  logout(): Observable<any[]>{
    this.token = null;
    return this.http.post(this.apiUrl+'Logout', {})
    .map((response) => {
      console.info(response);
       return <any[]>response;
    },
    (error: Error) => error);
  }




  loginTokenUpdate(): Observable<any[]>{
    return this.http.post(this.apiUrl+'ResetToken', {})
    .map((response) => {
       let token = response && response['token'];
       return <any[]>response;
    },
    (error: Error) => error);
  }

  getDetail(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'authenticate', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }


}

