import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe, DecimalPipe } from '@angular/common';
import { HttpClientModule, HttpClient, HttpClientXsrfModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule,TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxBarcodeModule } from 'ngx-barcode';

import { AllReportService,
    AuthenticateService,
    FinancialYearService,
    TfmHttpInterceptorService,
    DepartmentMasterService,
    DepreciationRatioService,
    ChartOfItemsService,
    BranchMasterService,
    SupplierMasterService,
    GoodsReceivingNotesService,
    RequisitionSlipService,
    SiteMasterService,
    AssetAccessoriesMasterService,
    AssetSpecificationMasterService,
    ItemAccessoriesService,
    ItemSpecificationService,
    DateFormatService,
    PurchaseOrderService,
    ItemInsuranceService,
    BillSundryMasterService,
    GoodsIssueService,
    GoodsReturnService,
    GoodsDisposalService,
    ApproveRequisitionService,
    UserService,
    PermissionService,
    ApprovePurchaseOrderService,
    YearlyRepairExpensesService,
    SetSerialNumberService,
    ItemRepairMaintenanceService,
    BackupService,
    IssueApproveService,
    CurrentUserService,
    ItemWarrantyService,
    EmployeeDetailsService,
    OrganizationMasterService
} from './shared';
import { AuthGuard, CanNavigateRouteGuard, NavigateGuard } from './shared';
import { SharedPipesModule } from './shared';
import { NgxPermissionsModule, NgxPermissionsService, NgxPermissionsStore, NgxRolesStore, NgxPermissionsGuard } from 'ngx-permissions';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
    // for development
    // return new TranslateHttpLoader(http, '/start-angular/SB-Admin-BS4-Angular-4/master/dist/assets/i18n/', '.json');
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        HttpClientXsrfModule,
        AppRoutingModule,
        NgxBarcodeModule,
        SharedPipesModule,
        NgxPermissionsModule.forChild(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    providers: [
        AuthGuard,
        NavigateGuard,
        CanNavigateRouteGuard,
        NgxPermissionsService,
        NgxPermissionsStore,
        NgxRolesStore,
        NgxPermissionsGuard,
        DatePipe,
        DecimalPipe,
        AuthenticateService,
        AllReportService,
        FinancialYearService,
        TfmHttpInterceptorService,
        DepartmentMasterService,
        DepreciationRatioService,
        ChartOfItemsService,
        BranchMasterService,
        SupplierMasterService,
        GoodsReceivingNotesService,
        RequisitionSlipService,
        SiteMasterService,
        AssetAccessoriesMasterService,
        AssetSpecificationMasterService,
        ItemAccessoriesService,
        ItemSpecificationService,
        DateFormatService,
        PurchaseOrderService,
        ItemInsuranceService,
        BillSundryMasterService,
        GoodsIssueService,
        GoodsReturnService,
        GoodsDisposalService,
        ApproveRequisitionService,
        UserService,
        PermissionService,
        ApprovePurchaseOrderService,
        YearlyRepairExpensesService,
        SetSerialNumberService,
        ItemRepairMaintenanceService,
        IssueApproveService,
        BackupService,
        CurrentUserService,
        ItemWarrantyService,
        TranslateService,
        EmployeeDetailsService,
        OrganizationMasterService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TfmHttpInterceptorService,
            multi: true,
        },
        // { provide: 'API_URL', useValue: 'http://182.93.67.119:8081/FixedAsset/api/' },
        // { provide: 'API_URL_DOC', useValue: 'http://182.93.67.119:8080/FixedAssetDocument/' },

        // {provide: 'API_URL', useValue: 'http://stg.phoenixsolutions.com.np:8080/simricfa/api/'},
        // {provide: 'API_URL_DOC', useValue: 'http://stg.phoenixsolutions.com.np:8080/simricfaDocument/'},
        // {provide: 'LoginUrl', useValue: 'http://stg.phoenixsolutions.com.np:8080/simricfa/'},

        // { provide: 'API_URL', useValue: 'http://192.168.100.4:8084/FixedAsset/api/' },
        // { provide: 'API_URL_DOC', useValue: 'http://192.168.100.4:8084/FixedAssetDocument/' },
        // { provide: 'LoginUrl', useValue: 'http://192.168.100.4:8084/FixedAsset/' },

        // { provide: 'API_URL', useValue: 'http://192.168.100.11:8080/api/' },
        // { provide: 'API_URL_DOC', useValue: 'http://192.168.100.11:8080/FixedAssetDocument/' },
        // { provide: 'LoginUrl', useValue: 'http://192.168.100.11:8080/' },


        // { provide: 'API_URL', useValue: 'http://ms-pc:8084/FixedAsset/api/' },
        // { provide: 'API_URL_DOC', useValue: 'http://ms-pc:8084/FixedAssetDocument/' },
        // { provide: 'API_URL_DOC', useValue: '/FixedAssetDocument/' },
        // { provide: 'API_URL', useValue: '/FixedAsset/api/' }

        // { provide: 'API_URL', useValue: window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2)) + "/api/" },
        // { provide: 'API_URL_DOC', useValue: window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2)) + 'Document/' },
        // { provide: 'LoginUrl', useValue: window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2)) + '/' },


    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
