import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { YearlyRepairExpensesService, FinancialYearService, DateFormatService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
  selector: 'app-yearly-repair-expenses',
  templateUrl: './yearly-repair-expenses.component.html',
  styleUrls: ['./yearly-repair-expenses.component.scss']
})
export class YearlyRepairExpensesComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

  /**
  * Global Variable decleration
  */
  yreForm: FormGroup;
  fyForm: FormGroup;

  fiscalYears: any = [];

  currentDate: any;


  constructor(
    private fb: FormBuilder,
    private fys: FinancialYearService,
    private yres: YearlyRepairExpensesService,
    private dfs: DateFormatService,
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.getFiscalYear();
  }

  ngAfterViewInit() {
    this.dfs.currentDate().subscribe(
      response => {
        this.currentDate = response;
        this.yreForm.get('enterDate').setValue(response[0]['fyEndBs']);
      },
      error => {
        console.log(error);
      }
    );
  }

  createForm() {
    this.fyForm = this.fb.group({
      fiscalYear: ['', Validators.required],
    });

    this.yreForm = this.fb.group({
      'requisitionItems': this.fb.array([
        // this.initReqItems(),
      ]),
      'enterDate': [null, Validators.required]
    });
  }

  initReqItems() {
    return this.fb.group({
      itemCode: [{ value: null, disabled: true }, Validators.required],
      itemName: [{ value: null, disabled: true }, Validators.required],
      group: [{ value: null, disabled: true }, Validators.required],
      repairExpensesAllow: [{ value: 0.00, disabled: true }, Validators.required],
      repairExpensesAmount: [{ value: 0.00, disabled: false }, Validators.required],
      depreciatedAmountAtTheEnd: [{ value: 0.00, disabled: true }, Validators.required],
      closeAmt: [{ value: 0.00, disabled: true }, Validators.required],
    });
  }

  getFiscalYear() {
    this.fys.index({}).subscribe(
      response => {

        if (response.length == 1 && response[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response[0].error;
          this.errNotification.open();
          this.fiscalYears = [];
        } else {
          this.fiscalYears = response;
        }
      },
      error => {
        console.log(error);
      }
    )
  }

  /**
 * Add FormGroup to Requisition Item FormArray
 * Increments Requestion Item FormArray
 */
  addItem() {
    const control1 = <FormArray>this.yreForm.controls['requisitionItems'];
    control1.push(this.initReqItems());
  }

  search(formData) {
    this.jqxLoader.open();
    this.yreForm.setControl('requisitionItems', this.fb.array([]));
    let fy = formData['fiscalYear']
    this.yres.show(fy).subscribe(
      response => {
        if (response['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response['error']['message'];
          this.errNotification.open();
          this.jqxLoader.close();
        } else {
          for (let i = 0; i < response.length; i++) {
            this.addItem();
            response[i]['closeAmt'] = response[i]['closeAmt'] > 0 && Number(response[i]['closeAmt']).toFixed(2) || response[i]['closeAmt'];
            response[i]['repairExpensesAmount'] = response[i]['repairExpensesAmount'] > 0 && Number(response[i]['repairExpensesAmount']).toFixed(2) || response[i]['repairExpensesAmount'];
            response[i]['depreciatedAmountAtTheEnd'] = response[i]['depreciatedAmountAtTheEnd'] > 0 && Number(response[i]['depreciatedAmountAtTheEnd']).toFixed(2) || response[i]['depreciatedAmountAtTheEnd'];
            response[i]['repairExpensesAllow'] = response[i]['repairExpensesAllow'] > 0 && Number(response[i]['repairExpensesAllow']).toFixed(2) || response[i]['repairExpensesAllow'];
            response[i]['repairExpensesAmount'] = response[i]['repairExpensesAmount'] > 0 && Number(response[i]['repairExpensesAmount']).toFixed(2) || response[i]['repairExpensesAmount'];
          }
          this.yreForm.get('requisitionItems').patchValue(response);
        }
        this.jqxLoader.close();
      },
      error => {
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }

  repairExpensesAmountChange(items, index) {
    let rowData = this.yreForm.get('requisitionItems')['controls'][index].getRawValue();
    let repairExpensesAllowValue = rowData['repairExpensesAllow'];
    let repairExpensesAmountValue = rowData['repairExpensesAmount'];
    let depreciatedAmountAtTheEndValue = rowData['depreciatedAmountAtTheEnd'];
    let closeAmtValue = 0;
    let calcValue = 0;
    if (repairExpensesAllowValue < repairExpensesAmountValue) {
      calcValue = depreciatedAmountAtTheEndValue + (repairExpensesAmountValue - repairExpensesAllowValue);
    } else {
      calcValue = depreciatedAmountAtTheEndValue;
    }
    console.log(calcValue);
    calcValue = Number(calcValue);
    this.yreForm.get('requisitionItems')['controls'][index].get('closeAmt').patchValue(calcValue.toFixed(2));
  }

  save() {
    let formData = this.yreForm.getRawValue();
    formData['fy'] = this.fyForm.get('fiscalYear').value;
    // console.log(formData);
    if (formData && formData['fy']) {
      this.jqxLoader.open();
      this.yres.store(formData).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    }

  }


}
