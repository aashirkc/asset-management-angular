import { Component, OnInit, ElementRef, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ApprovePurchaseOrderService,CurrentUserService, BranchMasterService, DateFormatService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-approve-purchase-order',
  templateUrl: './approve-purchase-order.component.html',
  styleUrls: ['./approve-purchase-order.component.scss']
})
export class ApprovePurchaseOrderComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('input1') inputEl: ElementRef;
  @ViewChild('myWindow') myWindow: jqxWindowComponent;
  @ViewChild('groupGrid') groupGrid: jqxGridComponent;
  @ViewChild('orderGrid') orderGrid: jqxGridComponent;


  approveReqForm: FormGroup;
  gridSource: any;
  gridDataAdapter: any;
  gridColumns: any = [];
  orderSource: any;
  orderDataAdapter: any;
  orderColumns: any = [];
  branchAdapter: any = [];
  editrow: number = -1;
  showDetailsGrid: boolean = false;
  orderName:string;
  userData: any = {};

  constructor(
    private fb: FormBuilder,
    private bms: BranchMasterService,
    private ars: ApprovePurchaseOrderService,
    private cus: CurrentUserService,
    private dfs: DateFormatService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
    this.userData = this.cus.getTokenData();
  }

  createForm() {
    this.approveReqForm = this.fb.group({
      'branchCode': [null, Validators.required],
      'approveDate': [null, Validators.required],
      'approveBy': [null, Validators.required]
    });
  }

  transData:any;
  getTranslation(){
    this.translate.get(['ORDERPENDINGLIST','ORDERPENDINGLISTDETAIL','SN','ORDER','DATE','NUMBER', 'ORDERBY','TYPE','SUPPLIER','DELIVERYPLACE','ITEM','NAME','SPECIFICATION','UNIT','ORDERQTY','RATE','TOTAL','INDELIVERYDAY','REMARKS','ACTION','VIEW']).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  ngOnInit() {
    this.bms.index({}).subscribe(
      response => {
        if (response.length == 1 && response[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response[0].error;
          this.errNotification.open();
          this.branchAdapter = [];
        } else {
          this.branchAdapter = response;
        }
      },
      error => {
        console.log(error)
      });

    this.gridSource =
      {
        datatype: 'json',
        datafields: [
          { name: 'orderDate', type: 'string' },
          { name: 'orderNo', type: 'string' },
          { name: 'orderBy', type: 'string' },
          { name: 'orderType', type: 'string' },
          { name: 'supplier', type: 'string' },
          { name: 'deleveryPlace', type: 'string' }
        ],
        id: 'orderNo',
        localdata: [],
      };

    this.gridDataAdapter = new jqx.dataAdapter(this.gridSource);

    this.gridColumns =
      [
        {
          text: this.transData['SN'], sortable: false, filterable: false, editable: false,
          groupable: false, draggable: false, resizable: false,
          datafield: '', columntype: 'number', width: 50,
          cellsrenderer: function (row, column, value) {
            return "<div style='margin:4px;'>" + (value + 1) + "</div>";
          }
        },
        { text: this.transData['ORDER'] + ' ' + this.transData['DATE'], datafield: 'orderDate', },
        { text: this.transData['ORDER'] + ' ' + this.transData['NUMBER'], datafield: 'orderNo' },
        { text: this.transData['ORDERBY'], datafield: 'orderBy' },
        { text: this.transData['ORDER'] + ' ' + this.transData['TYPE'], datafield: 'orderType' },
        { text: this.transData['SUPPLIER'], datafield: 'supplier', width: 250 },
        { text: this.transData['DELIVERYPLACE'], datafield: 'deleveryPlace' },
        {
          text: this.transData['ACTION'], datafield: 'view', sortable: false, filterable: false, width: 85, columntype: 'button',
          cellsrenderer: (): string => {
            return this.transData['VIEW'];
          },
          buttonclick: (row: number): void => {
            this.editrow = row;
            let dataRecord = this.groupGrid.getrowdata(this.editrow);
            this.orderName = dataRecord['orderBy']
            if (dataRecord['orderNo']) {
              this.jqxLoader.open();
              this.ars.show(dataRecord['orderNo']).subscribe((res) => {
                this.orderSource.localdata = res;
                this.orderGrid.updatebounddata();
                this.jqxLoader.close();
              }, (error) => {
                this.jqxLoader.close();
              });
            }
          }
        }
      ];

    this.orderSource =
      {
        datatype: 'json',
        datafields: [
          { name: 'itemName', type: 'string' },
          { name: 'specification', type: 'string' },
          { name: 'unit', type: 'string' },
          { name: 'orderQty', type: 'number' },
          { name: 'rate', type: 'number' },
          { name: 'total', type: 'number',map:'total.toFixed(2)' },
          { name: 'inDeliveryDay', type: 'number' },
          { name: 'remarks', type: 'string' }
        ],
        localdata: [],
      };

    this.orderDataAdapter = new jqx.dataAdapter(this.orderSource);

    this.orderColumns =
      [
        {
          text: this.transData['SN'], sortable: false, filterable: false, editable: false,
          groupable: false, draggable: false, resizable: false,
          datafield: '', columntype: 'number', width: 50,
          cellsrenderer: function (row, column, value) {
            return "<div style='margin:4px;'>" + (value + 1) + "</div>";
          }
        },
        { text: this.transData['ITEM'] + ' ' + this.transData['NAME'], datafield: 'itemName',width: 250 },
        { text: this.transData['SPECIFICATION'], datafield: 'specification',width: 150 },
        { text: this.transData['UNIT'], datafield: 'unit' },
        { text: this.transData['ORDERQTY'], datafield: 'orderQty' },
        { text: this.transData['RATE'], datafield: 'rate',width: 150 },
        { text: this.transData['TOTAL'], datafield: 'total',width: 150 ,
        cellsrenderer: function (row, column, value) {
            return "<div style='margin:4px;'>" + value['rate']*value['total'] + "</div>";
          }},
        { text: this.transData['INDELIVERYDAY'], datafield: 'inDeliveryDay', },
        { text: this.transData['REMARKS'], datafield: 'remarks' },
      ];
  }

  gridRenderToolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';
    let buttonContainer3 = document.createElement('div');
    buttonContainer3.id = 'buttonContainer3';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.innerHTML = "<b>"+this.transData['ORDERPENDINGLIST']+":</b>";
    container.appendChild(buttonContainer3);
    toolbar[0].appendChild(container);
  };

  orderRenderToolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';
    let buttonContainer3 = document.createElement('div');
    buttonContainer3.id = 'buttonContainer3';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.innerHTML = "<b>"+ this.transData['ORDERPENDINGLISTDETAIL'] +":</b>";
    container.appendChild(buttonContainer3);
    toolbar[0].appendChild(container);
  };

  ngAfterViewInit() {
    this.dfs.currentDate().subscribe((response)=>{
      let dateData = response[0];
        setTimeout(() => {
          this.approveReqForm.controls['approveDate'].setValue(dateData['BS_DATE']);
          this.approveReqForm.get('approveDate').markAsTouched();
          this.approveReqForm.controls['approveBy'].setValue(this.userData['user'].userName);
          this.approveReqForm.get('approveBy').markAsTouched();
        }, 100);
    },(error)=>{
      console.info(error);
    });

  }

  branchChange(event) {
    if (event) {
      this.jqxLoader.open();
      let data = {};
      data['branch'] = event;
      this.ars.index(data).subscribe((res) => {
        this.gridSource.localdata = res;
        this.groupGrid.updatebounddata();
        this.jqxLoader.close();
        if (res[0]['error']) {
          this.gridSource.localdata = [];
          this.groupGrid.updatebounddata();
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0]['error'];
          this.errNotification.open();
        }
      }, (error) => {
        this.jqxLoader.close();
      });
    }
  }

  approve(formData) {
    let id = this.groupGrid.getselectedrowindexes();;
    let ids = [];
    for (let i = 0; i < id.length; i++) {
      let dataRecord = this.groupGrid.getrowdata(Number(id[i]));
      let dt = {};
      dt['orderNo'] = dataRecord['orderNo'];
      ids.push(dt);
    }
    this.jqxLoader.open();
    formData['array'] = ids;
    formData['status'] = 'Approve';
    if (formData['array'].length > 0) {
      this.ars.store(formData).subscribe(
        result => {
            console.log('hello')
          if (result['message']) {
            this.branchChange(this.approveReqForm.controls['branchCode'].value);
            this.orderSource.localdata = [];
            this.orderGrid.updatebounddata();
            this.groupGrid.clearselection();
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          this.jqxLoader.close();
          if (result['error']) {
            this.branchChange(this.approveReqForm.controls['branchCode'].value);
            this.orderSource.localdata = [];
            this.orderGrid.updatebounddata();
            this.groupGrid.clearselection();
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      this.jqxLoader.close();
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = "Please Select at least one item";
      this.errNotification.open();
    }
  }

  reject(formData) {
    let id = this.groupGrid.getselectedrowindexes();;
    let ids = [];
    for (let i = 0; i < id.length; i++) {
      let dataRecord = this.groupGrid.getrowdata(Number(id[i]));
      let dt = {};
      dt['orderNo'] = dataRecord['orderNo'];
      ids.push(dt);
    }
    this.jqxLoader.open();
    formData['array'] = ids;
    formData['status'] = 'Reject';
    if (formData['array'].length > 0) {
      this.ars.store(formData).subscribe(
        result => {
          if (result['message']) {
            this.branchChange(this.approveReqForm.controls['branchCode'].value);
            this.orderSource.localdata = [];
            this.orderGrid.updatebounddata();
            this.groupGrid.clearselection();
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          this.jqxLoader.close();
          if (result['error']) {
            this.branchChange(this.approveReqForm.controls['branchCode'].value);
            this.orderSource.localdata = [];
            this.orderGrid.updatebounddata();
            this.groupGrid.clearselection();
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      this.jqxLoader.close();
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = "Please Select at least one item";
      this.errNotification.open();
    }
  }
}
