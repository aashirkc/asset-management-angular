import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { SiteMasterService, BranchMasterService, UserService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('branchCombo') branchCombo: jqxComboBoxComponent;
  @ViewChild('siteCombo') siteCombo: jqxComboBoxComponent;

  uForm: FormGroup;

  // For Grid
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  deleteRowIndexes: Array<any> = [];

  branchAdapter: Array<any> = [];
  siteAdapter: Array<any> = [];

  userTypes: Array<any> = [
    { type: 'B', name: 'Branch User' },
    { type: 'S', name: 'Site User' }
  ];

  constructor(
    private fb: FormBuilder,
    private sms: SiteMasterService,
    private bms: BranchMasterService,
    private us: UserService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
  }

  transData:any;
  getTranslation(){
    this.translate.get(['SN','USER','CODE', 'FULLNAME','CONTACTNO',"ADDRESS","TYPE","EMAILADDRESS","BRANCH","SITE","ACTION","UPDATE","RELOAD","DELETESELECTED"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  ngOnInit() {
    this.bms.index({}).subscribe((res) => {
      this.branchAdapter = res;
    }, (error) => {
      console.info(error);
    });

    this.sms.index({}).subscribe((res) => {
      this.siteAdapter = res;
    }, (error) => {
      console.info(error);
    });
    this.loadGrid();
  }

  createForm() {
    this.uForm = this.fb.group({
      'userCode': ['', Validators.required],
      'userName': ['', Validators.required],
      'userPassword': ['', Validators.required],
      'contactNumber': ['', Validators.required],
      'address': ['', Validators.required],
      'userType': ['', Validators.required],
      'email': ['', Validators.required]
    });
  }

  ngAfterViewInit() {
    this.loadGridData();

  }

  loadGridData() {
    this.myGrid.clearselection();
    this.deleteRowIndexes = [];
    this.jqxLoader.open();
    this.us.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        this.source.localdata = res;
      }
      this.jqxLoader.close();
      this.myGrid.updatebounddata();
    }, (error) => {
      this.jqxLoader.close();
    });
  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'userCode', type: 'string' },
          { name: 'userName', type: 'string' },
          { name: 'contactNumber', type: 'string' },
          { name: 'address', type: 'string' },
          { name: 'userType', type: 'string' },
          { name: 'email', type: 'string' },
          { name: 'branch', type: 'string' },
        //   { name: 'site', type: 'string' },
        ],
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);

    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: '', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['USER'] + ' ' + this.transData['CODE'], datafield: 'userCode', columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      { text: this.transData['FULLNAME'], datafield: 'userName', columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['CONTACTNO'], datafield: 'contactNumber', columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['ADDRESS'], datafield: 'address', columntype: 'textbox', filtercondition: 'starts_with' },
      {
        text: this.transData['TYPE'], datafield: 'userType', displayfield: 'userType', columntype: 'combobox', filtercondition: 'containsignorecase',
        createeditor: (row: number, column: any, editor: any): void => {
          editor.jqxComboBox({
            source: this.userTypes,
            valueMember: "type",
            displayMember: "name"
          });
        }
      },
      { text: this.transData['EMAILADDRESS'], datafield: 'email', columntype: 'textbox', filtercondition: 'starts_with' },
      {
        text: this.transData['BRANCH'], datafield: 'branch', displayfield: 'branch', columntype: 'combobox', filtercondition: 'containsignorecase',
        createeditor: (row: number, column: any, editor: any): void => {
          editor.jqxComboBox({
            source: this.branchAdapter,
            valueMember: "branchCode",
            displayMember: "branchName"
          });
        }
      },
    //   {
    //     text: this.transData['SITE'], datafield: 'site', displayfield: 'site', columntype: 'combobox', filtercondition: 'containsignorecase',
    //     createeditor: (row: number, column: any, editor: any): void => {
    //       editor.jqxComboBox({
    //         source: this.siteAdapter,
    //         valueMember: "sideCode",
    //         displayMember: "sideName"
    //       });
    //     }
    //   },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 85, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['UPDATE'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          this.jqxLoader.open();
          this.us.update(dataRecord['userCode'], dataRecord).subscribe(
            result => {
              this.jqxLoader.close();
              if (result['message']) {
                let messageDiv: any = document.getElementById('message');
                messageDiv.innerText = result['message'];
                this.msgNotification.open();
                this.loadGridData();
              }
              if (result['error']) {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = result['error']['message'];
                this.errNotification.open();
                this.loadGridData();
              }
            },
            error => {
              this.jqxLoader.close();
              console.info(error);
            }
          );

        }
      }
    ];

  }


  rowChange(event: any) {
    this.deleteRowIndexes.push(event.args.rowindex);
  }
  rowUnChange(event: any) {
    let index = this.deleteRowIndexes.indexOf(event.args.rowindex);
    if (index > -1) {
      this.deleteRowIndexes.splice(index, 1);
    }

  }
  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETESELECTED'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 80, value: '<i class="fa fa-refresh fa-fw"></i> '+ this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.deleteRowIndexes;
      let ids = [];
      let rowscount = this.myGrid.getdatainformation().rowscount;
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myGrid.getrowdata(Number(id[i]));
        ids.push("'" + dataRecord['userCode'] + "'");
        let testId = this.myGrid.getrowid(Number(id[i]));
        this.myGrid.deleterow(testId);
      }
      if (ids.length > 0 && ids.length <= parseFloat(rowscount)) {
        if (confirm("Are you sure? You Want to delete")) {
          this.jqxLoader.open();
          this.us.destroy('(' + ids + ')').subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.loadGridData();

            }
            if (result['error']) {
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = 'Please select some item to delete';
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      this.loadGridData();
    });

  }; //render toolbar ends

  save(post) {
    console.log(post);
    post['branch'] = this.branchCombo.val();
    post['site'] = this.siteCombo.val() || '';
    console.log(post);
    if (post && post['branch']) {
      this.jqxLoader.open();
      this.us.store(post).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
            this.loadGridData();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please enter all required Field.';
      this.errNotification.open();
    }
  }

}
