import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { OrganizationMasterService, } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';

@Component({
  selector: 'app-organization-master',
  templateUrl: './organization-master.component.html',
  styleUrls: ['./organization-master.component.scss']
})
export class OrganizationMasterComponent implements OnInit {

  supplierForm: FormGroup;
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;

   constructor(
    private fb: FormBuilder,
    private oms: OrganizationMasterService,
  ) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.supplierForm = this.fb.group({
      'orgName': ['', Validators.required],
      'contactNo': ['', Validators.required],
      'address': ['', Validators.required],
      'email': ['', Validators.required],
      'fax': ['', Validators.required],
    });
  }
  ngAfterViewInit() {
    this.oms.index({}).subscribe((res)=>{

        console.log(res)
    //   if(res[0]){
    //     setTimeout(() => {
    //       this.supplierForm.setValue(res[0]);
    //     }, 100);
        this.supplierForm.get('orgName').patchValue(res['orgName']);


    },(error)=>{
      console.info(error);
    });

  }

  saveBtn(post) {
    this.jqxLoader.open();
    this.oms.store(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
}
