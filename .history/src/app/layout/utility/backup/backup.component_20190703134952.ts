import { Component, ViewChild, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { BackupService, UserService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';


@Component({
  selector: 'app-backup',
  templateUrl: './backup.component.html',
  styleUrls: ['./backup.component.scss']
})
export class BackupComponent implements OnInit {
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  documentUrl: string;

  constructor(
    private us: UserService,
    @Inject('API_URL_DOC') API_URL_DOC: string,
    private bs: BackupService) {
    this.documentUrl = API_URL_DOC;
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.loadDocument();
  }

  loadDocument() {
    this.jqxLoader.open();
    this.bs.index({}).subscribe(
      response => {
          console.log(response)
        if (response['message']) {
          let anchor = document.createElement('a');
          anchor.style.cssText = 'display: none';
          document.body.appendChild(anchor);
          anchor.href = this.documentUrl + response['message'];
          anchor.target = '_blank';
          anchor.click();
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = "Success";
          this.msgNotification.open();
        }
        if (response['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response['error']['message'];
          this.errNotification.open();
        }
        this.jqxLoader.close();
      },
      error => {
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }
}
