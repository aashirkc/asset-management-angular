import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {
  BranchMasterService,
  ChartOfItemsService,
  SetSerialNumberService,
  AssetAccessoriesMasterService,
  AssetSpecificationMasterService,
  ItemAccessoriesService,
  ItemSpecificationService,
  ItemInsuranceService,
  ItemWarrantyService,
  DateFormatService,
  BillSundryMasterService,
  GoodsReceivingNotesService
 } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-set-serial-number',
  templateUrl: './set-serial-number.component.html',
  styleUrls: ['./set-serial-number.component.scss']
})
export class SetSerialNumberComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('accessoryGrid') accessoryGrid: jqxGridComponent;
  @ViewChild('specificationGrid') specificationGrid: jqxGridComponent;

  /**
  * Global Variable decleration
  */
  seForm: FormGroup;

  // Item Insurance Form
  iiForm: FormGroup;

  // Upload Image Form
  uploadForm: FormGroup;

  // Warranty Form
  iwForm: FormGroup;

  // Bill Sundry Form
  billSundryForm: FormGroup;

  branchAdapter: any;

  itemAdapter: any;
  itemFocus: boolean = false;

  accessorySource: any;
  accessoryDataAdapter: any;
  accessoryColumns: any;

  specificationSource: any;
  specificationDataAdapter: any;
  specificationColumns: any;

  isAssesoryLoaded: boolean = false;
  isSpecificationLoaded: boolean = false;

  constructor(
    private fb: FormBuilder,
    private bms: BranchMasterService,
    private cois: ChartOfItemsService,
    private ssns: SetSerialNumberService,
    private aams: AssetAccessoriesMasterService,
    private asms: AssetSpecificationMasterService,
    private ias: ItemAccessoriesService,
    private iss: ItemSpecificationService,
    private iis: ItemInsuranceService,
    private iws: ItemWarrantyService,
    private dfs: DateFormatService,
    private bsms: BillSundryMasterService,
    private grns: GoodsReceivingNotesService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
  }

  transData:any;
  getTranslation(){
    this.translate.get(['SN','ACCESSORIES','SPECIFICATION', 'UNIT','REMARKS',"SAVEACCESSORIES","SAVESPECIFICATION"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  ngOnInit() {

    let Data = JSON.parse(localStorage.getItem('fAssetUser'))


    let Date1 = Data['fiscalYear']
    let startDate = Date1['startDateBS']
    let todayDate = Date1['endDateBS']
    console.log(startDate)

    this.iiForm.get('insuranceDateFrom').patchValue(todayDate);
    this.iiForm.get('warrantyFrom').patchValue(todayDate);


    this.setGridSource();
    this.bms.index({}).subscribe(
      response => {
        if (response.length == 1 && response[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response[0].error;
          this.errNotification.open();
          this.branchAdapter = [];
        } else {
          this.branchAdapter = response;
        }
      },
      error => {
        console.log(error);
      }
    );
  }


  ngAfterViewInit() {
    this.dfs.currentDate().subscribe(
      response => {
        let BS_DATE = response[0].BS_DATE;
        this.iiForm.get('insuranceDateFrom').setValue(BS_DATE);
        this.iwForm.get('warrantyFrom').setValue(BS_DATE);
      },
      error => {
        console.log(error);
      }
    );

  }

  createForm() {

    this.seForm = this.fb.group({
      branch: [null, Validators.required],
      itemCode: [null, Validators.required],
    });

    this.uploadForm = this.fb.group({
      image: [null, Validators.required],
    });

    this.iiForm = this.fb.group({
      'insurancesCompaniesCode': [null, Validators.required],
      'insurancesCompaniesName': [null, Validators.required],
      'address': [null, Validators.required],
      'officeContactNo': [null, Validators.required],
      'contactPerson': [null, Validators.required],
      'contactPersonNo': [null, Validators.required],
      'insuranceAmount': [null, Validators.required],
      'insuranceDateFrom': [null, Validators.required],
      'insuranceDateTo': [null, Validators.required]
    });

    this.iwForm = this.fb.group({
      'warrantyCompany': [null, Validators.required],
      'warrantyCompanyAddress': [null, Validators.required],
      'contactPerson': [null, Validators.required],
      'contactNo': [null, Validators.required],
      'warrantyFrom': [null, Validators.required],
      'warrantyMonth': [null, Validators.required]
    });

    this.billSundryForm = this.fb.group({
      'itemNo': [{value: null, disabled: true}, Validators.required],
      'charge': [0, Validators.required],
      'totalAmt': [{value: null, disabled: true}, Validators.required],
      'rate': [{value: null, disabled: true}, Validators.required],
    });
  }

  /**
 * itemFilter Event is called when Item input field has
 * keyup action followed by 'Enter'
 * Generate Suggestion based on input value entered
 * @param searchString
 * @param index
 */
  itemFilter(searchPr) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemCode').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(/\s/g,'');
    // let temp = searchString.replace(' ', '');
    console.log(searchString);
    if (dataString == ' ' && searchString.length > 2) {
      let branch = this.seForm.get('branch').value;
      if (searchString && branch) {
        this.jqxLoader.open();
        this.itemFocus = true;
        let post = {};
        post['item'] = temp;
        this.ssns.searchItem(branch, post).subscribe(
          response => {
            this.itemAdapter = response;
            this.jqxLoader.close();
          },
          error => {
            console.log(error);
            this.jqxLoader.close();
          }
        );
      } else {
        this.itemFocus = false;
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = 'Please select branch';
        this.errNotification.open();
      }
    }

  }


  /**
 * Event fired when option is selected from Item Suggestion Select field
 * Hide Select field after Item Selected.
 * @param selectedValue
 * @param index
 */
  itemListSelected(selectedValue) {

    if (selectedValue) {
      this.seForm.get('itemCode').patchValue(selectedValue);

      this.billSundryForm.get('itemNo').setValue(selectedValue);
      this.setupBillSundry(selectedValue);
    }
  }

  checkLoading() {
    if (this.isAssesoryLoaded && this.isSpecificationLoaded) {
      this.jqxLoader.close();
    }
  }

  loadGridData() {
    this.isAssesoryLoaded = false;
    this.isSpecificationLoaded = false;

    let selectedValue = this.seForm.get('itemCode').value;

    if (selectedValue) {
      this.jqxLoader.open();
      // Load Data in Jqx Loader;
      this.aams.show(selectedValue).subscribe(
        response => {
          this.accessorySource.localdata = [];
          this.accessorySource.localdata = response;
          this.isAssesoryLoaded = true;
          this.checkLoading();
          this.accessoryGrid.updatebounddata();

        },
        error => {
          console.log(error);
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = 'Could not load accessories';
          this.errNotification.open();
          this.jqxLoader.close();
        }
      );

      this.asms.show(selectedValue).subscribe(
        response => {
          this.specificationSource.localdata = [];
          this.specificationSource.localdata = response;
          this.isSpecificationLoaded = true;
          this.checkLoading();
          this.specificationGrid.updatebounddata();
        },
        error => {
          console.log(error);
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = 'Could not load accessories';
          this.errNotification.open();
          this.jqxLoader.close();
        }
      )
    }
  }

  accessoryGridRenderStatusbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer1 = document.createElement('div');

    buttonContainer1.id = 'buttonContainer1';

    buttonContainer1.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer1);
    toolbar[0].appendChild(container);

    let saveAccessoryDataButton = jqwidgets.createInstance('#buttonContainer1', 'jqxButton', { width: 200, value: this.transData['SAVEACCESSORIES'], theme: 'energyblue' });

    saveAccessoryDataButton.addEventHandler('click', () => {
      let post = {};
      let formData = this.seForm.value;

      let ids = this.accessoryGrid.getselectedrowindexes();
      let arrData = [];
      for (let i = 0; i < ids.length; i++) {
        let id = String(ids[i]);
        let data = this.accessoryGrid.getrowdata(Number(id));
        arrData.push(data);
      }
      let branch = formData['branch'];
      post['accessories'] = arrData;
      post['itemNo'] = formData['itemCode'];
      if (arrData.length > 0 && post['itemNo'] && branch) {
        this.jqxLoader.open();
        this.ias.update(branch,post).subscribe(
          result => {
            if (result['message']) {
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.accessorySource.localdata = [];
              this.accessoryGrid.updatebounddata();
              this.specificationGrid.clearselection();
            }
            if (result['error']) {
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
            }
            this.jqxLoader.close();
          },
          error => {
            this.jqxLoader.close();
            console.info(error);
          }
        );
      } else {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = 'Please select Accessories';
        this.errNotification.open();
      }
    });

  }; //render toolbar ends

  specificationGridRenderStatusbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let saveSpecificationDataButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 170, value: this.transData['SAVESPECIFICATION'], theme: 'energyblue' });

    saveSpecificationDataButton.addEventHandler('click', () => {
      let post = {};
      let formData = this.seForm.value;

      let ids = this.specificationGrid.getselectedrowindexes();
      let arrData = [];
      for (let i = 0; i < ids.length; i++) {
        let id = String(ids[i]);
        let data = this.specificationGrid.getrowdata(Number(id));
        arrData.push(data);
      }
      let branch = formData['branch'];
      post['specifications'] = arrData;
      post['itemNo'] = formData['itemCode'];

      if (arrData.length > 0 && post['itemNo'] && branch) {
        this.jqxLoader.open();
        this.iss.update(branch,post).subscribe(
          result => {
            if (result['message']) {
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.specificationSource.localdata = [];
              this.specificationGrid.updatebounddata();
              this.specificationGrid.clearselection();
            }
            if (result['error']) {
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
            }
            this.jqxLoader.close();
          },
          error => {
            this.jqxLoader.close();
            console.info(error);
          }
        );
      } else {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = 'Please select Specifications.';
        this.errNotification.open();
      }
    });

  }; //render toolbar ends


  setGridSource() {
    this.accessorySource =
      {
        datatype: 'json',
        datafields: [
          { name: 'accessoriseId', type: 'string', map: 'id' },
          { name: 'accessoriesName', type: 'string' },
          { name: 'mappingUnit', type: 'string' },
          { name: 'remarks', type: 'string' },
        ],
        id: 'id',
        localdata: [],
      }

    this.accessoryDataAdapter = new jqx.dataAdapter(this.accessorySource);

    this.accessoryColumns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: '', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['ACCESSORIES'], datafield: 'id', displayfield: 'accessoriesName', columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      { text: this.transData['UNIT'], datafield: 'mappingUnit', columntype: 'textbox', filterable: false, editable: false, width: '120' },
      { text: this.transData['REMARKS'], datafield: 'remarks', columntype: 'textbox', filterable: false, editable: true }
    ];

    this.specificationSource =
      {
        datatype: 'json',
        datafields: [
          { name: 'specificationId', type: 'string', map: 'id' },
          { name: 'specificationName', type: 'string' },
          { name: 'mappingUnit', type: 'string' },
          { name: 'remarks', type: 'string' },
        ],
        id: 'id',
        pagesize: 20,
        localdata: [],
      }

    this.specificationDataAdapter = new jqx.dataAdapter(this.specificationSource);

    this.specificationColumns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: '', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['SPECIFICATION'], datafield: 'specificationId', displayfield: 'specificationName', columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      { text: this.transData['UNIT'], datafield: 'mappingUnit', columntype: 'textbox', filterable: false, editable: false, width: '120' },
      { text: this.transData['REMARKS'], datafield: 'remarks', columntype: 'textbox', filterable: false, editable: true }
    ];

  }


  saveItemInsurance(post) {
    let formData = this.seForm.value;

    let itemNo= formData['itemCode'];
    if (post && formData['itemCode']) {
      this.jqxLoader.open();
      this.iis.update(itemNo,post).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
          this.jqxLoader.close();
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please Enter All Fields';
      this.errNotification.open();
    }
  }

  fSelected:Array<File> = [];
  url:any;

  fileEventS(event: any) {
    this.fSelected = [];
    for (var i = 0; i < event.target.files.length; i++) {
      this.fSelected.push(event.target.files[i]);
    }
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: any) => {
        this.url = event.target.result;
      }

      reader.readAsDataURL(event.target.files[0]);
    }
  }

  uploadImage(formData){
    let itemForm = this.seForm.value;
    let itemNo = itemForm['itemCode'];

    if (this.fSelected && itemForm['itemCode']) {
      this.jqxLoader.open();
      this.ssns.uploadFileCreate(this.fSelected,itemNo).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
            this.uploadForm.reset();
          }
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
          this.jqxLoader.close();
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please Select Item Before Uploading Image';
      this.errNotification.open();
    }
  }

  saveInsurance(post) {
    let itemForm = this.seForm.value;
    let itemNo = itemForm['itemCode'];
    if (post && itemNo) {
      this.jqxLoader.open();
      this.iws.update(itemNo,post).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
          this.jqxLoader.close();
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please Enter All Fields';
      this.errNotification.open();
    }
  }



  /**
   * Bill Sundry
   */

  // clearBillSundryFormArray = (formArray: FormArray) => {
  //   this.billSundryForm.controls['billSundry']['controls'] = [];
  // }

  // addItem() {
  //   const control1 = <FormArray>this.billSundryForm.controls['billSundry'];
  //   control1.push(this.initBillSundryItems());
  // }

  setupBillSundry(itemNo) {
    this.grns.getBillSundryByItemNo(itemNo).subscribe(
      response => {
        this.billSundryForm.reset();
        let billSundryData: any = response && response[0] || [];
        this.billSundryForm.patchValue(billSundryData);
      },
      error => {
        console.log(error);
      }
    )
  }

  saveBillSundry(formData) {
    let itemForm = this.seForm.value;
    let itemNo = itemForm['itemCode'];
    if(formData && itemNo){
      this.jqxLoader.open();
      this.grns.updateBillSundryByItemNo(itemNo,formData).subscribe(
        response => {

          if (response['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = response['message'];
            this.msgNotification.open();
            this.billSundryForm.reset();
          }

          if (response['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = response['error']['message'];
            this.errNotification.open();
          }

          this.jqxLoader.close();
        },
        error => {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = 'Error Occured';
          this.errNotification.open();
          this.jqxLoader.close();
        }
      );
    }else{
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please Select Item Before saving Bill Sundry';
      this.errNotification.open();
    }
  }

}
