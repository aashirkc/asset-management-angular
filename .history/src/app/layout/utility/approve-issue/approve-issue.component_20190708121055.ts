import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IssueApproveService,CurrentUserService, DateFormatService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-approve-issue',
  templateUrl: './approve-issue.component.html',
  styleUrls: ['./approve-issue.component.scss']
})
export class ApproveIssueComponent implements OnInit {
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('input1') inputEl: ElementRef;
  @ViewChild('myWindow') myWindow: jqxWindowComponent;
  @ViewChild('groupGrid') groupGrid: jqxGridComponent;
  @ViewChild('orderGrid') orderGrid: jqxGridComponent;


  approveReqForm: FormGroup;
  gridSource: any;
  gridDataAdapter: any;
  gridColumns: any = [];
  orderSource: any;
  orderDataAdapter: any;
  orderColumns: any = [];
  branchAdapter: any = [];
  editrow: number = -1;
  showDetailsGrid: boolean = false;
  userData: any = {};

  constructor(
    private fb: FormBuilder,
    private ips: IssueApproveService,
    private cus: CurrentUserService,
    private dfs: DateFormatService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
    this.userData = this.cus.getTokenData();
  }

  createForm() {
    this.approveReqForm = this.fb.group({
      'approveDate': [null, Validators.required],
      'approveBy': [null, Validators.required]
    });
  }

  transData:any;
  getTranslation(){
    this.translate.get(['SN','ISSUELIST','ITEM','NUMBER', 'ISSUEDATE','ITEMNAME','ASSETCODE','ISSUEBY','RECEIVEBRANCH','RECEIVEPERSON','RECEIVESITE','REMARKS']).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  ngOnInit() {
    this.ips.index({}).subscribe(
      response => {
        this.branchAdapter = response;
        this.gridSource.localdata = response;
        this.groupGrid.updatebounddata();
        this.groupGrid.updatebounddata();
        if (response[0]['error']) {
          this.gridSource.localdata = [];
          this.groupGrid.updatebounddata();
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response[0]['error'];
          this.errNotification.open();
        }
      },
      error => {
        console.log(error)
      });

    this.gridSource =
      {
        datatype: 'json',
        datafields: [
          { name: 'sn', type: 'string' },
          { name: 'assetCode', type: 'string' },
          { name: 'issueBy', type: 'string' },
          { name: 'issueDate', type: 'string' },
          { name: 'itemName', type: 'string' },
          { name: 'itemNo', type: 'string' },
          { name: 'receiveBranch', type: 'string' },
          { name: 'receivePerson', type: 'string' },
          { name: 'receiveSite', type: 'string' },
          { name: 'remarks', type: 'string' },
        ],
        id: 'sn',
        localdata: [],
      };

    this.gridDataAdapter = new jqx.dataAdapter(this.gridSource);

    this.gridColumns =
      [
        {
          text: this.transData['SN'], sortable: false, filterable: false, editable: false,
          groupable: false, draggable: false, resizable: false,
          datafield: '', columntype: 'number', width: 50,
          cellsrenderer: function (row, column, value) {
            return "<div style='margin:4px;'>" + (value + 1) + "</div>";
          }
        },
        { text: this.transData['ISSUEDATE'], datafield: 'issueDate', },
        { text: this.transData['ITEM'] + ' ' + this.transData['NUMBER'], datafield: 'itemNo' },
        { text: this.transData['ASSETCODE'], datafield: 'assetCode' },
        { text: this.transData['ITEMNAME'], datafield: 'itemName' },
        { text: this.transData['ISSUEBY'], datafield: 'issueBy', width: 250 },
        { text: this.transData['RECEIVEBRANCH'], datafield: 'receiveBranch' },
        { text: this.transData['RECEIVEPERSON'], datafield: 'receivePerson' },
        { text: this.transData['RECEIVESITE'], datafield: 'receiveSite' },
        { text: this.transData['REMARKS'], datafield: 'remarks' }
      ];
  }

  gridRenderToolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';
    let buttonContainer3 = document.createElement('div');
    buttonContainer3.id = 'buttonContainer3';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.innerHTML = "<b>"+ this.transData['ISSUELIST'] +":</b>";
    container.appendChild(buttonContainer3);
    toolbar[0].appendChild(container);
  };
  ngAfterViewInit() {
     this.dfs.currentDate().subscribe((res)=>{
      let data = res[0];
      setTimeout(() => {
        this.approveReqForm.controls['approveDate'].setValue(data['BS_DATE']);
        this.approveReqForm.get('approveDate').markAsTouched();
        this.approveReqForm.controls['approveBy'].setValue(this.userData['user'].userName);
        this.approveReqForm.get('approveBy').markAsTouched();
      }, 100);
    },(error)=>{
      console.info(error);
    });
  }

  approve(formData) {
    let id = this.groupGrid.getselectedrowindexes();;
    let ids = [];
    for (let i = 0; i < id.length; i++) {
      let dataRecord = this.groupGrid.getrowdata(Number(id[i]));
      let dt = {};
      dt['itemNo'] = dataRecord['itemNo'];
      dt['sn'] = dataRecord['sn'];
      ids.push(dt);
    }
    this.jqxLoader.open();
    formData['array'] = ids;
    formData['status'] = 'Approve';
    if (formData['array'].length > 0) {
      this.ips.storeAprove(formData).subscribe(
        result => {
          if (result['message']) {
            this.groupGrid.clearselection();
            this.groupGrid.updatebounddata();
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          this.jqxLoader.close();
          if (result['error']) {
            this.groupGrid.clearselection();
            this.groupGrid.updatebounddata();
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      this.jqxLoader.close();
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = "Please Select at least one item";
      this.errNotification.open();
    }
  }

  reject(formData) {
    let id = this.groupGrid.getselectedrowindexes();;
    let ids = [];
    for (let i = 0; i < id.length; i++) {
      let dataRecord = this.groupGrid.getrowdata(Number(id[i]));
      let dt = {};
      dt['itemNo'] = dataRecord['itemNo'];
      dt['sn'] = dataRecord['sn'];
      ids.push(dt);
    }
    this.jqxLoader.open();
    formData['array'] = ids;
    formData['status'] = 'Reject';
    if (formData['array'].length > 0) {
      this.ips.storeAprove(formData).subscribe(
        result => {
          if (result['message']) {
            this.groupGrid.clearselection();
            this.groupGrid.updatebounddata();
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          this.jqxLoader.close();
          if (result['error']) {
            this.groupGrid.clearselection();
            this.groupGrid.updatebounddata();
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      this.jqxLoader.close();
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = "Please Select at least one item";
      this.errNotification.open();
    }
  }

}
