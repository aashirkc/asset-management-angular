import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { FinancialYearService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-financial-year',
  templateUrl: './financial-year.component.html',
  styleUrls: ['./financial-year.component.scss']
})
export class FinancialYearComponent implements OnInit {
  fyForm: FormGroup;
  days: Array<any> = ['01', '02', '03', '04', '05', '06', '07', '08', '09', 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32];
  months: Array<any> = ['01', '02', '03', '04', '05', '06', '07', '08', '09', 10, 11, 12];
  years: Array<any> = [];
  fyStartBs;
  fyEndBs;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  deleteRowIndexes:Array<any> = [];

  rules:any = [];
  active: any =[
      {
         'name':'y'
      },
      {
        'name':'n'
     }
  ]

  transData:any;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;

  constructor(
    private fb: FormBuilder,
    private fy: FinancialYearService,
    private translate: TranslateService
  ) {
    this.createForm();

    let cdate = new Date();
    for (let i = cdate.getFullYear() + 57; i > cdate.getFullYear() - 1; i--) {
      this.years.push(i);
    }
    this.getTranslation();
  }

  ngOnInit() {
    this.loadGrid();
  }

  getTranslation(){
    this.translate.get(['SN','SYMBOL','START','END','FINANCIALYEAR',"ACTIVE","ACTION","UPDATE","DELETESELECTED","RELOAD"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  loadGridData() {
    this.jqxLoader.open();
    this.fy.index({}).subscribe((res) => {
        console.log(res)
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        this.source.localdata = res;
      }
      this.myGrid.updatebounddata();
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  createForm() {
    this.fyForm = this.fb.group({
      'fy': ['', Validators.required],
      'fySymbol': ['', Validators.required],
      'fyStartBs': [''],
      'fyEndBs': [''],
      'active': [''],
      'fys_day': ['', Validators.required],
      'fys_month': ['', Validators.required],
      'fys_year': ['', Validators.required],
      'fye_day': ['', Validators.required],
      'fye_month': ['', Validators.required],
      'fye_year': ['', Validators.required]
    });
  }
  ngAfterViewInit() {
    this.loadGridData();
    this.fyForm.valueChanges.subscribe(val => {
      if (val['fys_year'] && val['fys_month'] && val['fys_day']) {
        this.fyStartBs = val['fys_year'] + '-' + val['fys_month'] + '-' + val['fys_day'];

      }
      if (val['fye_year'] && val['fye_month'] && val['fye_day']) {
        this.fyEndBs = val['fye_year'] + '-' + val['fye_month'] + '-' + val['fye_day'];
      }
    });
  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'fy', type: 'string' },
          { name: 'fySymbol', type: 'string' },
          { name: 'fyStartBs', type: 'string' },
          { name: 'fyEndBs', type: 'string' },
          { name: 'active', type: 'string' },
        ],
        id: 'id',
        localdata: [],
        pagesize:20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: '', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['FINANCIALYEAR'], datafield: 'fy', columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      { text: this.transData['FINANCIALYEAR'] + ' '+ this.transData['SYMBOL'], datafield: 'fySymbol', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['FINANCIALYEAR'] + ' '+ this.transData['START'], datafield: 'fyStartBs', columntype: 'textbox', filtercondition: 'starts_with' },
      {
        text: this.transData['FINANCIALYEAR'] + ' '+ this.transData['END'], datafield: 'fyEndBs', columntype: 'textbox', filtercondition: 'starts_with'
      },
      { text: this.transData['ACTIVE'], datafield: 'active', columntype: 'combobox', filtercondition: 'starts_with' ,
      createeditor: (row: number, column: any, editor: any): void => {
        editor.jqxComboBox({
          source: this.active,
          valueMember: "name",
          displayMember: "name"
        });
      } },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 85, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['UPDATE'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          console.log('update1')
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          console.log(dataRecord)
          this.jqxLoader.open();
          console.log('update2')
          this.fy.update(dataRecord['uid'], dataRecord).subscribe(
            result => {
                console.log(result)
              this.jqxLoader.close();
              if (result['message']) {
                let messageDiv: any = document.getElementById('message');
                console.log('update3')
                messageDiv.innerText = result['message'];
                this.msgNotification.open();
                this.loadGridData();
              }
              if (result['error']) {
                let messageDiv: any = document.getElementById('error');
                console.log(result['error'])
                messageDiv.innerText = result['error']['message'];
                this.errNotification.open();
              }
            },
            error => {
              this.jqxLoader.close();
              console.info(error);
            }
          );

        }
      }
    ];
    this.columngroups =
      [
        { text: 'Actions', align: 'center', name: 'action' },
      ];

  }
  rowChange(event:any){
    this.deleteRowIndexes.push(event.args.rowindex);
  }
  rowUnChange(event:any){
    let index = this.deleteRowIndexes.indexOf(event.args.rowindex);
    if(index>-1){
      this.deleteRowIndexes.splice(index,1);
    }

  }
  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETESELECTED'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 150, value: '<i class="fa fa-refresh fa-fw"></i> ' + this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.deleteRowIndexes;
      let ids = [];
      let rowscount = this.myGrid.getdatainformation().rowscount;
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myGrid.getrowdata(Number(id[i]));
        ids.push(dataRecord['uid']);
        let testId = this.myGrid.getrowid(Number(id[i]));
        this.myGrid.deleterow(testId);
      }

      if (ids.length > 0 && ids.length <= parseFloat(rowscount)) {
        if (confirm("Are you sure? You Want to delete")) {
          this.jqxLoader.open();
          this.fy.destroy( ids ).subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.myGrid.clearselection();
              this.deleteRowIndexes = [];
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.loadGridData();
            }
            if (result['error']) {
              this.myGrid.clearselection();
              this.deleteRowIndexes = [];
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
              this.loadGridData();
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = 'Please select some item to delete';
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      this.loadGridData();
    });

  }; //render toolbar ends

  saveBtn(post) {

    post['fyStartBs'] = this.fyStartBs;
    post['fyEndBs'] = this.fyEndBs;
    this.jqxLoader.open();
    this.fy.store(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.loadGridData();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }

}
