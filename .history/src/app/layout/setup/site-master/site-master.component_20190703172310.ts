import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { SiteMasterService, BranchMasterService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-site-master',
  templateUrl: './site-master.component.html',
  styleUrls: ['./site-master.component.scss']
})
export class SiteMasterComponent implements OnInit {
  siteForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  deleteRowIndexes:Array<any> = [];
  branchAdapter:Array<any> = [];

  rules: any = [];

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;

   constructor(
    private fb: FormBuilder,
    private sms: SiteMasterService,
    private bms: BranchMasterService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
  }

  ngOnInit() {
    this.loadGrid();
  }

  transData:any;
  getTranslation(){
    this.translate.get(['SN','SITE','NAME','CODE',"CONTACTPERSON","CONTACTNO","BRANCH","WARD", "STATE", "MUNICIPALITY", "DISTRICT","LOCATION", "DELETESELECTED","ACTION","UPDATE","RELOAD"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  loadGridData() {
    this.jqxLoader.open();
    this.sms.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        this.source.localdata = res;
      }
      this.jqxLoader.close();
      this.myGrid.updatebounddata();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }
  createForm() {
    this.siteForm = this.fb.group({
      'sideCode': ['', Validators.required],
      'sideName': ['', Validators.required],
      'contactNo': ['', Validators.required],
      'state': ['', Validators.required],
      'district': ['', Validators.required],
      'municipality': ['', Validators.required],
      'ward': ['', Validators.required],
      'location': ['', Validators.required],
      'contactPersion': ['', Validators.required],
      'branch': ['', Validators.required],
    });
  }
  ngAfterViewInit() {
    this.loadGridData();
    this.bms.index({}).subscribe((res)=>{
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.branchAdapter = [];
      } else {
        this.branchAdapter = res;
      }
    },(error)=>{
      console.info(error);
    });
  }


  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'sideCode', type: 'string' },
          { name: 'sideName', type: 'string' },
          { name: 'contactNo', type: 'string' },
          { name: 'state', type: 'string' },
          { name: 'district', type: 'string' },
          { name: 'municipality', type: 'string' },
          { name: 'ward', type: 'string' },
          { name: 'location', type: 'string' },
          { name: 'contactPersion', type: 'string' },
          { name: 'branch', type: 'string' },
        ],
        id: 'id',
        pagesize:20,
        localdata: [],
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);

    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: '', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['SITE'] + ' ' +this.transData['CODE'], datafield: 'sideCode', columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      { text:  this.transData['SITE'] + ' ' +this.transData['NAME'], datafield: 'sideName', columntype: 'textbox', filtercondition: 'starts_with' },
      { text:  this.transData['CONTACTNO'], datafield: 'contactNo', columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['STATE'], datafield: 'state', columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['DISTRICT'], datafield: 'district', columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['MUNICIPALITY'], datafield: 'municipality', columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['WARD'], datafield: 'ward', columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['LOCATION'], datafield: 'location', columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['CONTACTPERSON'], datafield: 'contactPersion', columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['BRANCH'], datafield: 'branch', displayfield:'branch',columntype: 'combobox', filtercondition: 'starts_with',
      createeditor: (row: number, column: any, editor: any): void => {
        editor.jqxComboBox({
          source: this.branchAdapter,
          valueMember: "branchCode",
          displayMember: "branchName"
        });
      } },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 85, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['UPDATE'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          console.log(dataRecord)
          this.jqxLoader.open();
          this.sms.update(dataRecord['uid'], dataRecord).subscribe(
            result => {
              this.jqxLoader.close();
              if (result['message']) {
                this.myGrid.clearselection();
                this.deleteRowIndexes = [];
                let messageDiv: any = document.getElementById('message');
                messageDiv.innerText = result['message'];
                this.msgNotification.open();
                this.loadGridData();
              }
              if (result['error']) {
                this.myGrid.clearselection();
                this.deleteRowIndexes = [];
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = result['error']['message'];
                this.errNotification.open();
                this.loadGridData();
              }
            },
            error => {
              this.jqxLoader.close();
              console.info(error);
            }
          );

        }
      }
    ];
    this.columngroups =
      [
        { text: 'Actions', align: 'center', name: 'action' },
      ];

  }
  rowChange(event:any){
    this.deleteRowIndexes.push(event.args.rowindex);
  }
  rowUnChange(event:any){
    let index = this.deleteRowIndexes.indexOf(event.args.rowindex);
    if(index>-1){
      this.deleteRowIndexes.splice(index,1);
    }

  }
  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETESELECTED'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 80, value: '<i class="fa fa-refresh fa-fw"></i> '+ this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.deleteRowIndexes;
      let ids = [];
      let rowscount = this.myGrid.getdatainformation().rowscount;
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myGrid.getrowdata(Number(id[i]));
        console.log(dataRecord)
        ids.push(dataRecord['uid']);
        let testId = this.myGrid.getrowid(Number(id[i]));
        this.myGrid.deleterow(testId);
      }
      if (ids.length > 0 && ids.length <= parseFloat(rowscount)) {
        if (confirm("Are you sure? You Want to delete")) {
          this.jqxLoader.open();
          this.sms.destroy(ids ).subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.loadGridData();
            }
            if (result['error']) {
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = 'Please select some item to delete';
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      this.loadGridData();
    });

  }; //render toolbar ends

  saveBtn(post) {
    this.jqxLoader.open();
    this.sms.store(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.loadGridData();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
}
