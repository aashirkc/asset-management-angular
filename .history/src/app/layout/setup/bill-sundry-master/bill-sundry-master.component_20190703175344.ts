import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { BillSundryMasterService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-bill-sundry-master',
  templateUrl: './bill-sundry-master.component.html',
  styleUrls: ['./bill-sundry-master.component.scss']
})
export class BillSundryMasterComponent implements OnInit {

  @ViewChild('bsmGrid') bsmGrid: jqxGridComponent;
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

  /**
  * Global Variable decleration
  */
  bsmForm: FormGroup;

  bsmSource: any;
  bsmDataAdapter: any;
  bsmColumns: any;

  editrow: number = -1;
  deleteRowIndexes: Array<any> = [];

  constructor(
    private fb: FormBuilder,
    private bsms: BillSundryMasterService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
  }

  createForm() {
    this.bsmForm = this.fb.group({
      'code': [null, Validators.required],
      'name': [null, Validators.required],
    });
  }

  transData:any;
  getTranslation(){
    this.translate.get(['SN','CODE', 'NAME',"ACTION","UPDATE","RELOAD","DELETESELECTED"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  ngOnInit() {

    this.bsmSource =
      {
        datatype: 'json',
        datafields: [
          { name: 'code', type: 'string' },
          { name: 'name', type: 'string' },
        ],
        pagesize:20,
        localdata: [],
      }

    this.bsmDataAdapter = new jqx.dataAdapter(this.bsmSource);

    this.bsmColumns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: '', columntype: 'number', width: 80,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['CODE'], datafield: 'code', columntype: 'textbox', editable: true,  filtercondition: 'starts_with' },
      { text: this.transData['NAME'], datafield: 'name', columntype: 'textbox',  filtercondition: 'starts_with' },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 120, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['UPDATE'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.bsmGrid.getrowdata(this.editrow);
          this.jqxLoader.open();
          this.bsms.update(dataRecord['uid'], dataRecord).subscribe(
            result => {
              this.jqxLoader.close();
              if (result['message']) {
                let messageDiv: any = document.getElementById('message');
                messageDiv.innerText = result['message'];
                this.msgNotification.open();
                this.getBillSundry();
              }
              if (result['error']) {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = result['error']['message'];
                this.errNotification.open();
              }
            },
            error => {
              this.jqxLoader.close();
              console.info(error);
            }
          );

        }
      }
    ];
  }

  rowChange(event: any) {
    this.deleteRowIndexes.push(event.args.rowindex);
  }
  rowUnChange(event: any) {
    let index = this.deleteRowIndexes.indexOf(event.args.rowindex);
    if (index > -1) {
      this.deleteRowIndexes.splice(index, 1);
    }

  }
  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETESELECTED'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 80, value: '<i class="fa fa-refresh fa-fw"></i> '+ this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.deleteRowIndexes;
      let ids = [];
      let rowscount = this.bsmGrid.getdatainformation().rowscount;
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.bsmGrid.getrowdata(Number(id[i]));
        ids.push( dataRecord['uid'] );
        let testId = this.bsmGrid.getrowid(Number(id[i]));
        this.bsmGrid.deleterow(testId);
      }
      if (ids.length > 0 && ids.length <= parseFloat(rowscount)) {
        if (confirm("Are you sure? You Want to delete")) {
          this.jqxLoader.open();
          this.bsms.destroy( ids ).subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.bsmGrid.clearselection();
              this.deleteRowIndexes = [];
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.getBillSundry();
            }
            if (result['error']) {
              this.bsmGrid.clearselection();
              this.deleteRowIndexes = [];
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
              this.getBillSundry();
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = 'Please select some item to delete';
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      this.getBillSundry();
    });

  }; //render toolbar ends

  ngAfterViewInit() {
    this.getBillSundry();
  }

  getBillSundry() {
    this.bsms.index({}).subscribe(
      response => {

        if (response.length == 1 && response[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response[0].error;
          this.errNotification.open();
          this.bsmSource.localdata = [];
        } else {
          this.bsmSource.localdata = response;
        }
        this.bsmGrid.updatebounddata();
        console.log(response);

      },
      error => {
        console.log(error);
      }
    )
  }

  /**
   * Function triggered when save button is clicked
   * @param formData
   */
  save(formData) {
    console.log(formData);
    this.jqxLoader.open();
    this.bsms.store(formData).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
        this.getBillSundry();
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }

}
