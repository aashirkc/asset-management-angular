import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ChartOfItemsService } from '../../../shared';
import { jqxValidatorComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxvalidator';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxTreeGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxtreegrid';
import { jqxCheckBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcheckbox';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-chart-of-items',
  templateUrl: './chart-of-items.component.html',
  styleUrls: ['./chart-of-items.component.scss']
})
export class ChartOfItemsComponent implements OnInit {

  @ViewChild('groupTreeGrid') groupTreeGrid: jqxTreeGridComponent;
  @ViewChild('groupGrid') groupGrid: jqxGridComponent;
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jtransactCheck') transactCheck: jqxCheckBoxComponent;

  /**
   * Global Variable decleration
   */
  coiForm: FormGroup;
  treeSource: any;
  treeDataAdapter: any;
  treeColumns: any = [];
  gridSource: any;
  gridDataAdapter: any;
  gridColumns: any = [];
  trasValue: any;
  selectedTreeGroupId: string;

  deleteRowIndexes: Array<any> = [];

  isChecked: boolean = false;
  selectedItem: string = '';

  constructor(
    private fb: FormBuilder,
    private cois: ChartOfItemsService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
  }

  transData: any;
  getTranslation() {
    this.translate.get(['SN', 'ITEM', 'NAME', 'CODE', 'GROUPNAME', "TRANSACTION", "DELETESELECTED",]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  /**
   * Create the form group 
   * with given form control name 
   */
  createForm() {
    this.coiForm = this.fb.group({
      'itemName': [null, Validators.required],
      'assetCode': [{ value: null, disabled: true }],
    });
  }

  ngOnInit() {
    localStorage.removeItem('selectedChard');
    this.treeSource =
      {
        dataType: "json",
        dataFields: [
          { name: 'itemCode', type: 'number' },
          { name: 'itemGroupCode', type: 'number' },
          { name: 'itemGroupName', type: 'string' },
          { name: 'itemName', type: 'string' },
        ],
        hierarchy:
          {
            keyDataField: { name: 'itemCode' },
            parentDataField: { name: 'itemGroupCode' }
          },
        id: 'itemCode',
        localdata: []
      };

    this.treeDataAdapter = new jqx.dataAdapter(this.treeSource);

    this.treeColumns =
      [
        { text: this.transData['ITEM'] + ' ' + this.transData['CODE'], dataField: 'itemCode', width: '130', filterable: false },
        { text: this.transData['ITEM'] + ' ' + this.transData['NAME'], dataField: 'itemName' },
      ];


    this.gridSource =
      {
        datatype: 'json',
        datafields: [
          { name: 'code', type: 'string' },
          { name: 'itemName', type: 'string' },
          { name: 'itemGroupCode', type: 'string' },
          { name: 'itemGroupName', type: 'string' },
          { name: 'trAccount', type: 'string' }
        ],
        id: 'itemCode',
        pagesize: 20,
        localdata: [],
      };

    this.gridDataAdapter = new jqx.dataAdapter(this.gridSource);

    this.gridColumns =
      [
        {
          text: this.transData['SN'], sortable: false, filterable: false, editable: false,
          groupable: false, draggable: false, resizable: false,
          datafield: '', columntype: 'number', width: 50,
          cellsrenderer: function (row, column, value) {
            return "<div style='margin:4px;'>" + (value + 1) + "</div>";
          }
        },
        { text: this.transData['ITEM'] + ' ' + this.transData['NAME'], displayfield: 'itemName', datafield: 'itemCode', width: 250 },
        { text: this.transData['Code'], displayfield: 'code', datafield: 'code' },
        { text: this.transData['ITEM'] + ' ' + this.transData['CODE'],  displayfield: 'itemCode' },
        { text: this.transData['TRANSACTION'], datafield: 'trAccount' },
      ];

  }


  ngAfterViewInit() {
    this.loadTreeData();
  }

  ngOnDestroy() {
    localStorage.removeItem('selectedChard');
  }

  Change($e) {
    this.isChecked = $e.args.checked;
    if (this.isChecked) {
      this.coiForm.get('assetCode').enable();
      // this.coiForm.get('assetCode').setValidators(Validators.required);
    } else {
      this.coiForm.get('assetCode').disable();
      // this.coiForm.get('assetCode').re
    }
  }

  /**
   * Row selected event in Tree Grid
   * @param $event
   */
  treeRowSelect($event) {
    // console.log($event);
    this.deleteRowIndexes = [];
    this.selectedTreeGroupId = $event.args['key'];
    this.treeRowSelectedData(this.selectedTreeGroupId);
    localStorage.setItem('selectedChard', JSON.stringify($event.args.row))
    this.selectedItem = $event && $event.args && $event.args.row && $event.args.row['itemName'];
  }

  /**
   * Get child Data of selected row in Tree Grid
   * and display the childern in Grid
   * @param groupId 
   */
  treeRowSelectedData(groupId) {
    this.groupGrid.clearselection();
    if (groupId) {
      this.cois.showChild(groupId).subscribe(
        response => {
          let TreeChild = response;
          if (response['length'] == 1 && response[0].error) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = response[0].error;
            this.errNotification.open();
            this.gridSource.localdata = [];
          } else if (TreeChild['length'] < 1) {
            this.gridSource.localdata = [];
          } else {
            this.gridSource.localdata = response;
          }
          this.groupGrid.updatebounddata();
        },
        error => {
          console.log(error);
        }
      )
    }
  }


  /**
   * Grid row checked event
   * @param event 
   */
  rowChange(event: any) {
    this.deleteRowIndexes.push(event.args.rowindex);
  }

  /**
   * Grid row unchecked event
   * @param event 
   */
  rowUnChange(event: any) {
    let index = this.deleteRowIndexes.indexOf(event.args.rowindex);
    if (index > -1) {
      this.deleteRowIndexes.splice(index, 1);
    }

  }

  /**
   * Child Grid Toolbar
   */
  gridRenderToolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer3 = document.createElement('div');

    buttonContainer3.id = 'buttonContainer3';

    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETESELECTED'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.deleteRowIndexes;
      let ids = [];
      let rowscount = this.groupGrid.getdatainformation().rowscount;
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.groupGrid.getrowdata(Number(id[i]));
        ids.push(dataRecord['itemCode']);
        let testId = this.groupGrid.getrowid(Number(id[i]));
        this.groupGrid.deleterow(testId);
      }
      //Load Grid After Item Have been deleted.
      this.treeRowSelectedData(this.selectedTreeGroupId);

      if (ids.length > 0 && ids.length <= parseFloat(rowscount)) {
        if (confirm("Are you sure? You Want to delete")) {
          this.jqxLoader.open();
          this.cois.destroy('(' + ids + ')').subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.groupGrid.clearselection();
              this.deleteRowIndexes = [];
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
            }
            if (result['error']) {
              this.groupGrid.clearselection();
              this.deleteRowIndexes = [];
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
            }

            // Reload Tree grid after item deletion
            this.loadTreeData();

            //Reload grid after item deletion
            this.treeRowSelectedData(this.selectedTreeGroupId);

          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
        this.groupGrid.updatebounddata();
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = 'Please select some item to delete';
        this.errNotification.open();
      }
    })

  }; //render toolbar ends


  /**
   * Load Tree Grid Data
   */
  loadTreeData() {
    this.cois.index({}).subscribe(
      res => {
        if (res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.errNotification.open();
          this.loadTreeSource([]);
        } else {
          this.loadTreeSource(res);
          
        }
      },
      error => {
        console.log(error);
      }
    )
  }

  /**
   *  Load Tree Grid Source with Data
   * @param data 
   */
  loadTreeSource(data) {
    this.treeSource.localdata = data;
    this.groupTreeGrid.updateBoundData();
    let datat = localStorage.getItem('selectedChard');
    this.groupTreeGrid.expandAll();
    if(datat){
      let selectedData = JSON.parse(datat);
      this.groupTreeGrid.selectRow(selectedData['itemCode']);
    }
    
  }

  /**
   * Function triggered when save button is clicked
   * @param formData 
   */
  save(formData) {
    let treeGridData = this.groupTreeGrid.getSelection();
    if (this.isChecked) {
      formData['trAccount'] = 'Y';
    } else {
      formData['trAccount'] = 'N';
    }
    let treeData = {};
    let data = localStorage.getItem('selectedChard');
    let selectedData = JSON.parse(data);
    if (selectedData) {
      formData['itemGroupCode'] = selectedData['itemCode'];
      formData['itemGroupName'] = selectedData['itemName'];
    }
    if (formData['itemGroupCode']) {
      this.jqxLoader.open();
      this.cois.store(formData).subscribe(
        result => {
          if (result['message']) {
            let groupId  = selectedData['itemCode'];
            this.treeRowSelectedData(groupId);
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
            this.loadTreeData();
            this.coiForm.reset();
            this.gridSource.localdata = [];
            this.groupGrid.updatebounddata();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please Select Group Name';
      this.errNotification.open();
    }
  }

}
