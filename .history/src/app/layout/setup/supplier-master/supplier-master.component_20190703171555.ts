import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { SupplierMasterService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-supplier-master',
  templateUrl: './supplier-master.component.html',
  styleUrls: ['./supplier-master.component.scss']
})
export class SupplierMasterComponent implements OnInit {
  supplierForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  deleteRowIndexes:Array<any> = [];

  rules:any = [];

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;

   constructor(
    private fb: FormBuilder,
    private sms: SupplierMasterService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
  }

  ngOnInit() {
    this.loadGrid();
  }

  transData:any;
  getTranslation(){
    this.translate.get(['SN','SUPPLIER','NAME','CODE','TYPE',"ADDRESS","TELEPHONENO","EMAILADDRESS","FAX","CONTACTPERSON","VATPANNO", "DELETESELECTED","ACTION","UPDATE","RELOAD"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  loadGridData() {
    this.jqxLoader.open();
    this.sms.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        this.source.localdata = res;
      }
      this.jqxLoader.close();
      this.myGrid.updatebounddata();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }
  createForm() {
    this.supplierForm = this.fb.group({
      'supplierCode': ['', Validators.required],
      'supplierType': ['', Validators.required],
      'supplierName': ['', Validators.required],
      'address': ['', Validators.required],
      'teliphoneNumber': ['', Validators.required],
      'emailAddress': ['', Validators.required],
      'faxNo': ['', Validators.required],
      'contactPerson': ['', Validators.required],
      'vatPanNo': ['', Validators.required],
    });
  }
  ngAfterViewInit() {
    this.loadGridData();
  }


  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'sn', type: 'string' },
          { name: 'supplierCode', type: 'string' },
          { name: 'supplierType', type: 'string' },
          { name: 'supplierName', type: 'string' },
          { name: 'address', type: 'string' },
          { name: 'teliphoneNumber', type: 'string' },
          { name: 'emailAddress', type: 'string' },
          { name: 'faxNo', type: 'string' },
          { name: 'contactPerson', type: 'string' },
          { name: 'vatPanNo', type: 'string' },
        ],
        id: 'id',
        pagesize:20,
        localdata: [],
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);

    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: '', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['SUPPLIER'] + ' ' + this.transData['CODE'], datafield: 'supplierCode', columntype: 'textbox', editable: false, width:'100', filtercondition: 'starts_with' },
      { text: this.transData['SUPPLIER'] + ' ' + this.transData['NAME'], datafield: 'supplierName', columntype: 'textbox',  width:'150', filtercondition: 'starts_with' },
      { text: this.transData['SUPPLIER'] + ' ' + this.transData['TYPE'], datafield: 'supplierType', columntype: 'textbox', width:'150', filtercondition: 'starts_with' },
      { text: this.transData['ADDRESS'], datafield: 'address', columntype: 'textbox', width:'150', filtercondition: 'starts_with' },
      { text: this.transData['TELEPHONENO'], datafield: 'teliphoneNumber', columntype: 'textbox',width:'150', filtercondition: 'starts_with' },
      { text: this.transData['EMAILADDRESS'], datafield: 'emailAddress', columntype: 'textbox',width:'150', filtercondition: 'starts_with' },
      { text: this.transData['FAX'], datafield: 'faxNo', columntype: 'textbox',width:'100', filtercondition: 'starts_with' },
      { text: this.transData['CONTACTPERSON'], datafield: 'contactPerson', width:'150', columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['VATPANNO'], datafield: 'vatPanNo', width:'100', columntype: 'textbox', filtercondition: 'starts_with' },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 85, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['UPDATE'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          console.log(dataRecord)
          this.jqxLoader.open();
          this.sms.update(dataRecord['uid'], dataRecord).subscribe(
            result => {
              this.jqxLoader.close();
              if (result['message']) {
                let messageDiv: any = document.getElementById('message');
                messageDiv.innerText = result['message'];
                this.msgNotification.open();
                this.loadGridData();
              }
              if (result['error']) {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = result['error']['message'];
                this.errNotification.open();
              }
            },
            error => {
              this.jqxLoader.close();
              console.info(error);
            }
          );

        }
      }
    ];
    this.columngroups =
      [
        { text: 'Actions', align: 'center', name: 'action' },
      ];

  }
  rowChange(event:any){
    this.deleteRowIndexes.push(event.args.rowindex);
  }
  rowUnChange(event:any){
    let index = this.deleteRowIndexes.indexOf(event.args.rowindex);
    if(index>-1){
      this.deleteRowIndexes.splice(index,1);
    }

  }
  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETESELECTED'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 80, value: '<i class="fa fa-refresh fa-fw"></i> '+ this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.deleteRowIndexes;
      let ids = [];
      let rowscount = this.myGrid.getdatainformation().rowscount;
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myGrid.getrowdata(Number(id[i]));
        console.log(dataRecord)
        ids.push(dataRecord['uid']);
        let testId = this.myGrid.getrowid(Number(id[i]));
        this.myGrid.deleterow(testId);
      }
      if (ids.length > 0 && ids.length <= parseFloat(rowscount)) {
        if (confirm("Are you sure? You Want to delete")) {
          this.jqxLoader.open();
          this.sms.destroy( ids ).subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.myGrid.clearselection();
              this.deleteRowIndexes = [];
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.loadGridData();
            }
            if (result['error']) {
              this.myGrid.clearselection();
              this.deleteRowIndexes = [];
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
              this.loadGridData();
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = 'Please select some item to delete';
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      this.loadGridData();
    });

  }; //render toolbar ends

  saveBtn(post) {
    this.jqxLoader.open();
    this.sms.store(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.loadGridData();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
}
