import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AssetAccessoriesMasterService, ChartOfItemsService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxInputComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxinput';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-asset-accessories-master',
  templateUrl: './asset-accessories-master.component.html',
  styleUrls: ['./asset-accessories-master.component.scss']
})
export class AssetAccessoriesMasterComponent implements OnInit {

  assetAccessoriesForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  deleteRowIndexes: Array<any> = [];
  itemCodes: any;
  itemFocus:boolean =  false;
  rules:any = [];

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('myInput') myInput: jqxInputComponent;

  constructor(
    private fb: FormBuilder,
    private aams: AssetAccessoriesMasterService,
    private coi: ChartOfItemsService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
  }

  ngOnInit() {
    this.loadGrid();
  }

  transData:any;
  getTranslation(){
    this.translate.get(['SN','ACCESSORIESNAME','CODE', 'ITEM','MAPPINGUNIT',"DELETESELECTED","ACTION","UPDATE","RELOAD"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  loadGridData() {
    this.jqxLoader.open();
    this.aams.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        this.source.localdata = res;
      }
      this.jqxLoader.close();
      this.myGrid.updatebounddata();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  createForm() {
    this.assetAccessoriesForm = this.fb.group({
      'itemCode': ['', Validators.required],
      'accessoriesName': ['', Validators.required],
      'mappingUnit': ['', Validators.required],
    });
  }
  ngAfterViewInit() {
    this.loadGridData();
  }

  itemFilter(searchPr) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemCode').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(' ', '');
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocus = true;
        this.coi.show(searchString.replace(/\s/g, "")).subscribe(
          response => {
            this.itemCodes = [];
            this.itemCodes = response;
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.itemFocus = false;
      }
    }

  }
  itemListSelected(selectedValue) {
    if (selectedValue) {
      this.assetAccessoriesForm.controls['itemCode'].setValue(selectedValue);
    }
  }
  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'string' },
          { name: 'itemCode', type: 'string' },
          { name: 'accessoriesName', type: 'string' },
          { name: 'mappingUnit', type: 'string' },
        ],
        id: 'id',
        localdata: [],
        pagesize: 20,
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);

    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: 'id', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['ITEM'] + ' ' + this.transData['CODE'], datafield: 'itemCode', columntype: 'textbox', editable: false, filtercondition: 'starts_with',
      cellsrenderer: function (row, column, value) {
        return "<div style='margin:4px;'>" + (value['itemName']) + "</div>";
      } },
      { text: this.transData['ACCESSORIESNAME'], datafield: 'accessoriesName', columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['MAPPINGUNIT'], datafield: 'mappingUnit', columntype: 'textbox', filtercondition: 'starts_with' },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 85, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['UPDATE'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          this.jqxLoader.open();
          this.aams.update(dataRecord['id'], dataRecord).subscribe(
            result => {
              this.jqxLoader.close();
              if (result['message']) {
                let messageDiv: any = document.getElementById('message');
                messageDiv.innerText = result['message'];
                this.msgNotification.open();
                this.loadGridData();
              }
              if (result['error']) {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = result['error']['message'];
                this.errNotification.open();
              }
            },
            error => {
              this.jqxLoader.close();
              console.info(error);
            }
          );

        }
      }
    ];
    this.columngroups =
      [
        { text: 'Actions', align: 'center', name: 'action' },
      ];

  }
  rowChange(event: any) {
    this.deleteRowIndexes.push(event.args.rowindex);
  }
  rowUnChange(event: any) {
    let index = this.deleteRowIndexes.indexOf(event.args.rowindex);
    if (index > -1) {
      this.deleteRowIndexes.splice(index, 1);
    }

  }
  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETESELECTED'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 80, value: '<i class="fa fa-refresh fa-fw"></i> '+ this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.deleteRowIndexes;
      let ids = [];
      let rowscount = this.myGrid.getdatainformation().rowscount;
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myGrid.getrowdata(Number(id[i]));
        ids.push(dataRecord['id']);
        let testId = this.myGrid.getrowid(Number(id[i]));
        this.myGrid.deleterow(testId);
      }
      if (ids.length > 0 && ids.length <= parseFloat(rowscount)) {
        if (confirm("Are you sure? You Want to delete")) {
          this.jqxLoader.open();
          this.aams.destroy(ids ).subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.myGrid.clearselection();
              this.deleteRowIndexes = [];
              this.myGrid.updatebounddata();
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.loadGridData();
            }
            if (result['error']) {
              this.myGrid.clearselection();
              this.deleteRowIndexes = [];
              this.myGrid.updatebounddata();
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
              this.loadGridData();
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = 'Please select some item to delete';
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      this.loadGridData();
    });

  }; //render toolbar ends

  saveBtn(post) {
    this.jqxLoader.open();
    this.aams.store(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.loadGridData();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }

}
