import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { EmployeeDetailsService, SiteMasterService, BranchMasterService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.scss']
})
export class EmployeeDetailsComponent implements OnInit {
  employeeForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  deleteRowIndexes: Array<any> = [];
  branchAdapter: Array<any> = [];
  siteAdapter: Array<any> = [];

  rules: any = [];

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;

  constructor(
    private fb: FormBuilder,
    private eds: EmployeeDetailsService,
    private sms: SiteMasterService,
    private translate: TranslateService,
    private bms: BranchMasterService
  ) {
    this.createForm();
    
    this.getTranslation();
  }

  ngOnInit() {
  }

  transData:any;
  getTranslation(){
    this.translate.get(['SN','BRANCH','DELETE','RELOAD','EMPLOYEECODE',"MOBILE","EMAIL","ADDRESS","ACTIONS","FULLNAME","UPDATE","SITE"]).subscribe((translation: [string]) => {
      this.transData = translation;
      this.loadGrid();
    });
  }

  loadGridData(post) {
    this.jqxLoader.open();
    this.eds.index(post).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        this.source.localdata = res;
      }
      this.jqxLoader.close();
      this.myGrid.updatebounddata();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }
  createForm() {
    this.employeeForm = this.fb.group({
      'empCode': ['', Validators.required],
      'fullName': ['', Validators.required],
      'address': [''],
      'mobile': ['', Validators.required],
      'email': [''],
      'branch': ['', Validators.required],
      'site': [''],
    });
  }
  ngAfterViewInit() {
    this.bms.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.branchAdapter = [];
      } else {
        this.branchAdapter = res;
      }
    }, (error) => {
      console.info(error);
    });
  }

  branchChange() {
    let post = {};
    post['branch'] = this.employeeForm.controls['branch'].value;
    post['site'] = this.employeeForm.controls['site'].value
    this.loadGridData(post);
    if(post['branch']){
      this.loadSite(post);
    }
    
  }
  loadSite(post){
    this.jqxLoader.open();
    this.sms.index(post).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.siteAdapter = [];
        this.jqxLoader.close();
      } else {
        this.siteAdapter = res;
        this.jqxLoader.close();
      }
    }, (error) => {
      console.info(error);
    });
  }

  siteChange() {
    let post = {};
    post['branch'] = this.employeeForm.controls['branch'].value;
    post['site'] = this.employeeForm.controls['site'].value
    this.loadGridData(post);
  }


  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'sn', type: 'string' },
          { name: 'site', type: 'string' },
          { name: 'address', type: 'string' },
          { name: 'empCode', type: 'string' },
          { name: 'mobile', type: 'string' },
          { name: 'fullName', type: 'string' },
          { name: 'branch', type: 'string' },
          { name: 'email', type: 'string' },
        ],
        id: 'sn',
        pagesize: 20,
        localdata: [],
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);

    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: '', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['EMPLOYEECODE'], datafield: 'empCode', columntype: 'textbox', editable: true, filtercondition: 'starts_with', width: '150' },
      { text: this.transData['FULLNAME'], datafield: 'fullName', columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['ADDRESS'], datafield: 'address', columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['MOBILE'], datafield: 'mobile', columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['EMAIL'], datafield: 'email', columntype: 'textbox', filtercondition: 'starts_with', width: '200' },
      {
        text: this.transData['SITE'], datafield: 'site', columntype: 'combobox', filtercondition: 'starts_with',
        createeditor: (row: number, column: any, editor: any): void => {
          editor.jqxComboBox({
            source: this.siteAdapter,
            valueMember: "sideCode",
            displayMember: "sideName"
          });
        }
      },
      {
        text: this.transData['BRANCH'], datafield: 'branch', displayfield: 'branch', columntype: 'combobox', filtercondition: 'starts_with',
        createeditor: (row: number, column: any, editor: any): void => {
          editor.jqxComboBox({
            source: this.branchAdapter,
            valueMember: "branchCode",
            displayMember: "branchName"
          });
        }
      },
      {
        text: this.transData['ACTIONS'], datafield: 'Edit', sortable: false, filterable: false, width: 85, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['UPDATE'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          this.jqxLoader.open();
          this.eds.update(dataRecord['sn'], dataRecord).subscribe(
            result => {
              this.jqxLoader.close();
              if (result['message']) {
                this.myGrid.clearselection();
                this.deleteRowIndexes = [];
                let messageDiv: any = document.getElementById('message');
                messageDiv.innerText = result['message'];
                this.msgNotification.open();
                let post = {};
                post['branch'] = this.employeeForm.controls['branch'].value;
                post['site'] = this.employeeForm.controls['site'].value;
                this.loadGridData(post);
              }
              if (result['error']) {
                this.myGrid.clearselection();
                this.deleteRowIndexes = [];
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = result['error']['message'];
                this.errNotification.open();
                let post = {};
                post['branch'] = this.employeeForm.controls['branch'].value;
                post['site'] = this.employeeForm.controls['site'].value;
                this.loadGridData(post);
              }
            },
            error => {
              this.jqxLoader.close();
              console.info(error);
            }
          );

        }
      }
    ];
    this.columngroups =
      [
        { text: 'Actions', align: 'center', name: 'action' },
      ];

  }
  rowChange(event: any) {
    this.deleteRowIndexes.push(event.args.rowindex);
  }
  rowUnChange(event: any) {
    let index = this.deleteRowIndexes.indexOf(event.args.rowindex);
    if (index > -1) {
      this.deleteRowIndexes.splice(index, 1);
    }

  }
  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETE'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 150, value: '<i class="fa fa-refresh fa-fw"></i>'+ this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.deleteRowIndexes;
      let ids = [];
      let rowscount = this.myGrid.getdatainformation().rowscount;
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myGrid.getrowdata(Number(id[i]));
        ids.push("'" + dataRecord['sn'] + "'");
        let testId = this.myGrid.getrowid(Number(id[i]));
        this.myGrid.deleterow(testId);
      }
      if (ids.length > 0 && ids.length <= parseFloat(rowscount)) {
        if (confirm("Are you sure? You Want to delete")) {
          this.jqxLoader.open();
          this.eds.destroy('(' + ids + ')').subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              let post = {};
              post['branch'] = this.employeeForm.controls['branch'].value;
              post['site'] = this.employeeForm.controls['site'].value;
              this.loadGridData(post);
            }
            if (result['error']) {
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = 'Please select some item to delete';
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      let post = {};
      post['branch'] = this.employeeForm.controls['branch'].value;
      post['site'] = this.employeeForm.controls['site'].value;
      this.loadGridData(post);
    });

  }; //render toolbar ends

  saveBtn(post) {
    this.jqxLoader.open();
    this.eds.store(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          let post = {};
          this.loadGridData(post);
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }

}
