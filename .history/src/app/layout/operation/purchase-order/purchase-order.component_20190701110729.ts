import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RequisitionSlipService,CurrentUserService, ChartOfItemsService, BranchMasterService, DateFormatService, SupplierMasterService, PurchaseOrderService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
  selector: 'app-purchase-order',
  templateUrl: './purchase-order.component.html',
  styleUrls: ['./purchase-order.component.scss']
})
export class PurchaseOrderComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

  poForm: FormGroup;
  itemAdapter: any = [];
  itemFocus: any = [];
  supplierAdapter: any = [];
  branchAdapter: any = [];
  requisitionItemLength: number = 0;
  userData: any = {};
  unitAdapter: any = [
    { name: 'PCS', value: 'PCS' },
    { name: 'PKT', value: 'PKT' },
    { name: 'SET', value: 'SET' },
    { name: 'BOX', value: 'BOX' },
    { name: 'KG', value: 'KG' },
    { name: 'METER', value: 'MTR' },
  ];
  paymentTermAdapter: any = [
    {
      name: 'Cash/Cheque On Delivery',
      value: 'COD'
    },
    {
      name: 'Draft',
      value: 'Draft'
    }, {
      name: 'Letter of Credit',
      value: 'LOC'
    }
  ]

  constructor(
    private fb: FormBuilder,
    private rss: RequisitionSlipService,
    private cois: ChartOfItemsService,
    private bms: BranchMasterService,
    private dfs: DateFormatService,
    private sms: SupplierMasterService,
    private cus: CurrentUserService,
    private pos: PurchaseOrderService
  ) {
    this.createForm();
    this.itemAdapter[0] = [];
    this.itemFocus[0] = false;
    this.userData = this.cus.getTokenData();
  }

  createForm() {
    this.poForm = this.fb.group({
      'enterDate': [null, Validators.required],
      'orderType': [null, Validators.required],
      'supplier': [null, Validators.required],
      'deleveryPlace': [null, Validators.required],
      'paymentTerm': [null, Validators.required],
      'enterOrderBy': [null, Validators.required],
      'requisitionItems': this.fb.array([
        this.initReqItems(),
      ]),
    });
  }

  initReqItems() {
    return this.fb.group({
      requisitionNo: [''],
      itemCode: [null, Validators.required],
      specification: [null, Validators.required],
      orderQty: [null, Validators.required],
      rate: [null, Validators.required],
      unit: [null, Validators.required],
      inDeliveryDay: [''],
      remark: ['']
    });
  }

  ngOnInit() {
    this.sms.index({}).subscribe(
      response => {

        if (response.length == 1 && response[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response[0].error;
          this.errNotification.open();
          this.supplierAdapter = [];
        } else {
          this.supplierAdapter = response;
        }
      },
      error => {
        console.log(error);
      }
    );

    this.bms.index({}).subscribe(
      response => {
        this.branchAdapter = response;
      },
      error => {
        console.log(error);
      }
    )
    this.getRequisitionItems();
  }
  ngAfterViewInit() {
    this.dfs.currentDate().subscribe(
      response => {
        let dateData = response[0];
        setTimeout(() => {
          this.poForm.controls['enterDate'].setValue(dateData['BS_DATE']);
          this.poForm.get('enterDate').markAsTouched();
          this.poForm.controls['enterOrderBy'].setValue(this.userData['user'].userName);
          this.poForm.get('enterOrderBy').markAsTouched();
        }, 100);
      },
      error => {
        console.log(error);
      }
    )
  }

  getRequisitionItems() {
    this.pos.index({}).subscribe(
      response => {
        console.log(response);
        this.requisitionItemLength = response.length;
        for (let i = 0; i < response.length - 1; i++) {
          this.addItem();
        }
        this.poForm.get('requisitionItems').reset();
        this.poForm.get('requisitionItems').patchValue(response);
        this.poForm.get('requisitionItems').markAsTouched();
      },
      error => {
        console.log(error);
      }
    )
  }

  itemFilter(searchPr, index) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemCode').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(' ', '');
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocus[index] = true;
        this.cois.show(searchString).subscribe(
          response => {
            this.itemAdapter[index] = [];
            this.itemAdapter[index] = response;
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.itemFocus[index] = false;
      }
    }
  }

  itemListSelected(selectedValue, index) {
    if (selectedValue) {
      this.poForm.get('requisitionItems')['controls'][index].get('itemCode').patchValue(selectedValue);
    }
  }

  addItem() {
    const control1 = <FormArray>this.poForm.controls['requisitionItems'];
    control1.push(this.initReqItems());
    console.log(control1.length);
  }

  removeItem(i: number) {
    const control1 = <FormArray>this.poForm.controls['requisitionItems'];
    control1.removeAt(i);
    this.itemAdapter.splice(i, 1);
  }

  save(formData) {
    this.jqxLoader.open();
    this.pos.store(formData).subscribe(
      result => {
        if (result['message']) {
          this.poForm.setControl('requisitionItems', this.fb.array([]));
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
        this.getRequisitionItems();
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }

}

