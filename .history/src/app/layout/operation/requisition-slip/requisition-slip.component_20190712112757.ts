import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RequisitionSlipService,EmployeeDetailsService,CurrentUserService,GoodsIssueService,SiteMasterService, ChartOfItemsService, BranchMasterService, DateFormatService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
  selector: 'app-requisition-slip',
  templateUrl: './requisition-slip.component.html',
  styleUrls: ['./requisition-slip.component.scss']
})
export class RequisitionSlipComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

  rsForm: FormGroup;
  itemAdapter: any = [];
  itemFocus: any = [];
  branchAdapter: any = [];
  siteAdapter: any = [];
  userData: any = {};
  unitAdapter:any = [
    { name:'PCS', value:'PCS'},
    { name:'PKT', value:'PKT'},
    { name:'SET', value:'SET'},
    { name:'BOX', value:'BOX'},
    { name:'KG', value:'KG'},
    { name:'METER',value:'MTR'},
  ];
  reqPersonAdapter:Array<any> = [];

  constructor(
    private fb: FormBuilder,
    private rss: RequisitionSlipService,
    private cois: ChartOfItemsService,
    private gis: GoodsIssueService,
    private bms: BranchMasterService,
    private site:SiteMasterService,
    private cus: CurrentUserService,
    private eds:EmployeeDetailsService,
    private dfs: DateFormatService
  ) {
    this.createForm();
    this.itemAdapter[0] = [];
    this.itemFocus[0] = false;
    this.userData = this.cus.getTokenData();
  }

  createForm() {
    this.rsForm = this.fb.group({
      'enteredDate': ['', Validators.required],
      'branch': [''],
      'site': [''],
      'enteredOrderName': [null, Validators.required],
      'requestingPerson': [''],
      'remark': [''],
      'requisitionItems': this.fb.array([
        this.initReqItems(),
      ]),
    });
  }

  initReqItems(){
    return this.fb.group({
      itemCode: ['', Validators.required],
      itemName: [''],
      specification: ['', Validators.required],
      requestingQty: [null, Validators.required],
      unit: [null, Validators.required],
      requiredDay: [null, Validators.required],
      remarks: ['']
    });
  }

  itemFilter(searchPr, index){
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
    //   document.getElementById('itemCode').focus();
      document.getElementById('itemCode').focus();


    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(' ', '');
    if (dataString == ' ' && searchString.length > 2) {
      if(searchString){
        this.itemFocus[index] = true;
        this.cois.show(searchString).subscribe(
          response => {
            this.itemAdapter[index] = [];
            this.itemAdapter[index] = response;
            console.log(response)
          },
          error => {
            console.log(error);
          }
        );
      }else{
        this.itemFocus[index] = false;
      }
    }
  }

  itemListSelected(selectedValue, index){
      let id=selectedValue;
//    this.itemAdapter[index].itemName =  document.getElementById('itemName').value;
    if(selectedValue){

              this.rsForm.get('requisitionItems')['controls'][index].get('itemCode').patchValue(selectedValue);
              let itemname=this.itemAdapter[index].filter(x=>{
                  console.log(id,x.itemCode)
                  return Number(id)===Number(x.itemCode)
              })
              this.rsForm.get('requisitionItems')['controls'][index].get('itemName').patchValue(itemname[0]['itemName'])
    }
  }

  addItem(){
    const control1 = <FormArray>this.rsForm.controls['requisitionItems'];
    control1.push(this.initReqItems());
  }

  removeItem(i: number) {
      const control1 = <FormArray>this.rsForm.controls['requisitionItems'];
      control1.removeAt(i);
      this.itemAdapter.splice(i, 1);
  }

  comboSource: any;

  ngOnInit() {
    this.bms.index({}).subscribe(
      response => {
        this.branchAdapter = response;
        this.rsForm.controls['branch'].setValue(this.userData['user']['branch']);
        this.branchChange(this.userData['user']['branch']);
      },
      error => {
        console.log(error)
      }
    );
  }

  branchChange(event: any) {
    this.siteAdapter = [];
    this.jqxLoader.open();
    this.gis.indexBranch(event).subscribe((res) => {
      this.siteAdapter = res;
      this.jqxLoader.close();
    }, (error) => {
      this.jqxLoader.close();
    });
    let post = {};
    post['branch'] = event;
    this.eds.index(post).subscribe((response)=>{
      console.info(response);
      this.reqPersonAdapter = response;
    },(error)=>{
      console.info(error);
    });
  }


  ngAfterViewInit() {
     this.dfs.currentDate().subscribe(
      response => {
        let dateData = response[0];
        setTimeout(() => {
          this.rsForm.controls['enteredDate'].setValue(dateData['BS_DATE']);
          this.rsForm.get('enteredDate').markAsTouched();
          this.rsForm.controls['enteredOrderName'].setValue(this.userData['user']['userName']);
          this.rsForm.get('enteredOrderName').markAsTouched();
        }, 100);
      },
      error => {
        console.log(error);
      }
    )
  }

  /**
   * Function triggered when save button is clicked
   * @param formData
   */
  save(formData) {
    this.jqxLoader.open();
    this.rss.store(formData).subscribe(
      result => {
        if (result['message']) {
          this.rsForm.setControl('requisitionItems', this.fb.array([]));
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }

}
