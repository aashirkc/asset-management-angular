import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RequisitionSlipService,CurrentUserService, ChartOfItemsService, BranchMasterService, DateFormatService, SupplierMasterService, PurchaseOrderService, GoodsReceivingNotesService, SiteMasterService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
  selector: 'app-goods-receiving-notes-opening',
  templateUrl: './goods-receiving-notes-opening.component.html',
  styleUrls: ['./goods-receiving-notes-opening.component.scss']
})
export class GoodsReceivingNotesOpeningComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

  /**
  * Global Variable decleration
  */
  grnForm: FormGroup;

  /**
 * itemAdater variable is used to load option for select based
 * item input field value
 */
  itemAdapter: any = [];


  /**
   * itemFocus Variable is used to hide/show item select field after
   * item have been selected in select input
   *
   */
  itemFocus: any = [];
  userData: any = {};
  supplierAdapter: any = [];
  branchAdapter: any = [];
  siteAdapter: any = [];


  /**
   *
   */
  requisitionItemLength:number = 0;

  /**
 * Unit Type
 */
  unitAdapter: any = [
    { name: 'PCS', value: 'PCS' },
    { name: 'PKT', value: 'PKT' },
    { name: 'SET', value: 'SET' },
    { name: 'BOX', value: 'BOX' },
    { name: 'KG', value: 'KG' },
    { name: 'METER', value: 'MTR' },
  ]

  constructor(
    private fb: FormBuilder,
    private cois: ChartOfItemsService,
    private bms: BranchMasterService,
    private dfs: DateFormatService,
    private sms: SupplierMasterService,
    private sims: SiteMasterService,
    private pos: PurchaseOrderService,
    private cus: CurrentUserService,
    private grns: GoodsReceivingNotesService
  ) {
    this.createForm();
    this.itemAdapter[0] = [];
    this.itemFocus[0] = false;
  }

  /**
 * Create the form group
 * with given form control name
 */
  createForm() {
    this.grnForm = this.fb.group({
      'receiveBranch': ['', Validators.required],
      'receiveSite': [''],
      'receiveBy': [null, Validators.required],
      'receiveDate': [null, Validators.required],
      'remarks': [''],
      'requisitionItems': this.fb.array([
        this.initReqItems(),
      ]),
    });
    this.userData = this.cus.getTokenData();
  }

  initReqItems() {
    return this.fb.group({
      itemCode: [null, Validators.required],
      specification: [null, Validators.required],
      receiveQty: [null, Validators.required],
      rate: [null, Validators.required],
      unit: [null, Validators.required],
      supplier: [null, Validators.required],
      remarks: [''],
    });
  }

  ngOnInit() {
    this.sms.index({}).subscribe(
      response => {
        this.supplierAdapter = response;
      },
      error => {
        console.log(error);
      }
    );

    this.bms.index({}).subscribe(
      response => {
        this.branchAdapter = response;
    // console.log(this.branchAdapter)
        // this.grnForm.controls['receiveBranch'].patchValue(response[0]['branchCode']);
        // console.log(this.userData['user'].branch)
        // this.grnForm.get('receiveBranch').markAsTouched();
        // if(this.userData['user'].branch){
        //   this.branchChange(this.userData['user'].branch);
        // }
      },
      error => {
        console.log(error);
      }
    )
  }
  ngAfterViewInit() {
    this.dfs.currentDate().subscribe(
      response => {
        let dateData = response[0];
        setTimeout(() => {
          this.grnForm.controls['receiveDate'].setValue(dateData['BS_DATE']);
          this.grnForm.get('receiveDate').markAsTouched();
          this.grnForm.controls['receiveBy'].setValue(this.userData['user'].userName);
          this.grnForm.get('receiveBy').markAsTouched();
        }, 100);
      },
      error => {
        console.log(error);
      }
    )

  }

  branchChange($branchCode) {
      console.log($branchCode)
    this.sims.getSitebyBranchCode($branchCode).subscribe(
      response => {
        this.siteAdapter = response;

      },
      error => {
        console.log(error);
      }
    )

  }



  /**
   * itemFilter Event is called when Item input field has
   * keyup action followed by 'Enter'
   * Generate Suggestion based on input value entered
   * @param searchString
   * @param index
   */
  itemFilter(searchPr, index) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemCode').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(' ', '');
    if (dataString == ' ' && searchString.length > 2) {
      if(searchString){
        this.itemFocus[index] = true;
        this.cois.show(searchString).subscribe(
          response => {
            this.itemAdapter[index] = [];
            this.itemAdapter[index] = response;
          },
          error => {
            console.log(error);
          }
        );
      }else{
        this.itemFocus[index] = false;
      }
    }
  }

  /**
   * Event fired when option is selected from Item Suggestion Select field
   * Hide Select field after Item Selected.
   * @param selectedValue
   * @param index
   */
  itemListSelected(selectedValue, index) {
    if (selectedValue) {
      this.grnForm.get('requisitionItems')['controls'][index].get('itemCode').patchValue(selectedValue);
      // this.itemFocus[index] = false;
    }
  }

  /**
   * Add FormGroup to Requisition Item FormArray
   * Increments Requestion Item FormArray
   */
  addItem() {
    const control1 = <FormArray>this.grnForm.controls['requisitionItems'];
    control1.push(this.initReqItems());
    console.log(control1.length);
  }

  /**
   * Remove FormGroup at particular position form Requisition Item FormArray
   * Decrements Requestion Item FormArray
   */
  removeItem(i: number) {
    const control1 = <FormArray>this.grnForm.controls['requisitionItems'];
    control1.removeAt(i);
    /**
     * Remove itemAdapter itemArray at Particular position i,
     * Select Field option for select field at position 'i' of formArray
     */
    this.itemAdapter.splice(i, 1);
  }

  /**
     * Function triggered when save button is clicked
     * @param formData
     */
  save(formData) {
    console.log(formData);
    this.jqxLoader.open();
    this.grns.openingStore(formData).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();

        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }

      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }

}
