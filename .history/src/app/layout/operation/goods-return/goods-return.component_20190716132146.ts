import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RequisitionSlipService,CurrentUserService,GoodsReturnService,SupplierMasterService,SiteMasterService, ChartOfItemsService, BranchMasterService, DateFormatService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
  selector: 'app-goods-return',
  templateUrl: './goods-return.component.html',
  styleUrls: ['./goods-return.component.scss']
})
export class GoodsReturnComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

  goodsReturnForm: FormGroup;
  itemAdapter: any = [];
  siteAdapter: any = [];
  itemFocus: any = [];
  userData: any = {};
  supplierAdapter: any = [];
  deleteRowIndexes: Array<any> = [];
  itemFilterArray: any = [];

  constructor(
    private fb: FormBuilder,
    private rss: RequisitionSlipService,
    private cois: ChartOfItemsService,
    private supMs: SupplierMasterService,
    private sms:SiteMasterService,
    private grs:GoodsReturnService,
    private cus: CurrentUserService,
    private dfs: DateFormatService
  ) {
    this.createForm();
    this.itemAdapter[0] = [];
    this.supplierAdapter[0] = [];
    this.itemFocus[0] = false;
    this.userData = this.cus.getTokenData();
  }

  createForm() {
    this.goodsReturnForm = this.fb.group({
      'returnDate': [null, Validators.required],
      'returnBy': [null, Validators.required],
      'returnItems': this.fb.array([
        this.initReqItems(),
      ]),
    });
  }

  initReqItems(){
    return this.fb.group({
      supplier:[null, Validators.required],
      itemNo: [null, Validators.required],
      assetCode: [null, Validators.required],
      orderNo: [null, Validators.required],
      remarks: ['']
    });
  }

  itemFilter(searchPr, index){
    let supplier = this.goodsReturnForm.get('returnItems')['controls'][index].get('supplier').value || '';
    let post = {};
    post['supplier'] = supplier;
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemNo').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(' ', '');
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocus[index] = true;
        this.grs.indexGoodsReturnItems(searchString.replace(/\s/g, ''),post).subscribe(
          response => {
            console.info(response);
            this.itemAdapter[index] = [];
            this.itemAdapter[index] = response;
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.itemFocus[index] = false;
      }
    }
  }

  itemListSelected(selectedValue, index){
    if(selectedValue){
      this.goodsReturnForm.get('returnItems')['controls'][index].get('itemNo').patchValue(selectedValue);
      this.itemFilterArray = this.itemAdapter[index];
      let newCart = this.itemFilterArray.filter(function(item) {
        if(item.itemNo === selectedValue){
          return true;
        }
      });
      if(newCart[0]){
        let data = newCart[0];
        this.goodsReturnForm.get('returnItems')['controls'][index].get('assetCode').patchValue(data['assetCode']);
        this.goodsReturnForm.get('returnItems')['controls'][index].get('orderNo').patchValue(data['orderNo']);
      }

    }
  }



  addItem(){
    const control1 = <FormArray>this.goodsReturnForm.controls['returnItems'];
    control1.push(this.initReqItems());
  }

  removeItem(i: number) {
      const control1 = <FormArray>this.goodsReturnForm.controls['returnItems'];
      control1.removeAt(i);
      this.itemAdapter.splice(i, 1);
  }

  comboSource: any;

  ngOnInit() {

    let Data = JSON.parse(localStorage.getItem('fAssetUser'))


    let Date1 = Data['fiscalYear']
    let startDate = Date1['startDateBS']
    let todayDate = Date1['endDateBS']
    console.log(startDate)

    this.goodsReturnForm.get('returnDate').patchValue(todayDate);
    this.supMs.index({}).subscribe(
      response => {
        if (response.length == 1 &&  response[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText =  response[0].error;
          this.errNotification.open();
          this.supplierAdapter = [];
        } else {
          this.supplierAdapter = response;
        }
      },
      error => {
        console.log(error)
      }
    )
  }



  ngAfterViewInit() {
    this.dfs.currentDate().subscribe(
      response => {
        let dateData = response[0];
        setTimeout(() => {

          this.goodsReturnForm.controls['returnBy'].setValue(this.userData['user'].userName);
          this.goodsReturnForm.get('returnBy').markAsTouched();
        }, 100);
      },
      error => {
        console.log(error);
      }
    )
  }

  save(formData) {
    console.log(formData);
    this.jqxLoader.open();
    this.grs.store(formData).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
}
