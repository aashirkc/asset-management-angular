import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { BranchMasterService, CurrentUserService, SiteMasterService, DateFormatService, GoodsReceivingNotesService, BillSundryMasterService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-goods-receiving-notes',
  templateUrl: './goods-receiving-notes.component.html',
  styleUrls: ['./goods-receiving-notes.component.scss']
})
export class GoodsReceivingNotesComponent implements OnInit {

  @ViewChild('grnGrid') grnGrid: jqxGridComponent;
  @ViewChild('billSundryWindow') billSundryWindow: jqxWindowComponent;
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

  /**
  * Global Variable decleration
  */
  grnForm: FormGroup;
  grnBillForm: FormGroup;

  gridSource: any;
  gridDataAdapter: any;
  gridColumns: any = [];

  branchAdapter: any = [];
  siteAdapter: any = [];
  userData: any = {};
  grnNo: any;
  supplierid: any;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private bms: BranchMasterService,
    private bsms: BillSundryMasterService,
    private sms: SiteMasterService,
    private grns: GoodsReceivingNotesService,
    private cus: CurrentUserService,
    private translate: TranslateService,
    private dfs: DateFormatService
  ) {
    this.createForm();
    this.userData = this.cus.getTokenData();
    this.getTranslation();
  }

  transData: any;
  getTranslation() {
    this.translate.get(['SN', 'ITEM','SUPPLIER','ORDERNO','RATE','UNIT','QUANTITY','REMARKS','SPECIFICATION']).subscribe((translation: [string]) => {
      this.transData = translation;
      this.loadGrid();
    });
  }

  /**
* Create the form group
* with given form control name
*/
  createForm() {
    this.grnForm = this.fb.group({
      'receiveBranch': [null, Validators.required],
      'receiveSite': [''],
      'receiveBy': [null, Validators.required],
      'receiveDate': [null, Validators.required],
      'remarks': [''],
    });

    this.grnBillForm = this.fb.group({
      'grn': [null, Validators.required],
      'billSundry': this.fb.array([
        this.initBillSundryItems(),
      ]),
    });
  }

  initBillSundryItems() {
    return this.fb.group({
      sundryName: [null, Validators.required],
      sundryCode: [null, Validators.required],
      amount: [null, Validators.required],
      remarks: [null],
    });
  }

  addItem() {
    const control1 = <FormArray>this.grnBillForm.controls['billSundry'];
    control1.push(this.initBillSundryItems());
  }

  removeItem(i: number) {
    const control1 = <FormArray>this.grnBillForm.controls['billSundry'];
    control1.removeAt(i);
  }

  clearFormArray = (formArray: FormArray) => {
    // this.grnBillForm.controls['billSundry']['controls'] = [];
    this.grnBillForm.setControl('billSundry', this.fb.array([]));
  }

  ngOnInit() {
    this.bms.index({}).subscribe(

      response => {
          this.supplierid=response['supplier']
          console.log('')
        this.branchAdapter = response;
        this.grnForm.controls['receiveBranch'].setValue(this.userData['user'].branch);
        this.grnForm.get('receiveBranch').markAsTouched();
        if (this.userData['user'].branch) {
          this.branchChange(this.userData['user'].branch);
        }
      },
      error => {
        console.log(error);
      }
    );

  }

  loadGrid() {
    this.gridSource =
      {
        datatype: 'json',
        datafields: [
          { name: 'itemCode', type: 'string' },
          { name: 'itemName', type: 'string' },
          { name: 'supplierName', type: 'string' },
          { name: 'supplier', type: 'string' },
          { name: 'orderNo', type: 'string' },
          { name: 'rate', type: 'string' },
          { name: 'unit', type: 'string' },
          { name: 'receiveQty', type: 'string', map: 'orderQty' },
          { name: 'remarks', type: 'string' },
          { name: 'specification', type: 'string' }
        ],
        localdata: [],
      };

    this.gridDataAdapter = new jqx.dataAdapter(this.gridSource);

    this.gridColumns =
      [
        {
          text: this.transData['SN'], sortable: false, filterable: false, editable: false,
          groupable: false, draggable: false, resizable: false,
          datafield: '', columntype: 'number', width: 50,
          cellsrenderer: function (row, column, value) {
            return "<div style='margin:4px;'>" + (value + 1) + "</div>";
          }
        },
        { text: this.transData['ITEM'], displayfield: 'itemName', datafield: 'itemCode', width: 200, editable: false },
        { text: this.transData['SUPPLIER'], displayfield: 'supplierName', datafield: 'supplierName', width: 200, editable: false },
        { text: this.transData['ORDERNO'], datafield: 'orderNo', width: 150, editable: false },
        { text: this.transData['RATE'], datafield: 'rate', width: 150, editable: false },
        { text: this.transData['UNIT'], datafield: 'unit', width: 90, editable: false },
        { text: this.transData['QUANTITY'], datafield: 'receiveQty', width: 90, editable: true },
        { text: this.transData['REMARKS'], datafield: 'remarks', editable: false },
        { text: this.transData['SPECIFICATION'], datafield: 'specification', editable: false },
      ];
  }

  setupBillSundry() {
    this.bsms.index({}).subscribe(
      response => {
        this.grnBillForm.get('billSundry').reset();
        let billSundryData: any = [];
        for (let i = 0; i < response.length - 1; i++) {
          this.addItem();
        }

        for (let i = 0; i < response.length; i++) {
          billSundryData[i] = [];
          billSundryData[i]['sundryCode'] = response[i]['code'];
          billSundryData[i]['sundryName'] = response[i]['name'];
          billSundryData[i]['amount'] = 0;
          billSundryData[i]['remarks'] = '';
        }
        console.log(billSundryData);
        this.grnBillForm.get('billSundry').patchValue(billSundryData);
      },
      error => {
        console.log(error);
      }
    )
  }


  getGRN() {
    this.jqxLoader.open();
    this.grns.index({}).subscribe(
      response => {
        if (response.length == 1 && response[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response[0].error;
          this.errNotification.open();
          this.gridSource.localdata = [];
        } else {
          this.gridSource.localdata = response;
        }
        this.grnGrid.updatebounddata();
        this.jqxLoader.close();
      },
      error => {
        this.jqxLoader.close();
        console.log(error);
      }
    )
  }

  ngAfterViewInit() {
    this.getGRN();
    this.dfs.currentDate().subscribe(
      response => {
        let dateData = response[0];
        setTimeout(() => {
          this.grnForm.controls['receiveDate'].setValue(dateData['BS_DATE']);
          this.grnForm.get('receiveDate').markAsTouched();
          this.grnForm.controls['receiveBy'].setValue(this.userData['user'].userName);
          this.grnForm.get('receiveBy').markAsTouched();
        }, 100);
      },
      error => {
        console.log(error);
      }
    )
  }


  openWindow() {
    this.billSundryWindow.open();
    this.setupBillSundry();
  }



  searchPO(purchaseOrderId) {
    console.log(purchaseOrderId);
    // this.pos.show(purchaseOrderId).subscribe(
    //   response => {
    //     console.log(response);
    //   },
    //   error => {
    //     console.log(error);
    //   }
    // )
  }

  branchChange($branchCode) {
    this.sms.getSitebyBranchCode($branchCode).subscribe(
      response => {
        this.siteAdapter = response;
      },
      error => {
        console.log(error);
      }
    )

  }

  save(formData) {
    let ids = this.grnGrid.getselectedrowindexes();
    let arrData = [];
    for (let i = 0; i < ids.length; i++) {
      let id = String(ids[i]);
      let data = this.grnGrid.getrowdatabyid(id);
      arrData.push(data);
    }
    formData['purchaseItems'] = arrData;
    console.log(formData);
    if (arrData.length > 0) {
      this.jqxLoader.open();
      this.grns.store(formData).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();

            this.grnNo = result['data'];
            localStorage.setItem('lastGrnNo', result['data']);
            this.grnBillForm.get('grn').patchValue(result['data']);
            this.openWindow();
            this.getGRN();
          }

          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
          this.jqxLoader.close();

        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please Select Purchase Order Items';
      this.errNotification.open();
    }

  }

  saveBillSundry(formData) {
    console.log(formData);
    this.jqxLoader.open();
    this.grns.grnBillSundry(formData).subscribe(
      response => {

        if (response['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = response['message'];
          this.msgNotification.open();

          this.grnBillForm.reset();
          this.billSundryWindow.close();
          this.getGRN();
          this.router.navigate(['/operation/item-accessories/' + this.grnNo]);

        }

        if (response['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response['error']['message'];
          this.errNotification.open();
        }

        this.jqxLoader.close();
      },
      error => {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = 'Please Select Purchase Order Items';
        this.errNotification.open();
        this.jqxLoader.close();
      }
    )
  }

}
