import { Component, OnInit, Inject, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RequisitionSlipService,CurrentUserService, GoodsReturnService, GoodsDisposalService, SiteMasterService, ChartOfItemsService, BranchMasterService, DateFormatService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
  selector: 'app-goods-disposal',
  templateUrl: './goods-disposal.component.html',
  styleUrls: ['./goods-disposal.component.scss']
})
export class GoodsDisposalComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild("item") itemOption: ElementRef;

  goodsDisposalForm: FormGroup;
  itemAdapter: any = [];
  siteAdapter: any = [];
  itemFocus: any = [];
  userData: any = {};
  disposalAdapter: any = [
    { name: 'Sales', value: 'Sales' },
    { name: 'Write Off', value: 'Write Off' },
  ]
  deleteRowIndexes: Array<any> = [];
  itemFilterArray: any = [];

  constructor(
    private fb: FormBuilder,
    private rss: RequisitionSlipService,
    private cois: ChartOfItemsService,
    private gds: GoodsDisposalService,
    private sms: SiteMasterService,
    private grs: GoodsReturnService,
    private cus: CurrentUserService,
    private dfs: DateFormatService
  ) {
    this.createForm();
    this.itemAdapter[0] = [];
    this.itemFocus[0] = false;
    this.userData = this.cus.getTokenData();
  }

  createForm() {
    this.goodsDisposalForm = this.fb.group({
      'disposalDate': [null, Validators.required],
      'disposalOrderBy': [null, Validators.required],
      'disposalType': [null, Validators.required],
      'disposalItems': this.fb.array([
        this.initReqItems(),
      ]),
    });
  }

  initReqItems() {
    return this.fb.group({
      itemNo: [null, Validators.required],
      disposalAmount: [null, Validators.required],
      depreciationAmount: [{ value: 0 }, Validators.required],
      purchaseAmount: [{ value: 0}, Validators.required],
      pl: [{ value: ' ',disabled:true}],
      remarks: ['']
    });
  }

  itemFilter(searchPr, index) {
    let post = {};
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemNo').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(' ', '');
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocus[index] = true;
        this.gds.indexGoodsDisposalItems(searchString.replace(/\s/g, ''), post).subscribe(
          response => {
            this.itemAdapter[index] = [];
            this.itemAdapter[index] = response;
          },
          error => {

          }
        );
      } else {
        this.itemFocus[index] = false;
      }
    }
  }

  disposalAmount(searchPr, index) {
    let disposalAmount = this.goodsDisposalForm.get('disposalItems')['controls'][index].get('disposalAmount').value;
    let depreciationAmount = this.goodsDisposalForm.get('disposalItems')['controls'][index].get('depreciationAmount').value;
    let purchaseAmount = this.goodsDisposalForm.get('disposalItems')['controls'][index].get('purchaseAmount').value;
    if (Number(disposalAmount)) {
      let totlaAmount = Number(disposalAmount) + Number(depreciationAmount);
      let result = Number(totlaAmount) - Number(purchaseAmount);
      if (result>=0) {
        this.goodsDisposalForm.get('disposalItems')['controls'][index].get('pl').patchValue(result);
      }else{
        this.goodsDisposalForm.get('disposalItems')['controls'][index].get('pl').patchValue('('+Math.abs(result)+')');
      }

    }

  }

  itemListSelected(selectedValue, index) {
    if (selectedValue) {
      this.goodsDisposalForm.get('disposalItems')['controls'][index].get('itemNo').patchValue(selectedValue);
      this.itemFilterArray = this.itemAdapter[index];
      let newCart = this.itemFilterArray.filter(function (item) {
        if (item.itemNo === selectedValue) {
          return true;
        }
      });
      if (newCart[0]) {
        let data = newCart[0];
        this.goodsDisposalForm.get('disposalItems')['controls'][index].get('purchaseAmount').patchValue(data['purchaseAmount']);
        this.goodsDisposalForm.get('disposalItems')['controls'][index].get('depreciationAmount').patchValue(data['depreciationAmount']);
        this.disposalAmount('',index);
      }

    }
  }
   moveFocus(eventRef,i){
    //console.log(eventRef)
    var charCode = (window.event) ? eventRef.keyCode : eventRef.which;
    if ((charCode === 40)) {
    // //console.log(this.itemOption.nativeElement)
    setTimeout(() => {
        this.itemOption.nativeElement.focus();
        this.itemOption.nativeElement.selectedIndex=0;

        this.goodsDisposalForm.get('disposalItems')['controls'][i].get('itemNo').patchValue(this.itemAdapter[i][0].itemCode);
        this.goodsDisposalForm.get('disposalItems')['controls'][i].get('itemName').patchValue(this.itemAdapter[i][0].itemName);
      }, 1000);


    }
}



  addItem() {
    const control1 = <FormArray>this.goodsDisposalForm.controls['disposalItems'];
    control1.push(this.initReqItems());
  }

  removeItem(i: number) {
    const control1 = <FormArray>this.goodsDisposalForm.controls['disposalItems'];
    control1.removeAt(i);
    this.itemAdapter.splice(i, 1);
  }

  comboSource: any;

  ngOnInit() {

  }



  ngAfterViewInit() {
    this.dfs.currentDate().subscribe(
      response => {
        let dateData = response[0];
        setTimeout(() => {
          this.goodsDisposalForm.controls['disposalDate'].setValue(dateData['BS_DATE']);
          this.goodsDisposalForm.get('disposalDate').markAsTouched();
          this.goodsDisposalForm.controls['disposalOrderBy'].setValue(this.userData['user'].userName);
          this.goodsDisposalForm.get('disposalOrderBy').markAsTouched();
        }, 100);
      },
      error => {
        console.log(error);
      }
    )
  }

  save(formData) {
    console.log(formData);
    this.jqxLoader.open();
    this.gds.store(formData).subscribe(
      result => {
        if (result['message']) {
          this.goodsDisposalForm.setControl('requisitionItems', this.fb.array([]));
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }

}
