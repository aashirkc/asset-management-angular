import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ChartOfItemsService, SiteMasterService, FinancialYearService, BranchMasterService, AllReportService, DateFormatService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxInputComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxinput';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
  selector: 'app-stateline-depreciation',
  templateUrl: './stateline-depreciation.component.html',
  styleUrls: ['./stateline-depreciation.component.scss']
})
export class StatelineDepreciationComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('branchCombo') branchCombo: jqxComboBoxComponent;
  @ViewChild('fiscalYearCombo') fiscalYearCombo: jqxComboBoxComponent;
  @ViewChild('dateFrom') dateFrom: jqxInputComponent;

  alForm: FormGroup;

  itemAdapter: any;
  itemFocus: boolean = false;
  branchAdapter: any = [];
  reportDatas: any = [];
  dateData: any;
  fiscalYearAdapter: any = [];

  constructor(
    private fb: FormBuilder,
    private cois: ChartOfItemsService,
    private bms: BranchMasterService,
    private report: AllReportService,
    private sms: SiteMasterService,
    private date: DateFormatService,
    private fys: FinancialYearService
  ) {
    this.createForm();
  }

  ngOnInit() {

    let Data = JSON.parse(localStorage.getItem('fAssetUser')).toDay
    let todayDate = Data

    console.log(todayDate)
    this.bms.index({}).subscribe(
      response => {
        if (response.length == 1 && response[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response[0].error;
          this.errNotification.open();
          this.branchAdapter = [];
        } else {
          this.branchAdapter = response;
        }
      },
      error => {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = 'Couldnot load Branches';
        this.errNotification.open();
      }
    );
    this.fys.index({}).subscribe((response) => {
      console.info(response);
      this.fiscalYearAdapter = response;
    })
  }

  itemFilter(searchPr) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemCode').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(' ', '');
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocus = true;
        this.cois.show(searchString).subscribe(
          response => {
            this.itemAdapter = response;
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.itemFocus = false;
      }
    }

  }

  itemListSelected(selectedValue) {
    if (selectedValue) {
      this.alForm.controls['itemCode'].setValue(selectedValue);
    }
  }

  ngAfterViewInit() {

  }

  createForm() {
    this.alForm = this.fb.group({
      'dateFrom': ['', Validators.required],
      'dateTo': ['', Validators.required],
      'itemCode':['']
    });
  }
  fiscalChange() {
    let selectedData = this.fiscalYearCombo.getSelectedItem().originalItem;
    if (selectedData['fyStartBs']) {
      this.alForm.controls['dateFrom'].setValue(selectedData['fyStartBs']);
      this.alForm.get('dateFrom').markAsTouched();
    }
    if (selectedData['fyEndBs']) {
      this.alForm.controls['dateTo'].setValue(selectedData['fyEndBs']);
      this.alForm.get('dateTo').markAsTouched();
    }
  }

  save(formData) {
    formData['branch'] = this.branchCombo.val() || '';
    formData['fy'] = this.fiscalYearCombo.val() || '';
    if (formData) {
      this.jqxLoader.open();
      this.report.getStatelineDepreciationReport(formData).subscribe(
        result => {
          this.reportDatas = result;
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please enter all data';
      this.errNotification.open();
    }

  }

  exportReport(): void {
    let htmltable = document.getElementById('reportContainer');
    let html = htmltable.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
  }

  printReport(): void {
    let printContents, popupWin;
    printContents = document.getElementById('reportContainer').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

}
