import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ChartOfItemsService, BranchMasterService, AllReportService, DateFormatService, SiteMasterService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxInputComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxinput';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
  selector: 'app-assets-wise-reports',
  templateUrl: './assets-wise-reports.component.html',
  styleUrls: ['./assets-wise-reports.component.scss']
})
export class AssetsWiseReportsComponent implements OnInit {
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('branchCombo') branchCombo: jqxComboBoxComponent;
  @ViewChild('hierarchyCombo') hierarchyCombo: jqxComboBoxComponent;
  @ViewChild('dateFrom') dateFrom: jqxInputComponent;
  @ViewChild('siteCombo') siteCombo: jqxComboBoxComponent;
  @ViewChild('requestedByCombo') requestedByCombo: jqxComboBoxComponent;

  alForm: FormGroup;
  itemAdapter: any;
  itemFocus: boolean = false;
  branchAdapter: any = [];
  hierarchyAdapter: any = [];
  siteAdapter: any = [];
  siteName:string;
  reportDatas: any = [];

  requestByAdapter: any = [];
  dateData: any;
  constructor(
    private fb: FormBuilder,
    private cois: ChartOfItemsService,
    private bms: BranchMasterService,
    private report: AllReportService,
    private date: DateFormatService,
    private site:SiteMasterService,
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.bms.index({}).subscribe(
      response => {
        if (response.length == 1 && response[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response[0].error;
          this.errNotification.open();
          this.branchAdapter = [];
        } else {
          this.branchAdapter = response;
        }
      },
      error => {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = 'Couldnot load Branches';
        this.errNotification.open();
      }
    );

    this.cois.getLevel().subscribe((res) => {
      let array = [];
      if (res['level']) {
        for (let i = 1; i <= res['level']; i++) {
          let dt = {};
          dt['name'] = i;
          array.push(dt);
        }
      }
      this.hierarchyAdapter = array;
    }, (error) => {
      console.info(error);
    });

  }

  ngAfterViewInit() {
    this.date.currentDate().subscribe(
      response => {
        this.dateData = response[0];
        setTimeout(() => {
          this.alForm.controls['dateFrom'].setValue(this.dateData['fy_StartBs']);
          this.alForm.get('dateFrom').markAsTouched();
          this.alForm.controls['dateTo'].setValue(this.dateData['BS_DATE']);
          this.alForm.get('dateTo').markAsTouched();
        }, 100);
      },
      error => {
        console.log(error);
      }
    )
  }

  createForm() {
    this.alForm = this.fb.group({
      'dateFrom': [null, Validators.required],
      'dateTo': [null, Validators.required]
    });
  }

  save(formData) {
    let site = this.siteCombo.val() || null;
    formData['site'] =  this.siteCombo.val() || null;

    let person = this.requestedByCombo.val() || null;
    let sitename = this.siteAdapter.filter(x=>x.sideCode== site);
    if(site && sitename.length==1){
      this.siteName = sitename[0].sideName;
    }

    let selectedBranches: any = this.branchCombo.getSelectedItems();
    let branches = '';
    for (let i = 0; i < selectedBranches.length; i++) {
      if (i > 0) {
        branches += ',';
      }
      branches += "'" + selectedBranches[i]['value'] + "'";
    }
    formData['branch'] = '(' + branches + ')';
    formData['site'] =  this.siteCombo.val() || '';
    formData['level'] = this.hierarchyCombo.val();



    if (formData && formData['level']) {
      this.jqxLoader.open();
      this.report.getAssetsWiseReports(formData).subscribe(
        result => {
          this.reportDatas = result;
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();

        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please Enter Mandatory data';
      this.errNotification.open();
    }

  }
  branchChanged(event: any) {
    let selectedBranches: any = this.branchCombo.getSelectedItems();
    let branches = '';
    if (selectedBranches.length == 1) {

      this.siteCombo.disabled(false);
      let data = selectedBranches[0];
      this.jqxLoader.open();
      this.report.getReqRequestedBy(data['value']).subscribe((response) => {
        this.requestByAdapter = response;
        this.jqxLoader.close();
      }, (error) => {
        this.jqxLoader.close();
        console.info(error);
      });
      this.jqxLoader.open();
      let post = {};
      post['branch'] = data['value'];
      this.site.index(post).subscribe((response) => {
        this.siteAdapter = response;
        this.jqxLoader.close();
      }, (error) => {
        this.jqxLoader.close();
        console.info(error);
      });

    } else {
      this.requestByAdapter = [];
      this.siteAdapter = [];
      this.requestedByCombo.disabled(true);
      this.siteCombo.disabled(true);
    }
  }

  exportReport(): void {
    let htmltable = document.getElementById('reportContainer');
    let html = htmltable.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
  }

  printReport(): void {
    let printContents, popupWin;
    printContents = document.getElementById('reportContainer').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          .text-bold{
            font-weight: bold;
            color: #025aa5;
        }
        .text-normal{
            font-weight: normal;
            color: #000000;
        }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }
}
