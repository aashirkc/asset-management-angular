import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ChartOfItemsService,CurrentUserService,  FinancialYearService, BranchMasterService, AllReportService, DateFormatService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxInputComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxinput';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
  selector: 'app-tax-depreciation-report',
  templateUrl: './tax-depreciation-report.component.html',
  styleUrls: ['./tax-depreciation-report.component.scss']
})
export class TaxDepreciationReportComponent implements OnInit {
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('branchCombo') branchCombo: jqxComboBoxComponent;
  @ViewChild('fiscalYCombo') fiscalYCombo: jqxComboBoxComponent;
  @ViewChild('dateFrom') dateFrom: jqxInputComponent;

  alForm: FormGroup;

  branchAdapter: any = [];
  fiscalYearAdapter: any = [];
  reportDatas: any = [];
  dateData: any;
  userData: any = {};

  constructor(
    private fb: FormBuilder,
    private cois: ChartOfItemsService,
    private bms: BranchMasterService,
    private report: AllReportService,
    private fys: FinancialYearService,
    private cus: CurrentUserService,
    private date: DateFormatService
  ) {
    this.createForm();
    this.userData = this.cus.getTokenData();
  }

  ngOnInit() {
    // this.bms.index({}).subscribe(
    //   response => {
    //     if (response.length == 1 && response[0].error) {
    //       let messageDiv: any = document.getElementById('error');
    //       messageDiv.innerText = response[0].error;
    //       this.errNotification.open();
    //       this.branchAdapter = [];
    //     } else {
    //       this.branchAdapter = response;
    //     }
    //   },
    //   error => {
    //     let messageDiv: any = document.getElementById('error');
    //     messageDiv.innerText = 'Couldnot load Branches';
    //     this.errNotification.open();
    //   }
    // );
    this.fys.index({}).subscribe((res) => {
      this.fiscalYearAdapter = res;
      setTimeout(() => {
        this.fiscalYCombo.selectItem(this.userData['user']['fy']);
      }, 100);

    }, (error) => {
      console.info(error);
    })
  }

  ngAfterViewInit() {

  }

  createForm() {
    this.alForm = this.fb.group({

    });
  }

  save(formData) {
    // formData['branch'] = this.branchCombo.val() || '';
    formData['fy'] = this.fiscalYCombo.val() || '';
    if (formData) {
      this.jqxLoader.open();
      this.report.getTaxDepreciationReport(formData).subscribe(
        result => {
          this.reportDatas = result;
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please enter all data';
      this.errNotification.open();
    }

  }

  exportReport(): void {
    let htmltable = document.getElementById('reportContainer');
    let html = htmltable.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
  }

  printReport(): void {
    let printContents, popupWin;
    printContents = document.getElementById('reportContainer').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

}
