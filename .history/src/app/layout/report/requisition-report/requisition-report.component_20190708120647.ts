import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ChartOfItemsService,SiteMasterService,OrganizationMasterService,EmployeeDetailsService, BranchMasterService, AllReportService, DateFormatService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxInputComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxinput';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';

@Component({
  selector: 'app-requisition-report',
  templateUrl: './requisition-report.component.html',
  styleUrls: ['./requisition-report.component.scss']
})
export class RequisitionReportComponent implements OnInit {
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('branchCombo') branchCombo: jqxComboBoxComponent;
  @ViewChild('requestedByCombo') requestedByCombo: jqxComboBoxComponent;
  @ViewChild('statusCombo') statusCombo: jqxComboBoxComponent;
  @ViewChild('siteCombo') siteCombo: jqxComboBoxComponent;
  @ViewChild('dateFrom') dateFrom: jqxInputComponent;
  @ViewChild('myWindow') myWindow: jqxWindowComponent;
  alForm: FormGroup;
  itemAdapter: any;
  itemFocus: boolean = false;
  branchAdapter: any = [];
  siteAdapter: any = [];
  reportDatas: any = [];
  dateData: any;
  siteName:string;
  personName:string;
  requestByAdapter: any = [];
  printOfficialDetails:any;
  printItemDetails:any = [];
  officeDetails:any;
  statusAdapter: any = [
    {
      name:'All'
    },
    {
      name:'Approve',
    },
    {
      name:'Reject'
    }
  ];

  constructor(
    private fb: FormBuilder,
    private cois: ChartOfItemsService,
    private bms: BranchMasterService,
    private report: AllReportService,
    private eds:EmployeeDetailsService,
    private site:SiteMasterService,
    private ors:OrganizationMasterService,
    private date: DateFormatService
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.bms.index({}).subscribe(
      response => {
        if (response.length == 1 && response[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response[0].error;
          this.errNotification.open();
          this.branchAdapter = [];
        } else {
          this.branchAdapter = response;
        }
      },
      error => {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = 'Couldnot load Branches';
        this.errNotification.open();
      }
    );
    this.ors.index({}).subscribe((res)=>{
      console.info(res);
      this.officeDetails = res[0];
    });

  }

  ngAfterViewInit() {
    this.date.currentDate().subscribe(
      response => {
        this.dateData = response[0];
        setTimeout(() => {
          this.alForm.controls['dateFrom'].setValue(this.dateData['fy_StartBs']);
          this.alForm.get('dateFrom').markAsTouched();
          this.alForm.controls['dateTo'].setValue(this.dateData['BS_DATE']);
          this.alForm.get('dateTo').markAsTouched();
        }, 100);
      },
      error => {
        console.log(error);
      }
    );
    this.statusCombo.selectItem('All');
  }

  createForm() {
    this.alForm = this.fb.group({
      'itemCode': [''],
      'dateFrom': [null, Validators.required],
      'dateTo': [null, Validators.required],
    });
  }

  itemFilter(searchPr) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemCode').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(' ', '');
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocus = true;
        this.cois.show(searchString).subscribe(
          response => {
            this.itemAdapter = response;
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.itemFocus = false;
      }
    }

  }

  itemListSelected(selectedValue) {
    if (selectedValue) {
      this.alForm.controls['itemCode'].setValue(selectedValue);
    }
  }

  branchChanged(event: any) {
    let selectedBranches: any = this.branchCombo.getSelectedItems();
    let branches = '';
    if (selectedBranches.length == 1) {
      this.requestedByCombo.disabled(false);
      this.siteCombo.disabled(false);
      let data = selectedBranches[0];
      this.jqxLoader.open();
      this.report.getReqRequestedBy(data['value']).subscribe((response) => {
        this.requestByAdapter = response;
        this.jqxLoader.close();
      }, (error) => {
        this.jqxLoader.close();
        console.info(error);
      });
      this.jqxLoader.open();
      let post = {};
      post['branch'] = data['value'];
      this.site.index(post).subscribe((response) => {
        this.siteAdapter = response;
        this.jqxLoader.close();
      }, (error) => {
        this.jqxLoader.close();
        console.info(error);
      });

    } else {
      this.requestByAdapter = [];
      this.siteAdapter = [];
      this.requestedByCombo.disabled(true);
      this.siteCombo.disabled(true);
    }
  }
  viewItem(data) {
    let itemNo = data['requestingNo'];
    if (Number(itemNo)) {
      this.jqxLoader.open();
      this.report.getRequisitionByItemNo(itemNo).subscribe((response) => {
        this.jqxLoader.close();
        this.printOfficialDetails = response[0] && response[0][0];
        this.printItemDetails = response[1];
        this.printOfficialDetails['itemNo'] = this.printItemDetails && itemNo;
        this.myWindow.draggable(true);
        this.myWindow.title('View Item');
        this.myWindow.open();
      }, (error) => {
        this.jqxLoader.close();
      })
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = "Please Select Item No First !!";
      this.errNotification.open();
    }
  }

  save(formData) {
    let site = this.siteCombo.val() || null;
    let person = this.requestedByCombo.val() || null;
    let sitename = this.siteAdapter.filter(x=>x.sideCode== site);
    if(site && sitename.length==1){
      this.siteName = sitename[0].sideName;
    }
    if(person){
      this.personName = person;
    }
    let selectedBranches: any = this.branchCombo.getSelectedItems();
    let branches = '';
    for (let i = 0; i < selectedBranches.length; i++) {
      if (i > 0) {
        branches += ',';
      }
      branches += "'" + selectedBranches[i]['value'] + "'";
    }
    formData['branch'] = '(' + branches + ')';
    formData['RequestBy'] = this.requestedByCombo.val() || '';
    formData['status'] =  this.statusCombo.val() || '';
    formData['site'] =  this.siteCombo.val() || '';

    if (formData) {
      this.jqxLoader.open();
      this.report.getRequisition(formData).subscribe(
        result => {
          this.reportDatas = result;
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please enter all data';
      this.errNotification.open();
    }

  }

  exportReport(): void {
    let htmltable = document.getElementById('reportContainer');
    let html = htmltable.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
  }

  printReport(): void {
    let printContents, popupWin;
    printContents = document.getElementById('reportContainer').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          .last-td{
            display:none;
          }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  printDetailsReport(): void {
    let printContents, popupWin;
    printContents = document.getElementById('page-wrap').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
      #page-wrap {
          width: 800px;
          margin: 0 auto;
      }

      .invoice-header {
          margin-top: 20px;
          /* text-align: center; */
      }
      .invoice-header .meta-sub{
          font-size: 12px;
          text-align: center;
      }

      .invoice-header .meta-header, .invoice-header .meta-info{
          margin-top: 10px;
          display: block;
      }

      .invoice-header .meta-header div, .invoice-header .meta-info div{
          display: inline-block;
          width: 60%;
      }

      .invoice-header .meta-header div:first-child, .invoice-header .meta-info div:first-child{
          display: inline-block;
          width: 38%;
      }

      .invoice-header .meta-header div:first-child p , .invoice-header .meta-info div:first-child p{
          font-size: 12px;
      }

      .invoice-header .meta-info{
          margin-top: 5px;
          font-size: 16px;
      }

      .invoice-header .meta-invoice-title{
          font-size: 20px;
          margin-top: 5px;
          font-weight: bold;
          padding-left: 9%;
      }

      .invoice-title-number {
          font-weight: bold;
          text-align: right;
          padding-top: 15px;
      }

      table.invoice-table {
          border-collapse: collapse;
          width: 100%;
      }

      table.invoice-table, .invoice-table th, .invoice-table td {
          border: 1px solid black;
          padding: 2px 4px;
      }

      .invoice-table th {
          font-size: 12px;
      }

      .invoice-footer-info {
          font-size: 12px;
          margin-top: 15px;
          border-bottom: 1px solid #000;
          padding-bottom: 10px;
          line-height: 24px;
      }

      .invoice-footer{
          display: block;
          padding-top: 30px;
          margin-bottom: 15px;
      }

      .invoice-footer .invoice-footer-col{
          display: inline-block;
          width: 32%;
          float: left;
      }

      .invoice-footer-field{

      }

      .invoice-footer-signature div:nth-child(2){
          margin-top: 4px
      }

      .check-box{
          display: inline-block;
          width: 20px;
          height: 20px;
          border: 1px solid #000;
          vertical-align: bottom;
      }

      .f-left{
          float: left !important;
      }

      .f-right{
          float: right !important;
      }

      .w-35{
          width: 35% !important;
      }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

}
