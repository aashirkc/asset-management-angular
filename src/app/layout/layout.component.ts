import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { NgxPermissionsService } from 'ngx-permissions';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class LayoutComponent implements OnInit {

    @ViewChild('navBar') navBar;
    @ViewChild('mainContainer') el:ElementRef;
    // @ViewChild('newtworkWindow') newtworkWindow:jqxWindowComponent;

    public online: boolean = true;

    private roleInfo: any;

   
    // private permissionInfo: any;
    constructor(
        public router: Router,
        private rd: Renderer2,
        private permissionsService: NgxPermissionsService,
    ) {
        // this.roleInfo = this.cus.getRole();
        // this.getRoleDetail(this.roleInfo['id']);
    }

    ngOnInit() {
        if (this.router.url === '/') {
            this.router.navigate(['/dashboard']);
        }
        // const perm = ["add-worker", "add-staff"];
        
        // this.permissionsService.loadPermissions(perm);

        // this.permissionsService.permissions$.subscribe((permissions) => {
        //     console.log(permissions);
        // })
    }

    ngAfterViewInit(){
    }

    /**
     * Get Current User Permissions assigned to Current user role
     */
    getRoleDetail(id: number){
        // this.rs.show(id).subscribe(
        //     response => {
        //         let permissions:any = response['data'] && response['data']['perms'] || [];
        //         const perms: any = [];
        //         for(let i = 0; i < permissions.length; i++){
        //             perms.push(permissions[i]['name']);
        //         }
        //         this.permissionsService.loadPermissions(perms);
        //         localStorage.setItem('teaERPPerms',JSON.stringify(perms));
        //         this.cus.currentUserPermissions = perms;
        //     },
        //     error => {
        //         console.log(error);
        //     }
        // );
    }

    /**
     * Detect Network Change Start
     */
    online$ = Observable.fromEvent(window, 'online').subscribe(
        x => {
            this.online = true;
            // this.newtworkWindow.close();
        }
    );

    offline$ = Observable.fromEvent(window, 'offline').subscribe(
        x => {
            this.online = false;
            // this.newtworkWindow.setTitle('NETWORK ERROR');
            // this.newtworkWindow.open();
        }
    );

    retry(){
        if(this.online){
            // this.newtworkWindow.close();
        }
        
    }

    // reload(){

    // }
    /**
     * Detect Network Change End
     */

    CanDeactivate() {
        console.log('i am navigating away');
        alert('Navigating Away');
        // if the editName !== this.user.name
        // if (this.user.name !== this.editName) {
          return window.confirm('Discard changes?');
        // }
      }  

}
