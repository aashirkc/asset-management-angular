import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];

    
    public Setup: any = {
        title: 'Setup',
        icon: 'fa-cog',
        items: [
            {
                name: 'Financial Year',
                route: '/setup/financial-year'
            },
            {
                name: 'Department Master',
                route: '/setup/department-master'
            },
            {
                name: 'Chart Of Items',
                route: '/setup/chart-of-items'
            },
            {
                name: 'Deprecitation Ratio',
                route: '/setup/depreciation-ratio'
            },
            {
                name: 'Branch Master',
                route: '/setup/branch-master'
            }
            ,
            {
                name: 'Supplier Master',
                route: '/setup/supplier-master'
            }
            ,
            {
                name: 'Site Master',
                route: '/setup/site-master'
            }
            ,
            {
                name: 'Asset Accessories Master',
                route: '/setup/asset-accessories-master'
            }
            // ,
            // {
            //     name: 'Asset Specification Master',
            //     route: '/asset-specification-master'
            // },
            // {
            //     name: 'Bill Sundry Master',
            //     route: '/bill-sundry-master'
            // }
        ]

    };
    public Operation: any = {
        title: 'Operation',
        icon: 'fa-users',
        items: [
            {
                name: 'Requisition Slip',
                route: '/operation/requisition-slip'
            },
            {
                name: 'Goods Issue',
                route: '/operation/goods-issue'
            },
            {
                name: 'Goods Issue Return',
                route: '/operation/goods-issue-return'
            },
            {
                name: 'Goods Return',
                route: '/operation/goods-return'
            },
            {
                name: 'Goods Disposal',
                route: '/operation/goods-disposal'
            },
            {
                name: 'Purchase Order',
                route: '/operation/purchase-order'
            },
            {
                name: 'Goods Receiving Notes',
                route: '/operation/goods-receiving-notes'
            },
            {
                name: 'Goods Receiving Notes Opening',
                route: '/operation/goods-receiving-notes-opening'
            }
        ]
    };
    public reports: any = {
        title: 'Reports',
        icon: 'fa-list-alt',
        items: [
            {
                name: 'Assets Ledger',
                route: '/report/assets-ledger'
            },
            {
                name: 'Assets Wise Report',
                route: '/report/assets-wise-reports'
            },
            {
                name: 'Requisition',
                route: '/report/requisition-report'
            },
            {
                name: 'Purchase Order',
                route: '/report/purchase-order-report'
            },
            {
                name: 'Goods Receiving Notes',
                route: '/report/goods-receiving-notes-report'
            },
            {
                name: 'Goods Issue Report',
                route: '/report/goods-issue-report'
            },
            {
                name: 'Goods Disposal Report',
                route: '/report/goods-disposal-report'
            },
            {
                name: 'Tax Depreciation Report',
                route: '/report/tax-depreciation-report'
            },
            // {
            //     name: 'Stateline Depreciation Report',
            //     route: '/stateline-depreciation'
            // },
            // {
            //     name: 'Item Repair Maintenance Report',
            //     route: '/item-repair-maintenance'
            // }
        ]

    };
    public utility: any = {
        title: 'Utility',
        icon: 'fa-clock-o',
        items: [
            {
                name: 'Approve Requisition',
                route: '/utility/approve-requisition'
            },
            {
                name: 'Approve Purchase Order',
                route: '/utility/approve-purchase-order'
            },
            {
                name: 'Approve Issue',
                route: '/utility/approve-issue'
            },
            {
                name: 'Yearly Repair Expenses',
                route: '/utility/yearly-repair-expenses'
            },
            {
                name: 'Item Repair',
                route: '/utility/item-repair'
            },
            {
                name: 'Set Serial Number',
                route: '/utility/set-serial-number'
            },
            {
                name: 'Manage Users',
                route: '/utility/user'
            },
            {
                name: 'Manage Permissions',
                route: '/utility/permission'
            },
            // {
            //     name: 'Backup',
            //     route: '/backup'
            // }
        ]

    };
    public loanLeaveMgmt: any = {
        title: 'Software',
        icon: 'fa-calendar',
        items: [
            {
                name: 'Software ',
                route: '/worker-loan-sanction'
            }
        ]

    };
    public misReport: any = {
        title: 'Tools',
        icon: 'fa-clipboard',
        items: [
            {
                name: 'tools',
                route: '/soe'
            }
        ]

    };
    public dailyReport: any = {
        title: 'Miscellaneous',
        icon: 'fa-file-text-o',
        items: [
            {
                name: 'Miscellaneous',
                route: '/kamjari-reports'
            }
        ]

    };
    public forNightlyReport: any = {
        title: 'Cost Group',
        icon: 'fa-book',
        items: [
            {
                name: 'Cost Group',
                route: '/paybook-summary'
            }
        ]

    };

    fiscalYear:string;
    branch:number;
    site:number;
    supplier:number;
    user:number;
    department:number;
    constructor(
        private translate: TranslateService,
    ) {
        let userData = JSON.parse(localStorage.getItem('fAssetUser'));
        let fy = userData['fy'];
        let fystart = fy.substr(0, 2);
        let fyend = fy.substr(2, 2);
        this.fiscalYear = fystart + '/' + fyend;
        this.branch = userData['branchCount']|| 0;
        this.site = userData['siteCount']|| 0;
        this.supplier = userData['supplierCount']|| 0;
        this.user = userData['userCount']|| 0;
        this.department = userData['departmentCount'] || 0;
        this.getTranslation();
    }
    transData: any;
  getTranslation() {
    this.translate.get(['FINANCIALYEAR','DEPARTMENT','CHARTOFITEMS','DEPRECIATIONRATION','BRANCH','SUPPLIER','SITE','ASSETACCESSORIESMASTER','REQUISITIONSLIP','GOODSISSUE','GOODSISSUERETURN','GOODSRETURN','GOODSDISPOSAL','PURCHASEORDER','GOODSRECEIVINGNOTES','GOODSRECEIVINGNOTESOPENING','ASSETSLEDGER','ASSETSWISEREPORT','REQUISITIONREPORT','PURCHASEORDERREPORT','GOODSRECEIPTNOTEREPORT','GOODSISSUEREPORT','GOODSDISPOSALREPORT','TAXDEPRECIATIONREPORT','APPROVEREQUISITION','APPROVEPO','APPROVEISSUE','YEARLYREPAIREXPENSES','ITEMREPAIR','GRNUPDATE','MANAGEUSER','MANAGEPERMISSION']).subscribe((translation: [string]) => {
      this.transData = translation;
      
    });
  }

    ngOnInit() {
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
}
