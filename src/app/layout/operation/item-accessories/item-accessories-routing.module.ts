import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemAccessoriesComponent } from './item-accessories.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: ItemAccessoriesComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemAccessoriesRoutingModule { }
