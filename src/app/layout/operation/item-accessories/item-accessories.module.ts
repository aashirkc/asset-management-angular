import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ItemAccessoriesComponent } from './item-accessories.component';
import { ItemAccessoriesRoutingModule } from './item-accessories-routing.module';
import { NgxBarcodeModule } from 'ngx-barcode';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NgxBarcodeModule,
    ItemAccessoriesRoutingModule
  ],
  declarations: [
    ItemAccessoriesComponent
  ]
})
export class ItemAccessoriesModule { }
