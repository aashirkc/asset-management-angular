import { Component, ViewChild, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ItemAccessoriesService, AssetAccessoriesMasterService, GoodsReceivingNotesService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';

@Component({
  selector: 'app-item-accessories',
  templateUrl: './item-accessories.component.html',
  styleUrls: ['./item-accessories.component.scss']
})
export class ItemAccessoriesComponent implements OnInit {
  itemForm: FormGroup;
  source: any;
  dataAdapter: any;
  itemsAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  deleteRowIndexes: Array<any> = [];
  barcodeValue:any;
  barcodeHeight:any;
  bcLineColor:any;
  rules:any = [];

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myWindow') myWindow: jqxWindowComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;

  grnNoParam: any;
  grnNoLocal: any;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private ias: ItemAccessoriesService,
    private aams: AssetAccessoriesMasterService,
    private grns: GoodsReceivingNotesService
  ) {
    this.grnNoParam = this.getRouteGrnParam();
    if (!this.grnNoParam) {
      let localGrnNo = localStorage.getItem('lastGrnNo');
      if (localGrnNo) {
        this.grnNoLocal = localGrnNo;
      }
    } else {
      localStorage.setItem('lastGrnNo', this.grnNoParam);
    }

    this.createForm();
    this.loadGrid();
  }

  getRouteGrnParam() {
    return this.route.snapshot.paramMap.get('grnNo');
  }

  ngOnInit() {

  }

  loadGridData() {
    this.jqxLoader.open();
    this.ias.index({}).subscribe((res) => {
      this.source.localdata = res;
      this.jqxLoader.close();
      this.myGrid.updatebounddata();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  createForm() {
    this.itemForm = this.fb.group({
      'itemNo': ['', Validators.required],
      'sameAccessoriesInAll': ['', Validators.required],
    });
  }


  loadItems() {
    let grnNo = this.getRouteGrnParam();
    if (grnNo) {
      this.jqxLoader.open();
      this.grns.itemByGrn({ 'grnNo': grnNo }).subscribe((res) => {
        console.info(res);
        this.itemsAdapter = res;
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });

    }
  }

  ngAfterViewInit() {
    this.loadItems();
    // this.loadGridData();
  }

  itemChange(itemNo) {
    console.log(itemNo);
    if (itemNo) {
      this.aams.show(itemNo).subscribe(
        response => {
          this.source.localdata = response;
          this.myGrid.updatebounddata();
        },
        error => {
          console.log(error);
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = 'Could not load accessories';
          this.errNotification.open();
        }
      )
    }
  }

  generateBarCode(){
    this.myWindow.draggable(true);
    this.barcodeValue = "1234gt";
    this.myWindow.title('Generated Barcode');
    this.myWindow.open();
  }

  printBarCode(): void {
    let printContents, popupWin;
    printContents = document.getElementById('printbarcode').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          @media (min-width: 1200px)
            .container {
                width: 1140px;
                max-width: 100%;
            }
            @media (min-width: 992px)
            .container {
                width: 960px;
                max-width: 100%;
            }
            @media (min-width: 768px)
            .container {
                width: 720px;
                max-width: 100%;
            }
            @media (min-width: 576px)
            .container {
                width: 540px;
                max-width: 100%;
            }
            @media (min-width: 1200px)
            .container {
                padding-right: 7.5px;
                padding-left: 7.5px;
            }
            @media (min-width: 992px)
            .container {
                padding-right: 7.5px;
                padding-left: 7.5px;
            }
            @media (min-width: 768px)
            .container {
                padding-right: 7.5px;
                padding-left: 7.5px;
            }
            @media (min-width: 576px)
            .container {
                padding-right: 7.5px;
                padding-left: 7.5px;
            }
            .container {
                position: relative;
                margin-left: auto;
                margin-right: auto;
                padding-right: 7.5px;
                padding-left: 7.5px;
            }
            @media (min-width: 1200px)
            .row {
                margin-right: -7.5px;
                margin-left: -7.5px;
            }
            @media (min-width: 992px)
            .row {
                margin-right: -7.5px;
                margin-left: -7.5px;
            }
            @media (min-width: 768px)
            .row {
                margin-right: -7.5px;
                margin-left: -7.5px;
            }
            @media (min-width: 576px)
            .row {
                margin-right: -7.5px;
                margin-left: -7.5px;
            }
            .row {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                margin-right: -7.5px;
                margin-left: -7.5px;
            }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'accessoriseId', type: 'string', map:'id' },
          { name: 'accessoriesName', type: 'string' },
          { name: 'mappingUnit', type: 'string' },
          { name: 'remarks', type: 'string' },
        ],
        id: 'id',
        localdata: [],
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);

    this.columns = [
      {
        text: 'S.N', sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: '', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: 'Accessories', datafield: 'id', displayfield: 'accessoriesName', columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      { text: 'Unit', datafield: 'mappingUnit', columntype: 'textbox', filterable: false, editable: false, width:'120' },
      { text: 'Remarks', datafield: 'remarks', columntype: 'textbox', filterable: false, editable: true }
    ];
    this.columngroups =
      [
        { text: 'Actions', align: 'center', name: 'action' },
      ];

  }


  saveBtn(post) {
    let grnNo = this.getRouteGrnParam();
    let ids = this.myGrid.getselectedrowindexes();
    let arrData = [];
    for (let i = 0; i < ids.length; i++) {
      let id = String(ids[i]);
      let data = this.myGrid.getrowdata(Number(id));
      arrData.push(data);
    }
    post['accessories'] = arrData;

    if (arrData.length > 0) {
      this.jqxLoader.open();
      this.ias.store(post).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
            this.source.localdata = [];
            this.myGrid.updatebounddata();
          }
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
          this.jqxLoader.close();
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please select Accessories.';
      this.errNotification.open();
    }
  }
}
