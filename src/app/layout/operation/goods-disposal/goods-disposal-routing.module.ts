import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoodsDisposalComponent } from './goods-disposal.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: GoodsDisposalComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoodsDisposalRoutingModule { }
