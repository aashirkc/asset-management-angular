import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { GoodsDisposalComponent } from './goods-disposal.component';
import { GoodsDisposalRoutingModule } from './goods-disposal-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    GoodsDisposalRoutingModule
  ],
  declarations: [
    GoodsDisposalComponent
  ]
})
export class GoodsDisposalModule { }
