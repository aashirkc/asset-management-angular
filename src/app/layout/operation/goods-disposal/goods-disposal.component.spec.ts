import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsDisposalComponent } from './goods-disposal.component';

describe('GoodsDisposalComponent', () => {
  let component: GoodsDisposalComponent;
  let fixture: ComponentFixture<GoodsDisposalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsDisposalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsDisposalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
