import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OperationComponent } from './operation.component';

const routes: Routes = [
    {
        path: '',
        component: OperationComponent,
        children: [
            { path:'', redirectTo: 'requisition-slip', pathMatch:'full'},
            { path:'goods-receiving-notes', loadChildren:'./goods-receiving-notes/goods-receiving-notes.module#GoodsReceivingNotesModule'},
            { path:'goods-receiving-notes-opening', loadChildren:'./goods-receiving-notes-opening/goods-receiving-notes-opening.module#GoodsReceivingNotesOpeningModule'},
            { path:'requisition-slip', loadChildren:'./requisition-slip/requisition-slip.module#RequisitionSlipModule'},
            { path:'purchase-order', loadChildren:'./purchase-order/purchase-order.module#PurchaseOrderModule'},
            
            // Same Route Can be assessed in two ways
            { path:'item-accessories', loadChildren:'./item-accessories/item-accessories.module#ItemAccessoriesModule'},
            { path:'item-accessories/:grnNo', loadChildren:'./item-accessories/item-accessories.module#ItemAccessoriesModule'},
            
            // Same Route Can be assessed in two ways
            { path:'item-specification', loadChildren:'./item-specification/item-specification.module#ItemSpecificationModule'},
            { path:'item-specification/:grnNo', loadChildren:'./item-specification/item-specification.module#ItemSpecificationModule'},
            
            // Same Route Can be assessed in two ways
            { path:'item-insurance', loadChildren:'./item-insurance/item-insurance.module#ItemInsuranceModule'},
            { path:'item-insurance/:grnNo', loadChildren:'./item-insurance/item-insurance.module#ItemInsuranceModule'},

            { path:'item-warranty', loadChildren:'./item-warranty/item-warranty.module#ItemWarrantyModule'},
            { path:'item-warranty/:grnNo', loadChildren:'./item-warranty/item-warranty.module#ItemWarrantyModule'},

            { path:'goods-issue', loadChildren:'./goods-issue/goods-issue.module#GoodsIssueModule'},
            { path:'goods-issue-return', loadChildren:'./goods-issue-return/goods-issue-return.module#GoodsIssueReturnModule'},
            { path:'goods-return', loadChildren:'./goods-return/goods-return.module#GoodsReturnModule'},
            { path:'goods-disposal', loadChildren:'./goods-disposal/goods-disposal.module#GoodsDisposalModule'},
        ]
    },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OperationRoutingModule { }
