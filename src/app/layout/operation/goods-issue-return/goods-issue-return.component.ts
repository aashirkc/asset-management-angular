import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RequisitionSlipService,EmployeeDetailsService,CurrentUserService,SiteMasterService, ChartOfItemsService, BranchMasterService, DateFormatService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
  selector: 'app-goods-issue-return',
  templateUrl: './goods-issue-return.component.html',
  styleUrls: ['./goods-issue-return.component.scss']
})
export class GoodsIssueReturnComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;


  rsForm: FormGroup;
  itemAdapter: any = [];
  siteAdapter: any = [];
  personAdapter:any = [];
  itemFocus: any = [];
  branchAdapter: any = [];
  userData: any = {};
  deleteRowIndexes: Array<any> = [];

  constructor(
    private fb: FormBuilder,
    private rss: RequisitionSlipService,
    private cois: ChartOfItemsService,
    private bms: BranchMasterService,
    private sms:SiteMasterService,
    private cus: CurrentUserService,
    private eds:EmployeeDetailsService,
    private dfs: DateFormatService
  ) {
    this.createForm();
    this.itemAdapter[0] = [];
    this.branchAdapter[0] = [];
    this.itemFocus[0] = false;
    this.userData = this.cus.getTokenData();
  }

  createForm() {
    this.rsForm = this.fb.group({
      'returnDate': [null, Validators.required],
      'returnBy': [null, Validators.required],
      'requisitionItems': this.fb.array([
        this.initReqItems(),
      ]),
    });
  }

  initReqItems(){
    return this.fb.group({
      itemNo: [null, Validators.required],
      returnSite: [null, Validators.required],
      returnPerson: [null, Validators.required],
      returnBranch: [null, Validators.required],
      remarks: [null, Validators.required]
    });
  }

  itemFilter(searchPr, index){
    let branch = this.rsForm.get('requisitionItems')['controls'][index].get('returnBranch').value || '';
    let site = this.rsForm.get('requisitionItems')['controls'][index].get('returnSite').value || '';
    let person = this.rsForm.get('requisitionItems')['controls'][index].get('returnPerson').value || '';
    let post = {};
    post['branch'] = branch;
    post['site'] = site;
    post['person'] = person;
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemNo').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(' ', '');
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocus[index] = true;
        this.cois.indexItemIssueReturn(post,searchString).subscribe(
          response => {
            console.info(response);
            this.itemAdapter[index] = [];
            this.itemAdapter[index] = response;
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.itemFocus[index] = false;
      }
    }
  }

  itemListSelected(selectedValue, index){
    if(selectedValue){
      this.rsForm.get('requisitionItems')['controls'][index].get('itemNo').patchValue(selectedValue);
    }
  }

  branchSelected(selectedValue, index){
    if(selectedValue){
      this.jqxLoader.open();
      this.sms.getSitebyBranchCode(selectedValue).subscribe(
        response => {
          this.siteAdapter[index] = [];
          this.siteAdapter[index] = response;
          this.jqxLoader.close();
        },
        error => {
          console.log(error);
          this.jqxLoader.close();
        }
      );
      this.jqxLoader.open();
      let post = {};
      post['branch'] = selectedValue;
      this.eds.index(post).subscribe(
        response => {
          this.personAdapter[index] = [];
          this.personAdapter[index] = response;
          this.jqxLoader.close();
        },
        error => {
          console.log(error);
          this.jqxLoader.close();
        }
      );
    }
  }

  addItem(){
    const control1 = <FormArray>this.rsForm.controls['requisitionItems'];
    control1.push(this.initReqItems());
  }

  removeItem(i: number) {
      const control1 = <FormArray>this.rsForm.controls['requisitionItems'];
      control1.removeAt(i);
      this.itemAdapter.splice(i, 1);
  }

  comboSource: any;

  ngOnInit() {

    let Data = JSON.parse(localStorage.getItem('fAssetUser'))


    let Date1 = Data['fiscalYear']
    let startDate = Date1['startDateBS']
    let todayDate = Date1['endDateBS']
    console.log(startDate)

    this.rsForm.get('returnDate').patchValue(todayDate);

    this.bms.index({}).subscribe(
      response => {
        if (response.length == 1 &&  response[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText =  response[0].error;
          this.errNotification.open();
          this.branchAdapter = [];
        } else {
          this.branchAdapter = response;
        }
      },
      error => {
        console.log(error)
      }
    )
  }



  ngAfterViewInit() {
    this.dfs.currentDate().subscribe(
      response => {
        let dateData = response[0];
        setTimeout(() => {

          this.rsForm.controls['returnBy'].setValue(this.userData['user'].userName);
          this.rsForm.get('returnBy').markAsTouched();
        }, 100);
      },
      error => {
        console.log(error);
      }
    )

  }

  save(formData) {
    this.jqxLoader.open();
    this.cois.storeItemIssueReturn(formData).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }

}
