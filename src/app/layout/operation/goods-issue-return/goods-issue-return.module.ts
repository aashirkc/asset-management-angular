import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { GoodsIssueReturnComponent } from './goods-issue-return.component';
import { GoodsIssueReturnRoutingModule } from './goods-issue-return-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    GoodsIssueReturnRoutingModule
  ],
  declarations: [
    GoodsIssueReturnComponent
  ]
})
export class GoodsIssueReturnModule { }
