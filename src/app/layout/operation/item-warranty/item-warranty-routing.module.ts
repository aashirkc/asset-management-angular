import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemWarrantyComponent } from './item-warranty.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: ItemWarrantyComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemWarrantyRoutingModule { }
