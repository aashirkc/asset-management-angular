import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ItemWarrantyComponent } from './item-warranty.component';
import { ItemWarrantyRoutingModule } from './item-warranty-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ItemWarrantyRoutingModule
  ],
  declarations: [ItemWarrantyComponent]
})
export class ItemWarrantyModule { }
