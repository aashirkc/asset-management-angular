import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ItemWarrantyService, DateFormatService, GoodsReceivingNotesService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-item-warranty',
  templateUrl: './item-warranty.component.html',
  styleUrls: ['./item-warranty.component.scss']
})
export class ItemWarrantyComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

  /**
* Global Variable decleration
*/
  iwForm: FormGroup;

  itemsAdapter: any;

  grnNoParam: any;
  grnNoLocal: any;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private dfs: DateFormatService,
    private grns: GoodsReceivingNotesService,
    private iws: ItemWarrantyService
  ) {
    this.createForm();

    this.grnNoParam = this.getRouteGrnParam();
    if (!this.grnNoParam) {
      let localGrnNo = localStorage.getItem('lastGrnNo');
      if (localGrnNo) {
        this.grnNoLocal = localGrnNo;
      }
    } else {
      localStorage.setItem('lastGrnNo', this.grnNoParam);
    }
  }

  getRouteGrnParam() {
    return this.route.snapshot.paramMap.get('grnNo');
  }

  /**
* Create the form group 
* with given form control name 
*/
  createForm() {
    this.iwForm = this.fb.group({
      'itemNo': [null, Validators.required],
      'warrantyCompany': [null, Validators.required],
      'warrantyCompanyAddress': [null, Validators.required],
      'contactPerson': [null, Validators.required],
      'contactNo': [null, Validators.required],
      'warrantyFrom': [null, Validators.required],
      'warrantyMonth': [null, Validators.required],
      'sameInAll': [null, Validators.required]
    });
  }


  ngOnInit() {
  }

  ngAfterViewInit() {
    this.loadItems();
    this.dfs.currentDate().subscribe(
      response => {
        console.log(response[0].BS_DATE);
        this.iwForm.get('warrantyFrom').setValue(response[0].BS_DATE);
      },
      error => {
        console.log(error);
      }
    );
  }

  loadItems() {
    let grnNo = this.getRouteGrnParam();
    if (grnNo) {
      this.jqxLoader.open();
      this.grns.itemByGrn({ 'grnNo': grnNo }).subscribe((res) => {
        this.itemsAdapter = res;
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });

    }
  }

  save(post) {
    console.log(post);
    if (post) {
      this.jqxLoader.open();
      this.iws.store(post).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
          this.jqxLoader.close();
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please Enter All Fields';
      this.errNotification.open();
    }
  }

}
