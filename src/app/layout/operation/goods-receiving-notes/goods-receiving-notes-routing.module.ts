import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoodsReceivingNotesComponent } from './goods-receiving-notes.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: GoodsReceivingNotesComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoodsReceivingNotesRoutingModule { }
