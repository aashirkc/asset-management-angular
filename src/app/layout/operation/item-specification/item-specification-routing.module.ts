import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemSpecificationComponent } from './item-specification.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: ItemSpecificationComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemSpecificationRoutingModule { }
