import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ItemSpecificationComponent } from './item-specification.component';
import { ItemSpecificationRoutingModule } from './item-specification-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ItemSpecificationRoutingModule
  ],
  declarations: [
    ItemSpecificationComponent
  ]
})
export class ItemSpecificationModule { }
