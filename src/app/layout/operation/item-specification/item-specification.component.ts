import { Component, ViewChild, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ItemSpecificationService,AssetSpecificationMasterService, GoodsReceivingNotesService  } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';

@Component({
  selector: 'app-item-specification',
  templateUrl: './item-specification.component.html',
  styleUrls: ['./item-specification.component.scss']
})
export class ItemSpecificationComponent implements OnInit {
  departmentForm: FormGroup;
  source: any;
  dataAdapter: any;
  itemsAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  deleteRowIndexes:Array<any> = [];

  rules:any = [];

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;

  grnNoParam: any;
  grnNoLocal: any;

   constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private iss: ItemSpecificationService,
    private asms:AssetSpecificationMasterService,
    private grns: GoodsReceivingNotesService
  ) {
    this.createForm();
    this.loadGrid();

    this.grnNoParam = this.getRouteGrnParam();
    if (!this.grnNoParam) {
      let localGrnNo = localStorage.getItem('lastGrnNo');
      if (localGrnNo) {
        this.grnNoLocal = localGrnNo;
      }
    } else {
      localStorage.setItem('lastGrnNo', this.grnNoParam);
    }
  }

  getRouteGrnParam() {
    return this.route.snapshot.paramMap.get('grnNo');
  }

  ngOnInit() {
  }

  loadItems() {
    let grnNo = this.getRouteGrnParam();
    if (grnNo) {
      this.jqxLoader.open();
      this.grns.itemByGrn({ 'grnNo': grnNo }).subscribe((res) => {
        console.info(res);
        this.itemsAdapter = res;
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });

    }
  }

  loadGridData() {
    this.jqxLoader.open();
    this.iss.index({}).subscribe((res) => {
      this.source.localdata = res;
      this.jqxLoader.close();
      this.myGrid.updatebounddata();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }
  createForm() {
    this.departmentForm = this.fb.group({
      'itemNo': ['', Validators.required],
      'sameSpecificationIdInAll': ['', Validators.required],
    });
  }

  itemChange(itemNo) {
    console.log(itemNo);
    if (itemNo) {
      this.jqxLoader.open();
      this.asms.show(itemNo).subscribe(
        response => {
          this.source.localdata = response;
          this.source.totalrecords = response.length;
          this.myGrid.updatebounddata();
          this.jqxLoader.close();
        },
        error => {
          console.log(error);
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = 'Could not load accessories';
          this.errNotification.open();
          this.jqxLoader.close();
        }
      )
    }
  }


  ngAfterViewInit() {
    // this.loadGridData();
    this.loadItems();
  }


  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'specificationId', type: 'string', map:'id' },
          { name:'specificationName', type:'string'},
          { name: 'mappingUnit', type: 'string' },
          { name: 'remarks', type: 'string' },
        ],
        id: 'id',
        pagesize:20,
        localdata: [],
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);

    this.columns = [
      {
        text: 'S.N', sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: '', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: 'Specification', datafield: 'specificationId', displayfield:'specificationName', columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      { text: 'Unit', datafield: 'mappingUnit', columntype: 'textbox', filterable: false, editable: false, width:'120' },
      { text: 'Remarks', datafield: 'remarks', columntype: 'textbox', filterable: false, editable: true }
    ];

  }

  saveBtn(post) {
    let grnNo = this.getRouteGrnParam();
    let ids = this.myGrid.getselectedrowindexes();
    let arrData = [];
    for (let i = 0; i < ids.length; i++) {
      let id = String(ids[i]);
      let data = this.myGrid.getrowdata(Number(id));
      arrData.push(data);
    }
    post['specifications'] = arrData;

    if (arrData.length > 0) {
      this.jqxLoader.open();
      this.iss.store(post).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
            
            this.source.localdata =[];
            this.myGrid.updatebounddata();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    }
  }
}
