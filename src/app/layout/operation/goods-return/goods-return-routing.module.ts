import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoodsReturnComponent } from './goods-return.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: GoodsReturnComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoodsReturnRoutingModule { }
