import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { GoodsReturnComponent } from './goods-return.component';
import { GoodsReturnRoutingModule } from './goods-return-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    GoodsReturnRoutingModule
  ],
  declarations: [
    GoodsReturnComponent
  ]
})
export class GoodsReturnModule { }
