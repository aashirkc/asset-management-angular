import { Component, OnInit, ElementRef, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RequisitionSlipService, EmployeeDetailsService, CurrentUserService, GoodsIssueService, ChartOfItemsService, BranchMasterService, DateFormatService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-goods-issue',
  templateUrl: './goods-issue.component.html',
  styleUrls: ['./goods-issue.component.scss']
})
export class GoodsIssueComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('input1') inputEl: ElementRef;
  @ViewChild('myWindow') myWindow: jqxWindowComponent;
  @ViewChild('myHistoryWindow') myHistoryWindow: jqxWindowComponent;

  /**
   * Global Variable decleration
   */
  rsForm: FormGroup;
  fiscalYears: Array<any> = [];
  branchs: Array<any> = [];
  sites: Array<any> = [];
  issueItems: any;
  userData: any = {};
  windowDatas: any = [];
  accessoriesDatas: any = [];
  specificationDatas: any = [];
  insurances: any;
  warranty: any;
  masterName: any;
  historyData: any = {};
  // EditData: GoodsIssue;

  /**
   * itemAdater variable is used to load option for select based
   * item input field value
   */
  itemAdapter: any = [];

  /**
   * itemFocus Variable is used to hide/show item select field after
   * item have been selected in select input
   *
   */
  itemFocus: any = [];
  branchAdapter: any = [];
  personAdapter: any = [];

  /**
   * Unit Type
   */
  unitAdapter: any = [
    { name: 'PCS', value: 'PCS' },
    { name: 'PKT', value: 'PKT' },
    { name: 'SET', value: 'SET' },
    { name: 'BOX', value: 'BOX' },
    { name: 'KG', value: 'KG' },
    { name: 'METER', value: 'MTR' },
  ]

  // deleteRowIndexes: Array<any> = [];

  constructor(
    private fb: FormBuilder,
    private rss: RequisitionSlipService,
    private cois: ChartOfItemsService,
    private bms: BranchMasterService,
    private gis: GoodsIssueService,
    private cus: CurrentUserService,
    private currentActivatedRoute: ActivatedRoute,
    private eds: EmployeeDetailsService,
    private router: Router,
    private dfs: DateFormatService
  ) {
    this.createForm();
    this.itemAdapter[0] = [];
    this.itemFocus[0] = false;
    this.userData = this.cus.getTokenData();
    console.info(this.userData);
  }

  /**
   * Create the form group
   * with given form control name
   */
  createForm() {
    this.rsForm = this.fb.group({
      'issueDate': [null],
      'issueOrderBy': [null],
      'branchCode': [''],
      'fySymbol': [''],
      'sideCode': [''],
      'personName': [''],
      'requisitionItems': this.fb.array([
        this.initReqItems(),
      ]),
    });
  }

  initReqItems() {
    return this.fb.group({
      itemNo: [null,Validators.required],
      requestingNo: [null],
      receivePerson: [null],
      remarks: [null],
      issueStatus: [null]
    });
  }

  /**
   * itemFilter Event is called when Item input field has
   * keyup action followed by 'Enter'
   * Generate Suggestion based on input value entered
   * @param searchString
   * @param index
   */
  itemFilter(searchPr, index) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemNo').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(' ', '');
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocus[index] = true;
        this.cois.showItem(searchString).subscribe(
          response => {
            this.itemAdapter[index] = [];
            this.itemAdapter[index] = response;
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.itemFocus[index] = false;
      }
    }

  }

  /**
   * Event fired when option is selected from Item Suggestion Select field
   * Hide Select field after Item Selected.
   * @param selectedValue
   * @param index
   */
  itemListSelected(selectedValue, index) {
    if (selectedValue) {
      this.rsForm.get('requisitionItems')['controls'][index].get('itemNo').patchValue(selectedValue);
    }
  }

  /**
   * Add FormGroup to Requisition Item FormArray
   * Increments Requestion Item FormArray
   */
  addItem() {
    const control1 = <FormArray>this.rsForm.controls['requisitionItems'];
    control1.push(this.initReqItems());
    // console.log(control1.length);
  }

  /**
   * Remove FormGroup at particular position form Requisition Item FormArray
   * Decrements Requestion Item FormArray
   */
  removeItem(i: number) {
    const control1 = <FormArray>this.rsForm.controls['requisitionItems'];
    control1.removeAt(i);
    /**
     * Remove itemAdapter itemArray at Particular position i,
     * Select Field option for select field at position 'i' of formArray
     */
    this.itemAdapter.splice(i, 1);
  }
  RemoveAll() {
    let formData = this.rsForm.get('requisitionItems')['controls'];
    if (formData.length > 1) {
      for (let j = 0; j < formData.length; j++) {
        this.removeItem(j);
      }
    }
  }

  comboSource: any;

  ngOnInit() {

    let Data = JSON.parse(localStorage.getItem('fAssetUser'))


    let Date1 = Data['fiscalYear']
    let startDate = Date1['startDateBS']
    let todayDate = Date1['endDateBS']
    console.log(startDate)

    this.rsForm.get('issueDate').patchValue(todayDate);

    this.bms.index({}).subscribe(
      response => {
        this.branchAdapter = response;
      },
      error => {
        console.log(error)
      }
    )
  }


  ngAfterViewInit() {
    this.jqxLoader.open();
    this.gis.index({}).subscribe((res) => {
      this.fiscalYears = res[0];
      this.rsForm.controls['fySymbol'].setValue(this.fiscalYears[0].fySymbol);
      this.branchs = res[1];

      this.jqxLoader.close();

    }, (error) => {
      this.jqxLoader.close();
    });

    this.dfs.currentDate().subscribe(
      response => {
        let dateData = response[0];
        setTimeout(() => {

          this.rsForm.controls['issueOrderBy'].setValue(this.userData['user'].userName);
          this.rsForm.get('issueOrderBy').markAsTouched();
        }, 100);
      },
      error => {
        console.log(error);
      }
    )
  }

  branchChange(event: any) {
    this.sites = [];
    this.jqxLoader.open();
    this.gis.indexBranch(event).subscribe((res) => {
      this.sites = res;
      this.jqxLoader.close();
    }, (error) => {
      this.jqxLoader.close();
    });
    let post = {};
    post['branch'] = event;
    this.eds.index(post).subscribe((response) => {
      this.personAdapter = response;
    }, (error) => {
      console.info(error);
    });
  }


  search(post) {
    this.jqxLoader.open();
    this.issueItems = [];
    let arrayData = [];
    this.rsForm.setControl('requisitionItems', this.fb.array([]));
    this.gis.searchBranch(post).subscribe((res) => {
      for (let i = 0; i < res.length; i++) {
        for (let j = 0; j < res[i]['requestingQty']; j++) {
          let dt = {};
          dt['branchName'] = res[i]['branchName'];
          dt['itemCode'] = res[i]['itemCode'];
          dt['itemName'] = res[i]['itemName'];
          dt['siteName'] = res[i]['siteName'];
          dt['requestingNo'] = res[i]['requestingNo'];
          arrayData.push(dt);
        }
      }
      this.issueItems = arrayData;
      let tempLen = this.issueItems.length;
      for (let i = 0; i < tempLen; i++) {
        this.addItem();
        this.setEditData(i, arrayData[i]);
      }
      this.jqxLoader.close();
    }, (error) => {
      this.jqxLoader.close();
    });
  }

  ViewHistory(post) {
    if (post['personName']) {
      let arrayData = this.personAdapter.filter(
        x => x.empCode === post['personName']);
      let data = {
        requestingPerson: arrayData[0]['empCode'],
        name: arrayData[0]['fullName']
      };
      this.historyData = data;
      this.myHistoryWindow.draggable(true);
      this.myHistoryWindow.title('View History');
      this.myHistoryWindow.open();
      // this.router.navigate(['../history-requisition.component'], { queryParams: data, relativeTo: this.currentActivatedRoute });
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = "Please Select Person Name First!!";
      this.errNotification.open();
    }
  }

  setEditData(index, data) {
    this.rsForm.get('requisitionItems')['controls'][index].get('requestingNo').patchValue(data['requestingNo']);
    this.rsForm.get('requisitionItems')['controls'][index].get('itemNo').patchValue(data['itemName']);
  }

  checkAll(event: any) {
    if (event.target.checked) {
      for (let i = 0; i < this.issueItems.length; i++) {
        this.rsForm.get('requisitionItems')['controls'][i].get('issueStatus').patchValue(true);
      }
    } else {
      for (let i = 0; i < this.issueItems.length; i++) {
        this.rsForm.get('requisitionItems')['controls'][i].get('issueStatus').patchValue(false);
      }
    }
  }
  viewItem(itemNo) {
    if (Number(itemNo)) {
      let data = {
        itemNo: itemNo
      };
      this.router.navigate(['../goods-issue-view'], { queryParams: data, relativeTo: this.currentActivatedRoute });
      // this.jqxLoader.open();
      // this.gis.showItemProperty(itemNo).subscribe((response) => {
      //   this.windowDatas = response;
      //   this.accessoriesDatas = response[0];
      //   this.specificationDatas = response[1];
      //   this.insurances = response[2] && response[2][0] || null;
      //   this.warranty = response[3] && response[3][0] || null;
      //   this.jqxLoader.close();
      //   this.myWindow.draggable(true);
      //   this.myWindow.title('View Item');
      //   this.myWindow.open();
      // }, (error) => {
      //   this.jqxLoader.close();
      // })
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = "Please Select Item No First !!";
      this.errNotification.open();
    }
  }
  /**
   * Function triggered when save button is clicked
   * @param formData
   */
  save(formData) {
    let issueItem = [];
    for (let i = 0; i < formData['requisitionItems'].length; i++) {
      if (formData['requisitionItems'][i]['itemNo'] && formData['requisitionItems'][i]['issueStatus'] == true) {
        issueItem.push(formData['requisitionItems'][i]);
      }
    }
    formData['requisitionItems'] = issueItem;
    formData['receiveBranch'] = formData['branchCode'];
    formData['receiveSite'] = formData['sideCode'];

    console.log(formData);
    this.jqxLoader.open();
    this.rss.storeIssue(formData).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
}
