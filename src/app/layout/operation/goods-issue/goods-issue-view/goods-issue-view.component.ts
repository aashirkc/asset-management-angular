import { Component, OnInit, } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GoodsIssueService } from '../../../../shared';
@Component({
  selector: 'app-goods-issue-view',
  templateUrl: './goods-issue-view.component.html',
  styleUrls: ['./goods-issue-view.component.scss']
})
export class GoodsIssueViewComponent implements OnInit {

  windowDatas: any = [];
  accessoriesDatas: any = [];
  specificationDatas: any = [];
  insurances: any;
  warranty: any;
  constructor(private activatedRoute: ActivatedRoute,
    private gis: GoodsIssueService,
  ) {

  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      console.log(params['itemNo']);
      // if (Number(params['itemNo'])) {
        // this.jqxLoader.open();
        this.gis.showItemProperty(params['itemNo']).subscribe((response) => {
          this.windowDatas = response;
          this.accessoriesDatas = response[0];
          this.specificationDatas = response[1];
          this.insurances = response[2] && response[2][0] || null;
          this.warranty = response[3] && response[3][0] || null;
        }, (error) => {
          // this.jqxLoader.close();
        })
      // }
    });
  }

}
