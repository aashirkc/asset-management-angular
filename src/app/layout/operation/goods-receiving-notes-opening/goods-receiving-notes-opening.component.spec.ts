import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsReceivingNotesOpeningComponent } from './goods-receiving-notes-opening.component';

describe('GoodsReceivingNotesOpeningComponent', () => {
  let component: GoodsReceivingNotesOpeningComponent;
  let fixture: ComponentFixture<GoodsReceivingNotesOpeningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsReceivingNotesOpeningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsReceivingNotesOpeningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
