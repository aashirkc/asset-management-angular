import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { GoodsReceivingNotesOpeningComponent } from './goods-receiving-notes-opening.component';
import { GoodsReceivingNotesOpeningRoutingModule } from './goods-receiving-notes-opening-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    GoodsReceivingNotesOpeningRoutingModule
  ],
  declarations: [GoodsReceivingNotesOpeningComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class GoodsReceivingNotesOpeningModule { }
