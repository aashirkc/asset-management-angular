import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoodsReceivingNotesOpeningComponent } from './goods-receiving-notes-opening.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: GoodsReceivingNotesOpeningComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoodsReceivingNotesOpeningRoutingModule { }
