import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { RequisitionSlipComponent } from './requisition-slip.component';
import { RequisitionSlipRoutingModule } from './requisition-slip-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RequisitionSlipRoutingModule,
  ],
  declarations: [RequisitionSlipComponent]
})
export class RequisitionSlipModule { }
