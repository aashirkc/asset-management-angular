import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequisitionSlipComponent } from './requisition-slip.component';

describe('RequisitionSlipComponent', () => {
  let component: RequisitionSlipComponent;
  let fixture: ComponentFixture<RequisitionSlipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequisitionSlipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequisitionSlipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
