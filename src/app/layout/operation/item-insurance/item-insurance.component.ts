import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ItemInsuranceService, DateFormatService, GoodsReceivingNotesService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-item-insurance',
  templateUrl: './item-insurance.component.html',
  styleUrls: ['./item-insurance.component.scss']
})
export class ItemInsuranceComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

  /**
  * Global Variable decleration
  */
  iiForm: FormGroup;

  itemsAdapter: any;

  grnNoParam: any;
  grnNoLocal: any;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private dfs: DateFormatService,
    private grns: GoodsReceivingNotesService,
    private iis: ItemInsuranceService
  ) {
    this.createForm();

    this.grnNoParam = this.getRouteGrnParam();
    if (!this.grnNoParam) {
      let localGrnNo = localStorage.getItem('lastGrnNo');
      if (localGrnNo) {
        this.grnNoLocal = localGrnNo;
      }
    } else {
      localStorage.setItem('lastGrnNo', this.grnNoParam);
    }
  }


  getRouteGrnParam() {
    return this.route.snapshot.paramMap.get('grnNo');
  }

  /**
* Create the form group 
* with given form control name 
*/
  createForm() {
    this.iiForm = this.fb.group({
      'itemNo': [null, Validators.required],
      'insurancesCompaniesCode': [null, Validators.required],
      'insurancesCompaniesName': [null, Validators.required],
      'address': [null, Validators.required],
      'officeContactNo': [null, Validators.required],
      'contactPerson': [null, Validators.required],
      'contactPersonNo': [null, Validators.required],
      'insuranceAmount': [null, Validators.required],
      'insuranceDateFrom': [null, Validators.required],
      'insuranceDateTo': [null, Validators.required],
      'sameInAll': [null, Validators.required]
    });
  }

  ngOnInit() {
  }

  loadItems() {
    let grnNo = this.getRouteGrnParam();
    if (grnNo) {
      this.jqxLoader.open();
      this.grns.itemByGrn({ 'grnNo': grnNo }).subscribe((res) => {
        console.info(res);
        this.itemsAdapter = res;
        this.jqxLoader.close();
      }, (error) => {
        console.info(error);
        this.jqxLoader.close();
      });

    }
  }

  ngAfterViewInit() {
    this.loadItems();
    // this.loadGridData();
  }

  save(post) {
    if (post) {
      this.jqxLoader.open();
      this.iis.store(post).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
          this.jqxLoader.close();
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please Enter All Fields';
      this.errNotification.open();
    }
  }

}
