import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemInsuranceComponent } from './item-insurance.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: ItemInsuranceComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemInsuranceRoutingModule { }
