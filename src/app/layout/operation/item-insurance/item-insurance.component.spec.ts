import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemInsuranceComponent } from './item-insurance.component';

describe('ItemInsuranceComponent', () => {
  let component: ItemInsuranceComponent;
  let fixture: ComponentFixture<ItemInsuranceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemInsuranceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemInsuranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
