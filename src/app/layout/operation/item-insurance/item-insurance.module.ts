import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ItemInsuranceComponent } from './item-insurance.component';
import { ItemInsuranceRoutingModule } from './item-insurance-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ItemInsuranceRoutingModule
  ],
  declarations: [ItemInsuranceComponent]
})
export class ItemInsuranceModule { }
