import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/modules/shared.module';
import { OperationComponent } from './operation.component';
import { OperationRoutingModule } from './operation-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    OperationRoutingModule
  ],
  declarations: [OperationComponent]
})
export class OperationModule { }
