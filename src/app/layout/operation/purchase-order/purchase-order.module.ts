import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { PurchaseOrderComponent } from './purchase-order.component';
import { PurchaseOrderRoutingModule } from './purchase-order-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PurchaseOrderRoutingModule
  ],
  declarations: [PurchaseOrderComponent]
})
export class PurchaseOrderModule { }
