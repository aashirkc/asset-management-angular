import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemRepairMaintenanceComponent } from './item-repair-maintenance.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: ItemRepairMaintenanceComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemRepairMaintenanceRoutingModule { }
