import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ChartOfItemsService, SiteMasterService, FinancialYearService, BranchMasterService, AllReportService, DateFormatService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxInputComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxinput';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
  selector: 'app-item-repair-maintenance',
  templateUrl: './item-repair-maintenance.component.html',
  styleUrls: ['./item-repair-maintenance.component.scss']
})
export class ItemRepairMaintenanceComponent implements OnInit {
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('dateFrom') dateFrom: jqxInputComponent;

  irmForm: FormGroup;

  itemAdapter: any;
  itemStoreAdapter:any;
  itemFocus: boolean = false;
  itemFocusStore: boolean = false;
  reportDatas: any = [];
  dateData: any;

  constructor(
    private fb: FormBuilder,
    private cois: ChartOfItemsService,
    private bms: BranchMasterService,
    private report: AllReportService,
    private sms: SiteMasterService,
    private date: DateFormatService,
    private fys: FinancialYearService
  ) {
    this.createForm();
  }

  ngOnInit() {
    let Data = JSON.parse(localStorage.getItem('fAssetUser'))


    let Date1 = Data['fiscalYear']
    let startDate = Date1['startDateBS']
    let todayDate = Date1['endDateBS']
    console.log(startDate)
    this.irmForm.get('dateFrom').patchValue(startDate);
    this.irmForm.get('dateTo').patchValue(todayDate)
  }

  itemFilter(searchPr) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemCode').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(' ', '');
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocus = true;
        this.cois.show(searchString).subscribe(
          response => {
            this.itemAdapter = response;
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.itemFocus = false;
      }
    }

  }

  itemListSelected(selectedValue) {
    if (selectedValue) {
      this.irmForm.controls['itemCode'].setValue(selectedValue);
    }
  }

  itemStoreFilter(searchPr) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemNo').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(' ', '');
    let itemCode =  this.irmForm.get('itemCode').value;
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocusStore = true;
        this.cois.showItemMaintenance(searchString.replace(/\s/g,''),itemCode).subscribe(
          response => {
            this.itemStoreAdapter = [];
            this.itemStoreAdapter = response;
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.itemFocusStore = false;
      }
    }

  }

  itemStoreListSelected(selectedValue) {
    if (selectedValue) {
      this.irmForm.controls['itemNo'].setValue(selectedValue);
    }
  }

  ngAfterViewInit() {
    // this.date.currentDate().subscribe(
    //   response => {
    //     this.dateData = response[0];
    //     setTimeout(() => {
    //       this.irmForm.controls['dateFrom'].setValue(this.dateData['fy_StartBs']);
    //       this.irmForm.get('dateFrom').markAsTouched();
    //       this.irmForm.controls['dateTo'].setValue(this.dateData['BS_DATE']);
    //       this.irmForm.get('dateTo').markAsTouched();
    //     }, 100);
    //   },
    //   error => {
    //     console.log(error);
    //   }
    // )
  }

  createForm() {
    this.irmForm = this.fb.group({
      'dateFrom': ['', Validators.required],
      'dateTo': ['', Validators.required],
      'itemCode': [''],
      'itemNo':['']
    });
  }

  save(formData) {
    if (formData) {
      this.jqxLoader.open();
      this.report.getItemRepairMaintenanceReport(formData).subscribe(
        result => {
          this.reportDatas = result;
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please enter all data';
      this.errNotification.open();
    }

  }

  exportReport(): void {
    let htmltable = document.getElementById('reportContainer');
    let html = htmltable.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
  }

  printReport(): void {
    let printContents, popupWin;
    printContents = document.getElementById('reportContainer').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }
}
