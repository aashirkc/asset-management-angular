import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemRepairMaintenanceComponent } from './item-repair-maintenance.component';

describe('ItemRepairMaintenanceComponent', () => {
  let component: ItemRepairMaintenanceComponent;
  let fixture: ComponentFixture<ItemRepairMaintenanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemRepairMaintenanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemRepairMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
