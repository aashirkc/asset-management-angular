import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ItemRepairMaintenanceComponent } from './item-repair-maintenance.component';
import { ItemRepairMaintenanceRoutingModule } from './item-repair-maintenance-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ItemRepairMaintenanceRoutingModule
  ],
  declarations: [ItemRepairMaintenanceComponent]
})
export class ItemRepairMaintenanceModule { }
