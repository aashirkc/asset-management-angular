import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxDepreciationReportComponent } from './tax-depreciation-report.component';

describe('TaxDepreciationReportComponent', () => {
  let component: TaxDepreciationReportComponent;
  let fixture: ComponentFixture<TaxDepreciationReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxDepreciationReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxDepreciationReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
