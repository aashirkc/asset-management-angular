import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaxDepreciationReportComponent } from './tax-depreciation-report.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: TaxDepreciationReportComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaxDepreciationReportRoutingModule { }
