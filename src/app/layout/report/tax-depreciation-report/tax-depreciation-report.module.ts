import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { TaxDepreciationReportComponent } from './tax-depreciation-report.component';
import { TaxDepreciationReportRoutingModule } from './tax-depreciation-report-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    TaxDepreciationReportRoutingModule
  ],
  declarations: [TaxDepreciationReportComponent]
})
export class TaxDepreciationReportModule { }
