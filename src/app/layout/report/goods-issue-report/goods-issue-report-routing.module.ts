import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoodsIssueReportComponent } from './goods-issue-report.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: GoodsIssueReportComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoodsIssueReportRoutingModule { }
