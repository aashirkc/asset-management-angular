import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportComponent } from './report.component';

const routes: Routes = [
    {
        path: '',
        component: ReportComponent,
        children: [
            { path:'', redirectTo: 'assets-ledger', pathMatch:'full'},
            { path:'assets-ledger', loadChildren:'./assets-ledger/assets-ledger.module#AssetsLedgerModule'},
            { path:'requisition-report', loadChildren:'./requisition-report/requisition-report.module#RequisitionReportModule'},
            { path:'purchase-order-report', loadChildren:'./purchase-order-report/purchase-order-report.module#PurchaseOrderReportModule'},
            { path:'goods-receiving-notes-report', loadChildren:'./goods-receiving-notes-report/goods-receiving-notes-report.module#GoodsReceivingNotesReportModule'},
            { path:'assets-wise-reports', loadChildren:'./assets-wise-reports/assets-wise-reports.module#AssetsWiseReportsModule'},
            { path:'goods-issue-report', loadChildren:'./goods-issue-report/goods-issue-report.module#GoodsIssueReportModule'},
            { path:'goods-disposal-report', loadChildren:'./goods-disposal-report/goods-disposal-report.module#GoodsDisposalReportModule'},
            { path:'tax-depreciation-report', loadChildren:'./tax-depreciation-report/tax-depreciation-report.module#TaxDepreciationReportModule'},
            { path:'stateline-depreciation', loadChildren:'./stateline-depreciation/stateline-depreciation.module#StatelineDepreciationModule'},
            { path:'item-repair-maintenance', loadChildren:'./item-repair-maintenance/item-repair-maintenance.module#ItemRepairMaintenanceModule'},
        ]
    },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ReportRoutingModule { }
