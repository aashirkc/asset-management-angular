import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ChartOfItemsService,SetSerialNumberService,GoodsIssueService, BranchMasterService, AllReportService, DateFormatService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxInputComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxinput';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-assets-ledger',
  templateUrl: './assets-ledger.component.html',
  styleUrls: ['./assets-ledger.component.scss']
})
export class AssetsLedgerComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('branchCombo') branchCombo: jqxComboBoxComponent;
  @ViewChild('dateFrom') dateFrom: jqxInputComponent;
  @ViewChild('myWindow') myWindow: jqxWindowComponent;
  /**
* Global Variable decleration
*/
  alForm: FormGroup;

  itemAdapter: any;
  itemFocus: boolean = false;
  branchAdapter: any = [];
  reportDatas: any = [];
  dateData: any;
  windowDatas: any = [];
  accessoriesDatas: any = [];
  specificationDatas: any = [];
  insurances: any;
  warranty: any;
  itemDetails:any;
  transData: any;
  itemImages:Array<any> = [];
  fileUrl:any;
  photoDeleted: any = [];

  constructor(
    private fb: FormBuilder,
    private cois: ChartOfItemsService,
    private bms: BranchMasterService,
    private report: AllReportService,
    private gis: GoodsIssueService,
    private imageDelete:SetSerialNumberService,
    private translate: TranslateService,
    @Inject('API_URL_DOC') fileUrl: string,
    private date: DateFormatService
  ) {
    this.createForm();
    this.fileUrl = fileUrl;
    console.info(this.fileUrl);
  }

  ngOnInit() {

    let Data = JSON.parse(localStorage.getItem('fAssetUser'))


    let Date1 = Data['fiscalYear']
    let startDate = Date1['startDateBS']
    let todayDate = Date1['endDateBS']
    console.log(startDate)
    this.alForm.get('dateFrom').patchValue(startDate);
    this.alForm.get('dateTo').patchValue(todayDate)

    this.bms.index({}).subscribe(
      response => {

        if (response.length == 1 && response[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response[0].error;
          this.errNotification.open();
          this.branchAdapter = [];
        } else {
          this.branchAdapter = response;
        }
      },
      error => {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = 'Couldnot load Branches';
        this.errNotification.open();
      }
    );
    this.getTranslation();

  }
  getTranslation(){
    this.translate.get(['SN','DEPARTMENT','CODE','NAME','FINANCIALYEAR',"ACTION","UPDATE","DELETESELECTED","RELOAD"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  ngAfterViewInit(){
    // this.date.currentDate().subscribe(
    //   response => {
    //     this.dateData = response[0];
    //     setTimeout(() => {
    //       this.alForm.controls['dateFrom'].setValue(this.dateData['fy_StartBs']);
    //       this.alForm.get('dateFrom').markAsTouched();
    //       this.alForm.controls['dateTo'].setValue(this.dateData['BS_DATE']);
    //       this.alForm.get('dateTo').markAsTouched();
    //     }, 100);
    //   },
    //   error => {
    //     console.log(error);
    //   }
    // )
  }

  /**
* Create the form group
* with given form control name
*/
  createForm() {
    this.alForm = this.fb.group({
      'itemCode': ['', Validators.required],
      'dateFrom': [null, Validators.required],
      'dateTo': [null, Validators.required]
    });
  }


  /**
   * itemFilter Event is called when Item input field has
   * keyup action followed by 'Enter'
   * Generate Suggestion based on input value entered
   * @param searchString
   * @param index
   */
  itemFilter(searchPr) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemCode').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(' ', '');
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocus = true;
        this.cois.show(searchString).subscribe(
          response => {
            this.itemAdapter = response;
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.itemFocus = false;
      }
    }

  }

  /**
   * Event fired when option is selected from Item Suggestion Select field
   * Hide Select field after Item Selected.
   * @param selectedValue
   * @param index
   */
  itemListSelected(selectedValue) {
    if (selectedValue) {
      this.alForm.get('itemCode').patchValue(selectedValue);
    }
  }

  viewItem(data) {
    let itemNo = data['itemNo'];
    this.itemDetails = data['assetCode'];
    console.info(data);
    if (Number(itemNo)) {
      this.jqxLoader.open();
      this.gis.showItemProperty(itemNo).subscribe((response) => {
        console.info(response);
        this.windowDatas = response;
        this.accessoriesDatas = response[0];
        this.specificationDatas = response[1];
        this.insurances = response[2] && response[2][0] || null;
        this.warranty =  response[3] && response[3][0] || null;
        this.itemImages = response && response[4] || null;
        this.jqxLoader.close();
        this.myWindow.draggable(true);
        this.myWindow.title('View Item');
        this.myWindow.open();
      }, (error) => {
        this.jqxLoader.close();
      })
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = "Please Select Item No First !!";
      this.errNotification.open();
    }
  }
  DeletePhoto(photo,i){
    if (confirm("Are you sure? You Want to Delete??")) {
       this.jqxLoader.open();
       this.imageDelete.DeleteFileCreated(photo).subscribe((result)=>{
         this.jqxLoader.close();
        if (result['message']) {
          document.getElementById('data'+i).style.display = 'none';
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
       },(error)=>{
        console.info(error);
        this.jqxLoader.close();
       });
    }
  }
  save(formData) {
    let selectedBranches: any = this.branchCombo.getSelectedItems();

    let branches = '';
    for (let i = 0; i < selectedBranches.length; i++) {
      if (i > 0) {
        branches += ',';
      }
      branches += "'" + selectedBranches[i]['value'] + "'";
    }
    formData['branch'] = '(' + branches + ')';

    if (formData) {
      this.jqxLoader.open();
      this.report.getAssetsLedger(formData).subscribe(
        result => {
          this.reportDatas = result;
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please enter all data';
      this.errNotification.open();
    }

  }

  /**
   * Export Report Data to Excel
   */
  exportReport():void{
    let htmltable= document.getElementById('reportContainer');
    let html = htmltable.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
  }

  /**
   * Open Print Window to print report
   */
  printReport(): void {
    let printContents, popupWin;
    printContents = document.getElementById('reportContainer').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            position:relative;
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          .last-td{
            display:none;
          }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  /**
   * Export Report Data to Excel
   */
  exportDetailsReport():void{
    let htmltable= document.getElementById('printContent');
    let html = htmltable.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
  }

  /**
   * Open Print Window to print report
   */
  printDetailsReport(): void {
    let printContents, popupWin;
    printContents = document.getElementById('printContent').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          .row {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -7.5px;
            margin-left: -7.5px;
        }
        .col {
          -ms-flex-preferred-size: 0;
          flex-basis: 0;
          -webkit-box-flex: 1;
          -ms-flex-positive: 1;
          flex-grow: 1;
          max-width: 100%;
          position: relative;
          width: 100%;
          min-height: 1px;
          padding-right: 7.5px;
          padding-left: 7.5px;
      }
      .image-class{
        width: 200;
        height: auto;
    }
    .imgage-div{
      display:none;
    }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

}
