import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { AssetsLedgerComponent } from './assets-ledger.component';
import { AssetsLedgerRoutingModule } from './assets-ledger-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AssetsLedgerRoutingModule
  ],
  declarations: [AssetsLedgerComponent]
})
export class AssetsLedgerModule { }
