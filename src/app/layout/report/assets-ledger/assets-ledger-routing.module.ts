import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssetsLedgerComponent } from './assets-ledger.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: AssetsLedgerComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssetsLedgerRoutingModule { }
