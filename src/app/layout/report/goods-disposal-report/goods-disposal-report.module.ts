import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { GoodsDisposalReportComponent } from './goods-disposal-report.component';
import { GoodsDisposalReportRoutingModule } from './goods-disposal-report-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    GoodsDisposalReportRoutingModule
  ],
  declarations: [GoodsDisposalReportComponent]
})
export class GoodsDisposalReportModule { }
