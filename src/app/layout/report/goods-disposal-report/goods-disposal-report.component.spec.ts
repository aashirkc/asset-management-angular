import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsDisposalReportComponent } from './goods-disposal-report.component';

describe('GoodsDisposalReportComponent', () => {
  let component: GoodsDisposalReportComponent;
  let fixture: ComponentFixture<GoodsDisposalReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsDisposalReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsDisposalReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
