import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoodsDisposalReportComponent } from './goods-disposal-report.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: GoodsDisposalReportComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoodsDisposalReportRoutingModule { }
