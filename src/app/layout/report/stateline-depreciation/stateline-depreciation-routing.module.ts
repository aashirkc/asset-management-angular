import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StatelineDepreciationComponent } from './stateline-depreciation.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: StatelineDepreciationComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StatelineDepreciationRoutingModule { }
