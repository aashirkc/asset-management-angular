import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatelineDepreciationComponent } from './stateline-depreciation.component';

describe('StatelineDepreciationComponent', () => {
  let component: StatelineDepreciationComponent;
  let fixture: ComponentFixture<StatelineDepreciationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatelineDepreciationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatelineDepreciationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
