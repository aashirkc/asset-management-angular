import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { StatelineDepreciationComponent } from './stateline-depreciation.component';
import { StatelineDepreciationRoutingModule } from './stateline-depreciation-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    StatelineDepreciationRoutingModule
  ],
  declarations: [StatelineDepreciationComponent]
})
export class StatelineDepreciationModule { }
