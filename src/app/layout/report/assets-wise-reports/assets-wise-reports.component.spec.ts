import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetsWiseReportsComponent } from './assets-wise-reports.component';

describe('AssetsWiseReportsComponent', () => {
  let component: AssetsWiseReportsComponent;
  let fixture: ComponentFixture<AssetsWiseReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetsWiseReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetsWiseReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
