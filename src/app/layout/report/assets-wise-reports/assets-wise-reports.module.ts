import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { AssetsWiseReportsComponent } from './assets-wise-reports.component';
import { AssetsWiseReportsRoutingModule } from './assets-wise-reports-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AssetsWiseReportsRoutingModule
  ],
  declarations: [AssetsWiseReportsComponent]
})
export class AssetsWiseReportsModule { }
