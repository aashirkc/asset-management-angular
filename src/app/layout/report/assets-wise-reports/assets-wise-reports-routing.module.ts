import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssetsWiseReportsComponent } from './assets-wise-reports.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: AssetsWiseReportsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssetsWiseReportsRoutingModule { }
