import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsReceivingNotesReportComponent } from './goods-receiving-notes-report.component';

describe('GoodsReceivingNotesReportComponent', () => {
  let component: GoodsReceivingNotesReportComponent;
  let fixture: ComponentFixture<GoodsReceivingNotesReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsReceivingNotesReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsReceivingNotesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
