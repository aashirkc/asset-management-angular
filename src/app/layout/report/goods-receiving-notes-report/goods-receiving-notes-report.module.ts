import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { GoodsReceivingNotesReportComponent } from './goods-receiving-notes-report.component';
import { GoodsReceivingNotesReportRoutingModule } from './goods-receiving-notes-report-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    GoodsReceivingNotesReportRoutingModule
  ],
  declarations: [GoodsReceivingNotesReportComponent]
})
export class GoodsReceivingNotesReportModule { }
