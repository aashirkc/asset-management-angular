import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoodsReceivingNotesReportComponent } from './goods-receiving-notes-report.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: GoodsReceivingNotesReportComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoodsReceivingNotesReportRoutingModule { }
