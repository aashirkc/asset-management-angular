import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { BillSundryMasterComponent } from './bill-sundry-master.component';
import { BillSundryMasterRoutingModule } from './bill-sundry-master-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BillSundryMasterRoutingModule
  ],
  declarations: [BillSundryMasterComponent]
})
export class BillSundryMasterModule { }
