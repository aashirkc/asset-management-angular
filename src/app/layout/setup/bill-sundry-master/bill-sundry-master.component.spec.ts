import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillSundryMasterComponent } from './bill-sundry-master.component';

describe('BillSundryMasterComponent', () => {
  let component: BillSundryMasterComponent;
  let fixture: ComponentFixture<BillSundryMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillSundryMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillSundryMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
