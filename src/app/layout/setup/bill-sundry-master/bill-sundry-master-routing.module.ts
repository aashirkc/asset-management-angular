import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BillSundryMasterComponent } from './bill-sundry-master.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: BillSundryMasterComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillSundryMasterRoutingModule { }
