import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssetSpecificationMasterComponent } from './asset-specification-master.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: AssetSpecificationMasterComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssetSpecificationMasterRoutingModule { }
