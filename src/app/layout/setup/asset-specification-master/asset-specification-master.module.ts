import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import {  AssetSpecificationMasterComponent } from './asset-specification-master.component';
import { AssetSpecificationMasterRoutingModule } from './asset-specification-master-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AssetSpecificationMasterRoutingModule
  ],
  declarations: [
    AssetSpecificationMasterComponent
  ]
})
export class AssetSpecificationMasterModule { }
