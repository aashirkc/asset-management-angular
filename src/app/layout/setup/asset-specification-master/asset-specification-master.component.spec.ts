import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetSpecificationMasterComponent } from './asset-specification-master.component';

describe('AssetSpecificationMasterComponent', () => {
  let component: AssetSpecificationMasterComponent;
  let fixture: ComponentFixture<AssetSpecificationMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetSpecificationMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetSpecificationMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
