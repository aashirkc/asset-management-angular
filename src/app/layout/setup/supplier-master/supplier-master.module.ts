import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { SupplierMasterComponent } from './supplier-master.component';
import { SupplierMasterRoutingModule } from './supplier-master-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SupplierMasterRoutingModule
  ],
  declarations: [SupplierMasterComponent]
})
export class SupplierMasterModule { }
