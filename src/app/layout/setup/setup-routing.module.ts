import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SetupComponent } from './setup.component';

const routes: Routes = [
    {
        path: '',
        component: SetupComponent,
        children: [
            { path:'', redirectTo: 'financial-year', pathMatch:'full'},
            { path:'financial-year', loadChildren:'./financial-year/financial-year.module#FinancialYearModule'},
            { path:'chart-of-items', loadChildren:'./chart-of-items/chart-of-items.module#ChartOfItemsModule'},
            { path: 'department-master', loadChildren: './department-master/department-master.module#DepartmentMasterModule' },
            { path: 'depreciation-ratio', loadChildren: './depreciation-ratio/depreciation-ratio.module#DepreciationRatioModule' },
            { path: 'branch-master', loadChildren: './branch-master/branch-master.module#BranchMasterModule' },
            { path: 'supplier-master', loadChildren: './supplier-master/supplier-master.module#SupplierMasterModule' },
            { path: 'site-master', loadChildren: './site-master/site-master.module#SiteMasterModule' },
            { path: 'asset-accessories-master', loadChildren: './asset-accessories-master/asset-accessories-master.module#AssetAccessoriesMasterModule' },
            { path: 'asset-specification-master', loadChildren: './asset-specification-master/asset-specification-master.module#AssetSpecificationMasterModule' },
            { path: 'bill-sundry-master', loadChildren: './bill-sundry-master/bill-sundry-master.module#BillSundryMasterModule' },
            { path: 'employee-details', loadChildren: './employee-details/employee-details.module#EmployeeDetailsModule' },
        ]
    },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SetupRoutingModule { }
