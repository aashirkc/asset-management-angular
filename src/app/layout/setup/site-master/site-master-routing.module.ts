import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SiteMasterComponent } from './site-master.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: SiteMasterComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SiteMasterRoutingModule { }
