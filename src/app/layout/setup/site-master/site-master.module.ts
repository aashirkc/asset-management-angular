import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import {  SiteMasterComponent } from './site-master.component';
import { SiteMasterRoutingModule } from './site-master-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SiteMasterRoutingModule
  ],
  declarations: [
    SiteMasterComponent
  ]
})
export class SiteMasterModule { }
