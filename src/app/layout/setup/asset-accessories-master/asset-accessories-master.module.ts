import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import {  AssetAccessoriesMasterComponent } from './asset-accessories-master.component';
import { AssetAccessoriesMasterRoutingModule } from './asset-accessories-master-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AssetAccessoriesMasterRoutingModule
  ],
  declarations: [
    AssetAccessoriesMasterComponent
  ]
})
export class AssetAccessoriesMasterModule { }
