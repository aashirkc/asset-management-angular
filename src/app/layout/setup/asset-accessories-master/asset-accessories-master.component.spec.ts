import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetAccessoriesMasterComponent } from './asset-accessories-master.component';

describe('AssetAccessoriesMasterComponent', () => {
  let component: AssetAccessoriesMasterComponent;
  let fixture: ComponentFixture<AssetAccessoriesMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetAccessoriesMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetAccessoriesMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
