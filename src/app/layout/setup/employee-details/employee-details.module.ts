import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import {  EmployeeDetailsComponent } from './employee-details.component';
import { EmployeeDetailsRoutingModule } from './employee-details-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    EmployeeDetailsRoutingModule
  ],
  declarations: [
    EmployeeDetailsComponent
  ]
})
export class EmployeeDetailsModule { }
