import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ChartOfItemsComponent } from './chart-of-items.component';
import { ChartOfItemsRoutingModule } from './chart-of-items-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ChartOfItemsRoutingModule
  ],
  declarations: [ChartOfItemsComponent]
})
export class ChartOfItemsModule { }
