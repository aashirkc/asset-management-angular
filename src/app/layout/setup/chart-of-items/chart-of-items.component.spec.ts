import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartOfItemsComponent } from './chart-of-items.component';

describe('ChartOfItemsComponent', () => {
  let component: ChartOfItemsComponent;
  let fixture: ComponentFixture<ChartOfItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartOfItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartOfItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
