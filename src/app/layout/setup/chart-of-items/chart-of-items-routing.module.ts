import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChartOfItemsComponent } from './chart-of-items.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: ChartOfItemsComponent,
     
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChartOfItemsRoutingModule { }
