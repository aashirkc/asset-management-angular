import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import {  BranchMasterComponent } from './branch-master.component';
import { BranchMasterRoutingModule } from './branch-master-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BranchMasterRoutingModule
  ],
  declarations: [
    BranchMasterComponent
  ]
})
export class BranchMasterModule { }
