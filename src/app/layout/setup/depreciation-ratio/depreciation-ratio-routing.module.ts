import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DepreciationRatioComponent } from './depreciation-ratio.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: DepreciationRatioComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DepreciationRatioRoutingModule { }
