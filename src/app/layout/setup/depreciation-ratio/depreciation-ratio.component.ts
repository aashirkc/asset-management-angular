import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { DepreciationRatioService, } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-depreciation-ratio',
  templateUrl: './depreciation-ratio.component.html',
  styleUrls: ['./depreciation-ratio.component.scss']
})
export class DepreciationRatioComponent implements OnInit {
  depreciationForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  deleteRowIndexes: Array<any> = [];
  groupAdapter: Array<any> = ['A', 'B', 'C', 'D'];
  taxTypeAdapter: Array<any> = [
    { ttname: 'Year', id: 'Y' },
    { ttname: 'Percent', id: 'P' }
  ];
  statelineTypeAdapter: Array<any> = [
    { sltname: 'Year', id: 'Y' },
    { sltname: 'Percent', id: 'P' }
  ];

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;

  constructor(
    private fb: FormBuilder,
    private dr: DepreciationRatioService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
  }

  ngOnInit() {
    this.loadGrid();
  }

  transData:any;
  getTranslation(){
    this.translate.get(['SN','ITEM','NAME','CODE','TAX',"RATE","TYPE","GROUP","STATELINERATE","STATELINETYPE","DELETESELECTED","ACTION","UPDATE"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  loadGridData() {
    this.jqxLoader.open();
    this.dr.index({}).subscribe((res) => {
      this.jqxLoader.close();
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        let data = [];
        for (let i = 0; i < res.length; i++) {
          if (res[i]['taxType'] == 'P') {
            res[i]['ttname'] = 'Percent'
          }
          if (res[i]['taxType'] == 'Y') {
            res[i]['ttname'] = 'Year'
          }
          if (res[i]['taxType'] == 'L') {
            res[i]['ttname'] = 'Life'
          }
          if (res[i]['statelineType'] == 'P') {
            res[i]['sltname'] = 'Percent'
          }
          if (res[i]['statelineType'] == 'Y') {
            res[i]['sltname'] = 'Year'
          }
        }
        this.source.localdata = res;
      }
      this.myGrid.updatebounddata();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }
  createForm() {
    this.depreciationForm = this.fb.group({
      'departmentCode': ['', Validators.required],
      'departmentName': ['', Validators.required],
    });
  }
  ngAfterViewInit() {
    this.loadGridData();
  }
  loadGrid() {

    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'taxRate', type: 'string' },
          { name: 'itemName', type: 'string' },
          { name: 'statelineRate', type: 'number' },
          { name: 'itemCode', type: 'string' },
          { name: 'statelineType', type: 'string' },
          { name: 'taxType', type: 'string' },
          { name: 'groupName', type: 'string' },
          { name: 'ttname', type: 'string' },
          { name: 'sltname', type: 'string' }
        ],
        id: 'id',
        pagesize: 20,
        localdata: [],
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);

    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: '', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['ITEM'] + ' ' + this.transData['CODE'], datafield: 'itemCode', columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      { text: this.transData['ITEM'] + ' ' + this.transData['NAME'], datafield: 'itemName', columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      { text: this.transData['TAX'] + ' ' + this.transData['RATE'], datafield: 'taxRate', columntype: 'textbox', filtercondition: 'starts_with' },
      {
        text: this.transData['TAX'] + ' ' + this.transData['TYPE'], datafield: 'taxType', displayfield: 'ttname', columntype: 'combobox', editable:false, filtercondition: 'starts_with',
        createeditor: (row: number, column: any, editor: any): void => {
          editor.jqxComboBox({
            source: this.taxTypeAdapter,
            valueMember: "id",
            displayMember: "ttname"
          });
        }
      },
      { text: this.transData['STATELINERATE'], datafield: 'statelineRate', columntype: 'textbox', filtercondition: 'starts_with' },
      {
        text: this.transData['STATELINETYPE'], datafield: 'statelineType', displayfield: 'sltname', columntype: 'combobox', editable:false, filtercondition: 'starts_with',
        createeditor: (row: number, column: any, editor: any): void => {
          editor.jqxComboBox({
            source: this.statelineTypeAdapter,
            valueMember: "id",
            displayMember: "sltname"
          });
        }
      },
      {
        text: this.transData['GROUP'], datafield: 'groupName', columntype: 'combobox', filtercondition: 'starts_with',
        createeditor: (row: number, column: any, editor: any): void => {
          editor.jqxComboBox({
            source: this.groupAdapter,
            valueMember: "value",
          });
        }
      },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 85, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['UPDATE'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          this.jqxLoader.open();
          this.dr.store(dataRecord).subscribe(
            result => {
              this.jqxLoader.close();
              if (result['message']) {
                let messageDiv: any = document.getElementById('message');
                messageDiv.innerText = result['message'];
                this.msgNotification.open();
                // this.loadGridData();
              }
              if (result['error']) {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = result['error']['message'];
                this.errNotification.open();
              }
            },
            error => {
              this.jqxLoader.close();
              console.info(error);
            }
          );

        }
      }
    ];
  }
}
