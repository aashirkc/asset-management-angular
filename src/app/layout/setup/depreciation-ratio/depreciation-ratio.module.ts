import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { DepreciationRatioComponent } from './depreciation-ratio.component';
import { DepreciationRatioRoutingModule } from './depreciation-ratio-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    DepreciationRatioRoutingModule
  ],
  declarations: [
    DepreciationRatioComponent
  ]
})
export class DepreciationRatioModule { }
