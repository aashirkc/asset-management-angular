import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepreciationRatioComponent } from './depreciation-ratio.component';

describe('DepreciationRatioComponent', () => {
  let component: DepreciationRatioComponent;
  let fixture: ComponentFixture<DepreciationRatioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepreciationRatioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepreciationRatioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
