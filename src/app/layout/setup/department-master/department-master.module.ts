import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { DepartmentMasterComponent } from './department-master.component';
import { DepartmentMasterRoutingModule } from './department-master-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    DepartmentMasterRoutingModule
  ],
  declarations: [
    DepartmentMasterComponent,
  ]
})
export class DepartmentMasterModule { }
