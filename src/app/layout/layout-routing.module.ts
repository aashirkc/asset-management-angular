import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '', 
        component: LayoutComponent,
        children: [
            { path:'', redirectTo: 'dashboard', pathMatch:'full'},
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'setup', loadChildren:'./setup/setup.module#SetupModule'},
            { path: 'operation', loadChildren:'./operation/operation.module#OperationModule'},
            { path: 'report', loadChildren:'./report/report.module#ReportModule'},
            { path: 'utility', loadChildren:'./utility/utility.module#UtilityModule'},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
