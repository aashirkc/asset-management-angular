import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { SetSerialNumberComponent } from './set-serial-number.component';
import { SetSerialNumberRoutingModule } from './set-serial-number-routing.module'

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SetSerialNumberRoutingModule
  ],
  declarations: [SetSerialNumberComponent]
})
export class SetSerialNumberModule { }
