import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SetSerialNumberComponent } from './set-serial-number.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: SetSerialNumberComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SetSerialNumberRoutingModule { }