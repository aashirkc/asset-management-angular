import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetSerialNumberComponent } from './set-serial-number.component';

describe('SetSerialNumberComponent', () => {
  let component: SetSerialNumberComponent;
  let fixture: ComponentFixture<SetSerialNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetSerialNumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetSerialNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
