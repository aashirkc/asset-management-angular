import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YearlyRepairExpensesComponent } from './yearly-repair-expenses.component';

describe('YearlyRepairExpensesComponent', () => {
  let component: YearlyRepairExpensesComponent;
  let fixture: ComponentFixture<YearlyRepairExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YearlyRepairExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YearlyRepairExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
