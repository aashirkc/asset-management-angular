import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { YearlyRepairExpensesComponent } from './yearly-repair-expenses.component';
import { YearlyRepairExpensesRoutingModule } from './yearly-repair-expenses-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    YearlyRepairExpensesRoutingModule
  ],
  declarations: [YearlyRepairExpensesComponent]
})
export class YearlyRepairExpensesModule { }
