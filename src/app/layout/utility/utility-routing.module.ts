import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {  UtilityComponent } from './utility.component';

const routes: Routes = [
    {
        path: '',
        component:  UtilityComponent,
        children: [
            { path:'', redirectTo: 'approve-requisition', pathMatch:'full'},
            { path:'approve-requisition', loadChildren:'./approve-requisition/approve-requisition.module#ApproveRequisitionModule'},
            { path:'yearly-repair-expenses', loadChildren:'./yearly-repair-expenses/yearly-repair-expenses.module#YearlyRepairExpensesModule'},
            { path:'user', loadChildren:'./user/user.module#UserModule'},
            { path:'permission', loadChildren:'./permission/permission.module#PermissionModule'},
            { path:'approve-purchase-order', loadChildren:'./approve-purchase-order/approve-purchase-order.module#ApprovePurchaseOrderModule'},
            { path:'set-serial-number', loadChildren:'./set-serial-number/set-serial-number.module#SetSerialNumberModule'},
            { path:'item-repair', loadChildren:'./item-repair/item-repair.module#ItemRepairModule'},
            { path:'backup', loadChildren:'./backup/backup.module#BackupModule'},
            { path:'approve-issue', loadChildren:'./approve-issue/approve-issue.module#ApproveIssueModule'},
            { path:'change-password', loadChildren:'./change-password/change-password.module#ChangePasswordModule'},
            { path:'organization-master', loadChildren:'./organization-master/organization-master.module#OrganizationMasterModule'}
        ]
    },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UtilityRoutingModule { }
