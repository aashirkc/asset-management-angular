import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrganizationMasterComponent } from './organization-master.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: OrganizationMasterComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganizationMasterRoutingModule { }
