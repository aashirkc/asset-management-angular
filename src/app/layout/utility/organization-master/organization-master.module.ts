import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { OrganizationMasterComponent } from './organization-master.component';
import { OrganizationMasterRoutingModule } from './organization-master-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    OrganizationMasterRoutingModule
  ],
  declarations: [
    OrganizationMasterComponent
  ]
})
export class OrganizationMasterModule { }
