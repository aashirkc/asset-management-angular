import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { PermissionService, UserService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss']
})
export class PermissionComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('userCombo') userCombo: jqxComboBoxComponent;

  selectedUser:any;

  userAdapter: any = [];

  source: any;
  dataAdapter: any;
  columns: any[];

  editrow: number = -1;

  branchAccessAdapter: any = [
    { name: 'All', accessBranch: 'All'},
    { name: 'Home', accessBranch: 'Home'}
  ]

  statusAdapter: any = [
    { status: 'Y', value: 'Y'},
    { status: 'N', value: 'N'}
  ]

  constructor(
    private ps: PermissionService,
    private us: UserService,
    private translate: TranslateService
  ) { 
    this.getTranslation();
  }

  ngOnInit() {
    this.loadGrid();
  }

  ngAfterViewInit(){
    this.loadUsers();
  }

  transData:any;
  getTranslation(){
    this.translate.get(['SN','USER','PERMISSION', 'PERMISSIONSTATUS','ACCESSBRANCH',"ACTION","UPDATE"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  loadUsers(){
    this.jqxLoader.open();
    this.us.index({}).subscribe(
      response => {
       
       if (response.length == 1 && response[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = response[0].error;
        this.errNotification.open();
        this.userAdapter = [];
      } else {
        this.userAdapter = response;
      }
       this.jqxLoader.close();
      },
      error => {
       console.log(error);
       this.jqxLoader.close();
      }
    )
  }


  getUserPermissions(){
    let userCode = this.userCombo.val();
    if(userCode){
      this.ps.show(userCode).subscribe(
        response => {
          console.log(response);
          this.source.localdata = response;
          this.myGrid.updatebounddata();
        },
        error=>{
          console.log(error);
        }
      );
    }else{
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please Select User';
      this.errNotification.open();
    }
  }


  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'accessBranch', type: 'string' },
          { name: 'permission', type: 'string' },
          { name: 'status', type: 'string' },
          { name: 'value', type:'string'}
        ],
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);

    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: '', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['USER'] + ' ' + this.transData['PERMISSION'], datafield: 'permission', columntype: 'textbox', editable: false, filtercondition: 'containsignorecase' },
      { 
        text: this.transData['ACCESSBRANCH'], datafield: 'accessBranch', displayfield: 'accessBranch', columntype: 'combobox', width:300, filtercondition: 'containsignorecase',
        createeditor: (row: number, column: any, editor: any): void => {
          editor.jqxComboBox({
            source: this.branchAccessAdapter,
            valueMember: "accessBranch",
            displayMember: "name"
          });
        }
      },
      { 
        text: this.transData['PERMISSIONSTATUS'], datafield: 'value', displayfield: 'status', columntype: 'combobox', width:160, filtercondition: 'containsignorecase',
        createeditor: (row: number, column: any, editor: any): void => {
          editor.jqxComboBox({
            source: this.statusAdapter,
            valueMember: "value",
            displayMember: "status"
          });
        }
      },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 85, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['UPDATE'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          this.jqxLoader.open();
          let userCode = this.userCombo.val();
          dataRecord['userCode'] = userCode;
          this.ps.store(dataRecord).subscribe(
            result => {
              this.jqxLoader.close();
              if (result['message']) {
                let messageDiv: any = document.getElementById('message');
                messageDiv.innerText = result['message'];
                this.msgNotification.open();
                this.getUserPermissions();
              }
              if (result['error']) {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = result['error']['message'];
                this.errNotification.open();
                this.getUserPermissions();
              }
            },
            error => {
              this.jqxLoader.close();
              console.info(error);
            }
          );

        }
      }
    ];

  }

}
