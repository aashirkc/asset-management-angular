import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { BackupComponent } from './backup.component';
import { BackupRoutingModule } from './backup-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BackupRoutingModule
  ],
  declarations: [BackupComponent]
})
export class BackupModule { }
