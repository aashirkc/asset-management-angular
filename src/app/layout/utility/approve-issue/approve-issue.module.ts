import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ApproveIssueComponent } from './approve-issue.component';
import { ApproveIssueRoutingModule } from './approve-issue-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ApproveIssueRoutingModule
  ],
  declarations: [
    ApproveIssueComponent
  ]
})
export class ApproveIssueModule { }
