import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveIssueComponent } from './approve-issue.component';

describe('ApproveIssueComponent', () => {
  let component: ApproveIssueComponent;
  let fixture: ComponentFixture<ApproveIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
