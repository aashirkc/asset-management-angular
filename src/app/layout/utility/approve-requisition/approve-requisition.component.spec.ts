import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveRequisitionComponent } from './approve-requisition.component';

describe('ApproveRequisitionComponent', () => {
  let component: ApproveRequisitionComponent;
  let fixture: ComponentFixture<ApproveRequisitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveRequisitionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveRequisitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
