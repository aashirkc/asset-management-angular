import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ApproveRequisitionComponent } from './approve-requisition.component';
import { CommonModule } from '@angular/common';
import { HistoryRequisitionComponent } from './history-requisition/history-requisition.component';
const routes: Routes = [
  {
    path: '',
    component: ApproveRequisitionComponent,
  },
  {
    path: 'history-requisition.component',
    component: HistoryRequisitionComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApproveRequisitionRoutingModule { }
