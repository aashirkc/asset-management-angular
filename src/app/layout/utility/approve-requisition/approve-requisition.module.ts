import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ApproveRequisitionComponent } from './approve-requisition.component';
import { ApproveRequisitionRoutingModule } from './approve-requisition-routing.module';
import { HistoryRequisitionComponent } from './history-requisition/history-requisition.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ApproveRequisitionRoutingModule
  ],
  declarations: [
    ApproveRequisitionComponent
    ,
    HistoryRequisitionComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ApproveRequisitionModule { }
