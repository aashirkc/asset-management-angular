import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemRepairComponent } from './item-repair.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: ItemRepairComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemRepairRoutingModule { }
