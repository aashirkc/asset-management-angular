import { Component, OnInit, Inject, EventEmitter, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ChartOfItemsService, CurrentUserService,ItemRepairMaintenanceService, DateFormatService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxTabsComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxtabs';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-item-repair',
  templateUrl: './item-repair.component.html',
  styleUrls: ['./item-repair.component.scss']
})
export class ItemRepairComponent implements OnInit {

  @ViewChild('irGrid') irGrid: jqxGridComponent;
  @ViewChild('repairPendingGrid') repairPendingGrid: jqxGridComponent;
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('repairTab') repairTab: jqxTabsComponent;

  irForm: FormGroup;
  userData: any = {};
  gridSource: any;
  gridDataAdapter: any;
  gridColumns: any = [];
  gridcolumngroups: any;


    /**
   * itemAdater variable is used to load option for select based
   * item input field value
   */
  itemAdapter: any = [];

  /**
   * itemFocus Variable is used to hide/show item select field after
   * item have been selected in select input
   * 
   */
  itemFocus: boolean = false;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private cois: ChartOfItemsService,
    private irms: ItemRepairMaintenanceService,
    private cus: CurrentUserService,
    private dfs: DateFormatService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef
  ) {
    this.createForm();
    this.userData = this.cus.getTokenData();
    this.getTranslation();
  }
  
  transData:any;
  getTranslation(){
    this.translate.get(['REPAIRENTRY','REPAIRPENDING','SN','ITEM','NUMBER','FISCALYEAR','ITEMREPAIR','REPAIRCAUSE',"REPAIRDATE","REPAIRAMOUNT","SERVICECENTER","NAME","ADDRESS","CONTACT","ENTERBY","APPROVEBY","APPROVEDATE","STATUS","ACTION","UPDATE","REMARKS"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  ngOnInit() {

    this.gridSource =
      {
        datatype: 'json',
        datafields: [
          { name: 'itemNo', type: 'string' },
          { name: 'fy', type: 'string' },
          { name: 'repairCouse', type: 'string' },
          { name: 'repairDate', type: 'string' },
          { name: 'repairAmount', type: 'string' },
          { name: 'serviceCenterName', type: 'string' },
          { name: 'serviceCenterAddress', type: 'string' },
          { name: 'serviceCenterContact', type: 'string' },
          { name: 'enterOrderBy', type: 'string' },
        ],
        localdata: [],
      };

    this.gridDataAdapter = new jqx.dataAdapter(this.gridSource);

    this.gridColumns =
      [
        {
          text: this.transData['SN'], sortable: false, filterable: false, editable: false,
          groupable: false, draggable: false, resizable: false,
          datafield: '', columntype: 'number', width: 50,
          cellsrenderer: function (row, column, value) {
            return "<div style='margin:4px;'>" + (value + 1) + "</div>";
          }
        },
        { text: this.transData['ITEM']+ ' ' + this.transData['NUMBER'], displayfield: 'itemNo', datafield: 'itemNo', width: 150, editable: false },
        { text: this.transData['FISCALYEAR'], displayfield: 'fy', datafield: 'fy', width: 120, editable: false },
        { text: this.transData['REPAIRCAUSE'], datafield: 'repairCouse', columngroup: 'repair', width: 150, editable: false },
        { text: this.transData['REPAIRDATE'], datafield: 'repairDate', columngroup: 'repair', width: 150, editable: false },
        { text: this.transData['REPAIRAMOUNT'], datafield: 'repairAmount',columngroup: 'repair', width: 90, editable: false },
        { text: this.transData['NAME'], datafield: 'serviceCenterName', columngroup: 'serviceCenter', width: 160, editable: false },
        { text: this.transData['ADDRESS'], datafield: 'serviceCenterAddress', columngroup: 'serviceCenter', editable: false },
        { text: this.transData['CONTACT'], datafield: 'serviceCenterContact', columngroup: 'serviceCenter', editable: false },
        { text: this.transData['ENTERBY'], datafield: 'enterOrderBy', editable: false },
      ];

      
    this.gridcolumngroups =
    [
        { text: this.transData['ITEMREPAIR'], align: 'center', name: 'repair' },
        { text: this.transData['SERVICECENTER'], align: 'center', name: 'serviceCenter' }
    ];

    this.loadRepairPendingGrid();
    this.getFiscalYear();
  }


  ngAfterViewInit(){
    this.irForm.controls['enterOrderBy'].setValue(this.userData['user'].userName);
    this.irForm.get('enterOrderBy').markAsTouched();
    this.repairTab.setTitleAt(0, this.transData['REPAIRENTRY']);
    this.repairTab.setTitleAt(1, this.transData['REPAIRPENDING']);
    this.loadPendingRepair();
    this.cdr.detectChanges();
  }
  
  loadPendingRepair(){
    this.irms.index({}).subscribe(
      response => {
        this.source.localdata = response;
        this.repairPendingGrid.updatebounddata();
      },
      error => {
        console.log(error);
      }
    );
  }
  getFiscalYear(){
    this.dfs.currentDate().subscribe(
      response => {
        console.log(response);
        if(response['length'] > 0 && response[0]['fySymbol']){
          this.irForm.get('fy').patchValue(response[0]['fySymbol']);
        }
        if(response['length'] > 0 && response[0]['BS_DATE']){
          this.irForm.get('repairDate').patchValue(response[0]['BS_DATE']);
        }
      },
      error => {
        console.log(error);
      }
    )
  }

    /**
* Create the form group 
* with given form control name 
*/
createForm() {
  this.irForm = this.fb.group({
    'itemNo': [null, Validators.required],
    'fy': [null, Validators.required],
    'repairCouse': [null, Validators.required],
    'repairDate': [null, Validators.required],
    'repairAmount': [null, Validators.required],
    'serviceCenterName': [null, Validators.required],
    'serviceCenterAddress': [null, Validators.required],
    'serviceCenterContact': [null, Validators.required],
    'enterOrderBy': [null, Validators.required],
  });
}

  /**
   * itemFilter Event is called when Item input field has
   * keyup action followed by 'Enter'
   * Generate Suggestion based on input value entered
   * @param searchString 
   * 
   */
  itemFilter(searchPr) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemNo').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(' ', '');
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocus = true;
        this.cois.showItem(searchString).subscribe(
          response => {
            this.itemAdapter = [];
            this.itemAdapter = response;
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.itemFocus = false;
      }
    }

  }

  /**
   * Event fired when option is selected from Item Suggestion Select field
   * Hide Select field after Item Selected.
   * @param selectedValue 
   * 
   */
  itemListSelected(selectedValue) {
    if (selectedValue) {
      this.irForm.get('itemNo').patchValue(selectedValue);
      this.loadItemRepairHistory(selectedValue);
    }
  }

  loadItemRepairHistory(itemNo){
    this.jqxLoader.open();
    this.irms.getRepairByItem(itemNo).subscribe(
      response => {
        this.jqxLoader.close();
        
        if (response.length == 1 && response[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response[0].error;
          this.errNotification.open();
          this.gridSource.localdata = [];
        } else {
          this.gridSource.localdata = response;
        }
        this.irGrid.updatebounddata();
      },
      error => {
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }

  save(formData) {
    this.jqxLoader.open();
    this.irms.store(formData).subscribe(
      response => {

        if (response['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = response['message'];
          this.msgNotification.open();
          this.irForm.reset();
          this.gridSource.localdata = [];
          this.irGrid.updatebounddata();
        }
        this.jqxLoader.close();
        if (response['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response['error']['message'];
          this.errNotification.open();
        }
        
        
      },
      error => {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please Select Purchase Order Items';
      this.errNotification.open();
        this.jqxLoader.close();
      }
    )
  }

  source: any;
  dataAdapter:any;
  columns:any = [];
  editrow: number = -1;
  statusAdapter:any = [
    { label: 'Approve' },
    { label: 'Reject' }
  ]

  loadRepairPendingGrid(){
      this.source =
        {
          datatype: 'json',
          datafields: [
            { name: 'sn', type: 'string' },
            { name: 'itemNo', type: 'string' },
            { name: 'approveBy', type: 'string' },
            { name: 'approveDate', type: 'string' },
            { name: 'status', type: 'string' },
            { name: 'remarks', type: 'string' },
          ],
          id: 'sn',
          localdata: [],
          pagesize: 20,
        }
  
      this.dataAdapter = new jqx.dataAdapter(this.source);
  
      this.columns = [
        {
          text: this.transData['SN'], sortable: false, filterable: false, editable: false,
          groupable: false, draggable: false, resizable: false,
          datafield: 'sn', columntype: 'number', width: 50,
          cellsrenderer: function (row, column, value) {
            return "<div style='margin:4px;'>" + (value + 1) + "</div>";
          }
        },
        { text: this.transData['ITEM'] + ' ' + this.transData['NUMBER'], datafield: 'itemNo', columntype: 'textbox', editable: false, filtercondition: 'containsignorecase' },
        { text: this.transData['APPROVEBY'], datafield: 'approveBy', columntype: 'textbox', filtercondition: 'containsignorecase' },
        { text: this.transData['APPROVEDATE'], datafield: 'approveDate', columntype: 'textbox', filtercondition: 'containsignorecase' },
        { text: this.transData['STATUS'], datafield: 'status', filtercondition: 'containsignorecase', width:100, columntype: "combobox",
          createeditor: (row: number, column: any, editor: any): void => {
            editor.jqxComboBox({
              source: this.statusAdapter,
              displayMember: "label",
              valueMember: "label",
              searchMode: "containsignorecase"
            });
          },
        },
        { text: this.transData['REMARKS'], datafield: 'remarks', columntype: 'textbox', filtercondition: 'containsignorecase' },
        {
          text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 85, columntype: 'button',
          cellsrenderer: (): string => {
            return this.transData['UPDATE'];
          },
          buttonclick: (row: number): void => {
            this.editrow = row;
            let dataRecord = this.repairPendingGrid.getrowdata(this.editrow);
            this.jqxLoader.open();
            this.irms.update(dataRecord['sn'], dataRecord).subscribe(
              result => {
                this.jqxLoader.close();
                if (result['message']) {
                  let messageDiv: any = document.getElementById('message');
                  messageDiv.innerText = result['message'];
                  this.msgNotification.open();
                  this.loadPendingRepair();
                }
                if (result['error']) {
                  let messageDiv: any = document.getElementById('error');
                  messageDiv.innerText = result['error']['message'];
                  this.errNotification.open();
                }
              },
              error => {
                this.jqxLoader.close();
                console.info(error);
              }
            );
  
          }
        }
      ];
  
  }

}
