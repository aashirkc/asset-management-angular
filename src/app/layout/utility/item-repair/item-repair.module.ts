import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ItemRepairComponent } from './item-repair.component';
import { ItemRepairRoutingModule } from './item-repair-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ItemRepairRoutingModule
  ],
  declarations: [ItemRepairComponent]
})
export class ItemRepairModule { }