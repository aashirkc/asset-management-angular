import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ChartOfItemsService {

  apiUrl: string;
  apiUrl2: string;
  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string
  ) {
    this.apiUrl = apiUrl+'Setup/ChartOfItems';
    this.apiUrl2 = apiUrl+'operation/';
  }

  index(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl, { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  store(post) {
    return this.http.post(this.apiUrl, post)
      .map(
        (response) => response,
        (error) => error
      );
  }

  destroy(id) {
    return this.http.delete(this.apiUrl + '/' + id).map(
      (response: Response) => response,
      (error) => error)
  }

  show(searchString) {
    return this.http.get(this.apiUrl + '/' + searchString).map(
      (response: Response) => response,
      (error) => error
    );
  }

  update(id, post): Observable<any[]> {
    return this.http.put(this.apiUrl + '/' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  showChild(id) {
    return this.http.get(this.apiUrl + '/child/' + id).map(
      (response: Response) => response,
      (error) => error
    );
  }
  showItem(id) {
    return this.http.get(this.apiUrl2 + 'Store/' + id).map(
      (response: Response) => response,
      (error) => error
    );
  }
  showItemMaintenance(id,code) {
    return this.http.get(this.apiUrl2 + 'Store/' + id+'?itemCode='+code).map(
      (response: Response) => response,
      (error) => error
    );
  }
 
  indexItemIssueReturn(post,id): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl2 + 'GoodsIssueReturn/' + id, { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  storeItemIssueReturn(post) {
    return this.http.post(this.apiUrl2 + 'GoodsIssueReturn', post)
      .map(
        (response) => response,
        (error) => error
      );
  }
  getLevel() {
    return this.http.get(this.apiUrl + '/level').map(
      (response: Response) => response,
      (error) => error
    );
  }

  

}
