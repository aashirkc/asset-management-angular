import { TestBed, inject } from '@angular/core/testing';

import { ItemSpecificationService } from './item-specification.service';

describe('ItemSpecificationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ItemSpecificationService]
    });
  });

  it('should be created', inject([ItemSpecificationService], (service: ItemSpecificationService) => {
    expect(service).toBeTruthy();
  }));
});
