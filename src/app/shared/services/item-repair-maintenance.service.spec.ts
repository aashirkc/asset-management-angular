import { TestBed, inject } from '@angular/core/testing';

import { ItemRepairMaintenanceService } from './item-repair-maintenance.service';

describe('ItemRepairMaintenanceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ItemRepairMaintenanceService]
    });
  });

  it('should be created', inject([ItemRepairMaintenanceService], (service: ItemRepairMaintenanceService) => {
    expect(service).toBeTruthy();
  }));
});
