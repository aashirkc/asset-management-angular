import { TestBed, inject } from '@angular/core/testing';

import { BillSundryMasterService } from './bill-sundry-master.service';

describe('BillSundryMasterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BillSundryMasterService]
    });
  });

  it('should be created', inject([BillSundryMasterService], (service: BillSundryMasterService) => {
    expect(service).toBeTruthy();
  }));
});
