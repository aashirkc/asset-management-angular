import { TestBed, inject } from '@angular/core/testing';

import { GoodsIssueService } from './goods-issue.service';

describe('GoodsIssueService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GoodsIssueService]
    });
  });

  it('should be created', inject([GoodsIssueService], (service: GoodsIssueService) => {
    expect(service).toBeTruthy();
  }));
});
