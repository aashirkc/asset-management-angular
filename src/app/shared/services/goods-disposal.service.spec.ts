import { TestBed, inject } from '@angular/core/testing';

import { GoodsDisposalService } from './goods-disposal.service';

describe('GoodsDisposalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GoodsDisposalService]
    });
  });

  it('should be created', inject([GoodsDisposalService], (service: GoodsDisposalService) => {
    expect(service).toBeTruthy();
  }));
});
