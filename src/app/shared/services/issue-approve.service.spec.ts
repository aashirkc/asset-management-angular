import { TestBed, inject } from '@angular/core/testing';

import { IssueApproveService } from './issue-approve.service';

describe('IssueApproveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IssueApproveService]
    });
  });

  it('should be created', inject([IssueApproveService], (service: IssueApproveService) => {
    expect(service).toBeTruthy();
  }));
});
