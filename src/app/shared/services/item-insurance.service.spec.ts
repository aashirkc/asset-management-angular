import { TestBed, inject } from '@angular/core/testing';

import { ItemInsuranceService } from './item-insurance.service';

describe('ItemInsuranceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ItemInsuranceService]
    });
  });

  it('should be created', inject([ItemInsuranceService], (service: ItemInsuranceService) => {
    expect(service).toBeTruthy();
  }));
});
