import { TestBed, inject } from '@angular/core/testing';

import { BranchMasterService } from './branch-master.service';

describe('BranchMasterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BranchMasterService]
    });
  });

  it('should be created', inject([BranchMasterService], (service: BranchMasterService) => {
    expect(service).toBeTruthy();
  }));
});
