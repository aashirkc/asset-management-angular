import { TestBed, inject } from '@angular/core/testing';

import { AssetSpecificationMasterService } from './asset-specification-master.service';

describe('AssetSpecificationMasterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AssetSpecificationMasterService]
    });
  });

  it('should be created', inject([AssetSpecificationMasterService], (service: AssetSpecificationMasterService) => {
    expect(service).toBeTruthy();
  }));
});
