import { TestBed, inject } from '@angular/core/testing';

import { AssetAccessoriesMasterService } from './asset-accessories-master.service';

describe('AssetAccessoriesMasterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AssetAccessoriesMasterService]
    });
  });

  it('should be created', inject([AssetAccessoriesMasterService], (service: AssetAccessoriesMasterService) => {
    expect(service).toBeTruthy();
  }));
});
