import { TestBed, inject } from '@angular/core/testing';

import { DepreciationRatioService } from './depreciation-ratio.service';

describe('DepreciationRatioService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DepreciationRatioService]
    });
  });

  it('should be created', inject([DepreciationRatioService], (service: DepreciationRatioService) => {
    expect(service).toBeTruthy();
  }));
});
