import { TestBed, inject } from '@angular/core/testing';

import { SiteMasterService } from './site-master.service';

describe('SiteMasterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SiteMasterService]
    });
  });

  it('should be created', inject([SiteMasterService], (service: SiteMasterService) => {
    expect(service).toBeTruthy();
  }));
});
