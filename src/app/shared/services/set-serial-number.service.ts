import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class SetSerialNumberService {

  apiUrl: string;
  uploadUrl: string;
  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string
  ) {
    this.apiUrl = apiUrl+'Utility/SetItemProperty';
    this.uploadUrl = apiUrl+'Utility/UploadImage';
  }

  index(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl, { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  store(post) {
    return this.http.post(this.apiUrl, post)
      .map(
        (response) => response,
        (error) => error
      );
  }

  searchItem(branchCode, post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + '/' + branchCode, { headers: myHeaders, params: Params })
    .map(
      (response) => <any[]>response,
      (error) => error
    )
  }

  destroy(id) {
    return this.http.delete(this.apiUrl + '/' + id).map(
      (response: Response) => response,
      (error) => error)
  }

  update(id, post): Observable<any[]> {
    return this.http.put(this.apiUrl + '/' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  upload(itemNo, post): Observable<any[]> {
    return this.http.put(this.uploadUrl + '/' + itemNo, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  uploadFileCreate(uploadfiles,itemNo) {
    const _formData: any = new FormData();
    const files: Array<File> = uploadfiles;
    for(let i =0; i < files.length; i++){
      _formData.append("image", files[i], files[i]['name']);
    }
    return  this.http.post(this.uploadUrl + '/' + itemNo, _formData);
  }
  DeleteFileCreated(filename) {
    return  this.http.delete(this.uploadUrl+'?item='+filename);
  }

}
