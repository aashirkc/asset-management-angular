import { TestBed, inject } from '@angular/core/testing';

import { ChartOfItemsService } from './chart-of-items.service';

describe('ChartOfItemsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChartOfItemsService]
    });
  });

  it('should be created', inject([ChartOfItemsService], (service: ChartOfItemsService) => {
    expect(service).toBeTruthy();
  }));
});
