import { TestBed, inject } from '@angular/core/testing';

import { ItemAccessoriesService } from './item-accessories.service';

describe('ItemAccessoriesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ItemAccessoriesService]
    });
  });

  it('should be created', inject([ItemAccessoriesService], (service: ItemAccessoriesService) => {
    expect(service).toBeTruthy();
  }));
});
