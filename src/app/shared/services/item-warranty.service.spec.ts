import { TestBed, inject } from '@angular/core/testing';

import { ItemWarrantyService } from './item-warranty.service';

describe('ItemWarrantyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ItemWarrantyService]
    });
  });

  it('should be created', inject([ItemWarrantyService], (service: ItemWarrantyService) => {
    expect(service).toBeTruthy();
  }));
});
