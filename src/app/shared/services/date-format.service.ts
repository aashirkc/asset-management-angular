import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { DatePipe, DecimalPipe } from '@angular/common';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class DateFormatService {

  apiUrl: string;

  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string,
    private datePipe: DatePipe
  ) {
    this.apiUrl = apiUrl;
  }

  formatYMD(date) {
    return this.datePipe.transform(date,'y-MM-dd');
  }

  ad2bs(date) {
    date = this.formatYMD(date);
    return this.http.get(this.apiUrl + 'Setup/AdBsCalender/' + date).map(
      (response: Response) => response,
      (error) => error
    );
  }
  currentDate(){
      return this.http.get(this.apiUrl + 'Setup/AdBsCalender').map(
        (response: Response) => response,
        (error) => error
      );
  }
}
