import { TestBed, inject } from '@angular/core/testing';

import { YearlyRepairExpensesService } from './yearly-repair-expenses.service';

describe('YearlyRepairExpensesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [YearlyRepairExpensesService]
    });
  });

  it('should be created', inject([YearlyRepairExpensesService], (service: YearlyRepairExpensesService) => {
    expect(service).toBeTruthy();
  }));
});
