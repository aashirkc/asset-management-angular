import { Injectable } from '@angular/core';
import { Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpHeaders, HttpXsrfTokenExtractor, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { JwtHelperService } from '@auth0/angular-jwt';
// import { AppDataService } from '../../shared';

@Injectable()
export class TfmHttpInterceptorService implements HttpInterceptor{

  jwtHelper: JwtHelperService = new JwtHelperService();

  constructor(
    private router: Router,
    private tokenExtractor: HttpXsrfTokenExtractor,
    // private dataService: AppDataService
    // private currentUserService: CurrentUserService
  ) { }
  
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.info('inside intercepter');
      let currentUser =localStorage.getItem('fAssetToken');
      const token: string = currentUser || null;
      if(token && this.jwtHelper.isTokenExpired(token)){
          localStorage.removeItem('fAssetToken');
          this.router.navigate(['/login'],{ queryParams: { type:'error', msg: 'Session Expired !! Please login to continue' } });
          // return false;
      }
      if (token) {
        
          req = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + token)});
      }
      
      return next.handle(req);
  }

}
