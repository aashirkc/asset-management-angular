import { TestBed, inject } from '@angular/core/testing';

import { ApproveRequisitionService } from './approve-requisition.service';

describe('ApproveRequisitionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApproveRequisitionService]
    });
  });

  it('should be created', inject([ApproveRequisitionService], (service: ApproveRequisitionService) => {
    expect(service).toBeTruthy();
  }));
});
