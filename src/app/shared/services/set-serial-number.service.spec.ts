import { TestBed, inject } from '@angular/core/testing';

import { SetSerialNumberService } from './set-serial-number.service';

describe('SetSerialNumberService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SetSerialNumberService]
    });
  });

  it('should be created', inject([SetSerialNumberService], (service: SetSerialNumberService) => {
    expect(service).toBeTruthy();
  }));
});
