import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class RequisitionSlipService {

  apiUrl: string;
  apiUrl2:string;
  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string
  ) {
    this.apiUrl = apiUrl+'requisition/RequisitionSlip';
    this.apiUrl2  = apiUrl + 'operation/GoodsIssue';
  }

  index(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl, { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  store(post) {
    return this.http.post(this.apiUrl, post)
      .map(
        (response) => response,
        (error) => error
      );
  }
  storeIssue(post) {
    return this.http.post(this.apiUrl2, post)
      .map(
        (response) => response,
        (error) => error
      );
  }

  destroy(id) {
    return this.http.delete(this.apiUrl + '/' + id).map(
      (response: Response) => response,
      (error) => error)
  }

  update(id, post): Observable<any[]> {
    return this.http.put(this.apiUrl + '/' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

}
