import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ResponseContentType } from '@angular/http/src/enums';

@Injectable()
export class AllReportService {
  apiUrl: string;
  apiUrl2: string;

  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string
  ) {
    this.apiUrl = apiUrl + 'Report/';
    this.apiUrl2 = apiUrl + 'Print/';

  }

  getAssetsLedger(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'AssetsLedger', { headers: myHeaders, params: Params })
    .map(
      (response) => <any[]>response,
      (error) => error
    )
  }

  // For Blobs excels
  index(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/vnd.ms-excel');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'production_analysis_report', { headers: myHeaders, params: Params, responseType: 'blob' })
      .catch(error => Observable.throw(error.json()))
      .map(
      (response) => {
        if (response instanceof Response) {
          return response.blob();
        }
        return response;
      },
      (error) => error
      )
  }

  getReqRequestedBy(id): Observable<any[]> {
    return this.http.get(this.apiUrl + 'Requisition/' + id)
    .map(
      (response) => <any[]>response,
      (error) => error
    );
  }
  getRequisition(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Requisition', { headers: myHeaders, params: Params })
    .map(
      (response) => <any[]>response,
      (error) => error
    )
  }
  getOrderRequestedBy(id): Observable<any[]> {
    return this.http.get(this.apiUrl + 'PurchaseOrder/' + id)
    .map(
      (response) => <any[]>response,
      (error) => error
    );
  }
  getRequisitionByItemNo(id): Observable<any[]> {
    return this.http.get(this.apiUrl2 + 'Requisition/' + id)
    .map(
      (response) => <any[]>response,
      (error) => error
    );
  }
  getPurchaseByOrderNo(id): Observable<any[]> {
    return this.http.get(this.apiUrl2 + 'PurchaseOrder/' + id)
    .map(
      (response) => <any[]>response,
      (error) => error
    );
  }
  getGoodReceiptNotesByGrnNo(id): Observable<any[]> {
    return this.http.get(this.apiUrl2 + 'GoodsReceivingNote/' + id)
    .map(
      (response) => <any[]>response,
      (error) => error
    );
  }
  getPurchaseOrder(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'PurchaseOrder', { headers: myHeaders, params: Params })
    .map(
      (response) => <any[]>response,
      (error) => error
    )
  }
  getGRNReceiveddBy(id): Observable<any[]> {
    return this.http.get(this.apiUrl + 'GoodsReceivingNotes/' + id)
    .map(
      (response) => <any[]>response,
      (error) => error
    );
  }
  getGoodReceiptNotes(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'GoodsReceivingNotes', { headers: myHeaders, params: Params })
    .map(
      (response) => <any[]>response,
      (error) => error
    )
  }
  getAssetsWiseReports(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'AssetsWise', { headers: myHeaders, params: Params })
    .map(
      (response) => <any[]>response,
      (error) => error
    )
  }
  getItemIssuedBy(id): Observable<any[]> {
    return this.http.get(this.apiUrl + 'GoodsIssue/' + id)
    .map(
      (response) => <any[]>response,
      (error) => error
    );
  }
  getGoodsIssueReports(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'GoodsIssue', { headers: myHeaders, params: Params })
    .map(
      (response) => <any[]>response,
      (error) => error
    )
  }
  getDisposalReports(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'GoodsDisposal', { headers: myHeaders, params: Params })
    .map(
      (response) => <any[]>response,
      (error) => error
    )
  }
  getTaxDepreciationReport(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'TaxDepreciation', { headers: myHeaders, params: Params })
    .map(
      (response) => <any[]>response,
      (error) => error
    )
  }
  getStatelineDepreciationReport(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'StatelineDepreciation', { headers: myHeaders, params: Params })
    .map(
      (response) => <any[]>response,
      (error) => error
    )
  }
  getItemRepairMaintenanceReport(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'ItemRepairMaintenance', { headers: myHeaders, params: Params })
    .map(
      (response) => <any[]>response,
      (error) => error
    )
  }

  
}
