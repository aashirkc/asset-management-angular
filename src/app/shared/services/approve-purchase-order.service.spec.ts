import { TestBed, inject } from '@angular/core/testing';

import { ApprovePurchaseOrderService } from './approve-purchase-order.service';

describe('ApprovePurchaseOrderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApprovePurchaseOrderService]
    });
  });

  it('should be created', inject([ApprovePurchaseOrderService], (service: ApprovePurchaseOrderService) => {
    expect(service).toBeTruthy();
  }));
});
