import { TestBed, inject } from '@angular/core/testing';

import { OrganizationMasterService } from './organization-master.service';

describe('OrganizationMasterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrganizationMasterService]
    });
  });

  it('should be created', inject([OrganizationMasterService], (service: OrganizationMasterService) => {
    expect(service).toBeTruthy();
  }));
});
