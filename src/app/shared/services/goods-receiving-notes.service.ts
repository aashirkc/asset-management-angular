import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class GoodsReceivingNotesService {

  apiUrl: string;
  apiUrlBill: string;
  apiUrlOpening: string;

  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string
  ) {
    this.apiUrl = apiUrl+'operation/GoodsReceivingNotes';
    this.apiUrlBill = apiUrl+'operation/GrnBillSundry';
    this.apiUrlOpening = apiUrl+'operation/GoodsReceivingNotes/Opening';
  }

  index(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl, { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  store(post) {
    return this.http.post(this.apiUrl, post)
      .map(
        (response) => response,
        (error) => error
      );
  }

  grnBillSundry(post) {
    return this.http.post(this.apiUrlBill, post)
      .map(
        (response) => response,
        (error) => error
      );
  }

  getBillSundryByItemNo(itemNo) {
    return this.http.get(this.apiUrlBill+'/'+itemNo)
      .map(
        (response) => response,
        (error) => error
      );
  }
  
  updateBillSundryByItemNo(itemNo,post) {
    return this.http.put(this.apiUrlBill+ '/'+itemNo, post)
      .map(
        (response) => response,
        (error) => error
      );
  }

  destroy(id) {
    return this.http.delete(this.apiUrl + '/' + id).map(
      (response: Response) => response,
      (error) => error)
  }

  update(id, post): Observable<any[]> {
    return this.http.put(this.apiUrl + '/' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

  itemByGrn(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl+'/Item', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  openingIndex(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrlOpening, { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  openingStore(post) {
    return this.http.post(this.apiUrlOpening, post)
      .map(
        (response) => response,
        (error) => error
      );
  }

  openingDestroy(id) {
    return this.http.delete(this.apiUrlOpening + '/' + id).map(
      (response: Response) => response,
      (error) => error)
  }

  openingUpdate(id, post): Observable<any[]> {
    return this.http.put(this.apiUrlOpening + '/' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }

}
