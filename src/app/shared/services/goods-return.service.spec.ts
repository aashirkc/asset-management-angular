import { TestBed, inject } from '@angular/core/testing';

import { GoodsReturnService } from './goods-return.service';

describe('GoodsReturnService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GoodsReturnService]
    });
  });

  it('should be created', inject([GoodsReturnService], (service: GoodsReturnService) => {
    expect(service).toBeTruthy();
  }));
});
