import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { jqxMenuComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxmenu';
import { AuthenticateService } from '../../services/authenticate.service';
import { TranslateService } from '@ngx-translate/core';
@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
    @ViewChild('jqxMenu') jqxMenu: jqxMenuComponent;

    fy: string;
    uname: string; // User Name
    bname: string //Branch Name
    currentLang: string = 'EN';

    constructor(
        private as: AuthenticateService,
        private translate: TranslateService,
        public router: Router
    ) {
        let userData = JSON.parse(localStorage.getItem('fAssetUser'));
        this.fy = userData['fy'];
        this.uname = userData['userName'];
        this.bname = userData['branch'];
        let savedLang = localStorage.getItem('fAssetLang');
        this.currentLang = savedLang.toUpperCase();
    }

    ngAfterViewInit(): void {
        this.jqxMenu.setItemOpenDirection('UserMaster', 'left', 'down');
    }
    menuClicked($event){
        let itemVal = $event.args['innerText'];
        if(itemVal == 'Nepali'){
            this.translate.use('ne');
            this.currentLang = 'NE';
            localStorage.setItem('fAssetLang','ne');
            window.location.reload();
        }
        if(itemVal == 'English'){
            this.translate.use('en');
            this.currentLang = 'EN';
            localStorage.setItem('fAssetLang','en');
            window.location.reload();
        }
        
    }

    isActive = false;
    showMenu = '';
    eventCalled() {
        this.isActive = !this.isActive;
    }
    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }
    logOut() {
        this.as.logout().subscribe((res) => {
            // if (res['message']) {
                localStorage.removeItem('fAssetToken');
                localStorage.removeItem('fAssetUser');
                localStorage.removeItem('lastGrnNo');
                this.router.navigate(['/login']);
            // }
        }, (error) => {
            localStorage.removeItem('fAssetToken');
            localStorage.removeItem('fAssetUser');
            localStorage.removeItem('lastGrnNo');
            this.router.navigate(['/login']);
        });
    }
}
